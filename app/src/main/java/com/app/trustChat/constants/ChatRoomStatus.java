package com.app.trustChat.constants;

public interface ChatRoomStatus {
    String ONLINE ="online";
    String OFFLINE ="offline";
}
