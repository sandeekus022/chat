package com.app.trustChat.constants;

public interface MessageStatus {
    String UNSENT = "unsent";
    String SENT = "sent";
    String DELIVERED = "delivered";
    String READ = "read";
}
