package com.app.trustChat.constants;

public interface MessageType {
    String MESSAGE_TYPE_TEXT = "0";
    String MESSAGE_TYPE_IMAGE = "1";
    String MESSAGE_TYPE_FILE = "2";
    String MESSAGE_TYPE_VIDEO = "3";
    String MESSAGE_TYPE_LOCATION = "4";
    String MESSAGE_TYPE_CONTACT = "5";
    String MESSAGE_TYPE_AUDIO = "6";
    String MESSAGE_TYPE_SYSTEM = "7";
}
