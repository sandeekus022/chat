package com.app.trustChat.constants;

public interface MediaMessageStatus {
    String UPLOADED = "uploaded";
    String IN_PROGRESS = "inProgress";
    String FAILED = "failed";
}
