package com.app.trustChat.constants;

public interface UserStatus {
    String ONLINE = "Online";
    String TYPING = "Typing...";
    String TYPING_TO_NO_ONE = "none_one";
    String OFFLINE = "Offline";
}
