package com.app.trustChat.networking;


import com.app.trustChat.model.DistrictModel;
import com.app.trustChat.model.LoginModel;
import com.app.trustChat.model.MediaUploadModel;
import com.app.trustChat.model.OtpVerificationModel;
import com.app.trustChat.model.PackageModel;
import com.app.trustChat.model.StateModel;
import com.app.trustChat.model.WrapperModel;

import java.util.List;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiCaller {

    @FormUrlEncoded
    @POST("getSponsorRequest/ZGV2ZWxvcGVy")
    Single<WrapperModel> getDefaultSponsor(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("login250720/ZGV2ZWxvcGVy")
    Single<Response<LoginModel>> login(@Field("mobile") String mobile, @Field("sponsor") String sponsor);


    @FormUrlEncoded
    @POST("verifiy_login/ZGV2ZWxvcGVy")
    Single<Response<OtpVerificationModel>> verifyLogin(@Field("mobile") String mobile,
                                                       @Field("otp") String otp,
                                                       @Field("otp_confirm") String otpConfirm);


    @Multipart
    @POST("uploadFile/ZGV2ZWxvcGVy")
    Single<Response<MediaUploadModel>> uploadFile(@Part MultipartBody.Part file,
                                                  @Part("mobile") RequestBody mobile,
                                                  @Part("filename") RequestBody fileName);

    @Multipart
    @POST("uploadProfileImage/ZGV2ZWxvcGVy")
    Single<Response<MediaUploadModel>> uploadProfileImage(@Part MultipartBody.Part file,
                                                          @Part("mobile") RequestBody mobile);

    @FormUrlEncoded
    @POST("updateProfile/ZGV2ZWxvcGVy")
    Single<Response<OtpVerificationModel>> updateProfile(@Field("mobile") String mobile,
                                                         @Field("name") String name,
                                                         @Field("email") String email,
                                                         @Field("state") String state,
                                                         @Field("city") String district);

    @FormUrlEncoded
    @POST("profile/ZGV2ZWxvcGVy")
    Single<Response<OtpVerificationModel>> getProfile(@Field("mobile") String mobile);


    @GET("state_list/ZGV2ZWxvcGVy")
    Single<List<StateModel>> getState();

    @FormUrlEncoded
    @POST("dist_list/ZGV2ZWxvcGVy")
    Single<List<DistrictModel>> getDistrict(@Field("state") String state);

    @GET("packages/ZGV2ZWxvcGVy")
    Single<List<PackageModel>> getPackageDetails();


    @Multipart
    @POST("purchasePackage/ZGV2ZWxvcGVy")
    Single<WrapperModel> uploadPurchase(@Part("mobile") RequestBody mobile,
                                        @Part("pkg_id") RequestBody pkg_id,
                                        @Part("ac_no") RequestBody ca_no,
                                        @Part("ac_holder") RequestBody ac_holder,
                                        @Part("ifsc") RequestBody ifsc,
                                        @Part("tranx_id") RequestBody tranx_id,
                                        @Part MultipartBody.Part file);


    @GET("subscription/ZGV2ZWxvcGVy")
    Single<WrapperModel> getSubscriptionDetails(@Query("mobile") String mobile);


}
