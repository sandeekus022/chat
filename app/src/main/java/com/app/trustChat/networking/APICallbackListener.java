package com.app.trustChat.networking;


import retrofit2.Response;

public interface APICallbackListener<T> {
    void onSuccess(Response<T> response);
    void onFail(ApiError object);
}
