package com.app.trustChat.networking;

public class ApiConstants {

    public interface Headers {
        String X_SECREAT_TOKEN = "x-secret-token";
        String SECRET_TOKEN_VALUE = "sdb38hjie48947ndsk7845wjkh";
    }

    public interface Fields{
        String EMAIL = "email";
        String PASSWORD = "password";
        String NAME = "name";
        String CONFIRM_PASSWORD = "confirmpassword";
        String TERMS_AND_CONDITION = "termsandcondition";
        String DATE_OF_BIRTH = "dob";
        String JOB_TITLE = "jobtitle";
        String COMPANY_NAME = "companyname";
        String INDUSTRY_CODE = "indrustrycode";
        String ADDRESS = "address";
        String POSTAL_CODE = "postalcode";
        String COUNTRY_NAME = "countryname";
        String OTP = "otp";
        String BUSINESS_ID = "businessid";
        String REASON = "reason";
        String TITLE = "title";
        String START_DATE = "startdate";
        String START_TRIP = "starttrip";
        String END_DATE = "enddate";
        String END_POINT = "endpoint";
        String DESCRIPTION = "description";
        String START_COORDINATES = "startcordinates";
        String END_COORDINATES = "endcordinates";
        String TRIP_CATEGORY = "tripcategory";
        String TRIP_ID = "tripid";
        String PROFILE_PIC = "profilepic";
        String NEW_EMAIL = "newemail";
        String ACCESS_TOKEN = "access_token";
        String LANDMARK = "landmark";
        String LAT_LNG = "Lati_longi";
        String HOUSE_NUMBER = "h_no";
        String STATUS = "status";
    }
}
