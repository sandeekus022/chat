package com.app.trustChat.networking;

import android.content.Context;
import android.os.NetworkOnMainThreadException;

import com.app.trustChat.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.concurrent.CancellationException;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

public class GenericApiCaller<T> {

    private Single<Response<T>> caller;
    private Context context;

    public GenericApiCaller(Context context, Single<Response<T>> caller) {
        this.caller = caller;
        this.context = context;
    }

    public void doApiCall(final APICallbackListener<T> listener) {
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<T>>() {
                    @Override
                    public void onSuccess(Response<T> response) {
                        handleSuccess(listener, response, context);
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(listener, e, context);
                    }
                }));
    }


    private void handleSuccess(APICallbackListener<T> apiCallbackListener, Response<T> response,
                               Context context) {

        if (response.code() == 200) {
                apiCallbackListener.onSuccess(response);
        } else {
            ApiError apiError = RetrofitUtils.parseError(response, new ApiError());

            if (apiError == null || apiError.getResponseMessage() == null) {
                apiError = new ApiError();
                apiError.setResponseMessage("Something went wrong, " + response.code());
                apiError.setResponseCode(String.valueOf(response.code()));
            }

            apiCallbackListener.onFail(apiError);
        }
    }


    private void handleError(APICallbackListener listener, Throwable t, Context context) {

        String message = context.getResources().getString(R.string.unable_to_perform_action);
        String log;

        if (t instanceof IOException) {
            message = t.getMessage();
            log = message;
        } else if (t instanceof JSONException) {
            log = "JSON Exception";
        } else if (t instanceof CancellationException) {
            log = "Cancellation Exception";
        } else if (t instanceof HttpException) {
            log = "Http Exception";
        } else if (t instanceof NetworkOnMainThreadException) {
            log = "Network on Main thread Exception";
        } else if (t instanceof RuntimeException) {
            log = "Runtime Exception";
        } else {
            log = "default case Exception";
        }

        log = t != null && t.getMessage() != null ? log + "  --> message : " + t.getMessage() : log;
        LoggerUtils.error("API_FAILURE", log);

        ApiError apiError = new ApiError();
        apiError.setResponseMessage(message);
        listener.onFail(apiError);


    }
}
