package com.app.trustChat.networking;

import android.content.Context;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    private static Retrofit loggedUserAPIService = null;
    private static Retrofit newUserAPIService = null;
    private static String userID = null;
    private static String accessToken = null;
    private static OkHttpClient newUserHttpClient = null;
    private static OkHttpClient loggedUserHttpClient = null;

    // for users who are not signed in yet
    public static ApiCaller getService(Context context) {
        if (newUserAPIService == null) {
            newUserAPIService = new Retrofit
                    .Builder()
                    .client(getNewUserHttpClient(context))
                    .baseUrl("http://trustapp.co.in/api1/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return newUserAPIService.create(ApiCaller.class);
    }



    private static OkHttpClient getNewUserHttpClient(Context context) {
        if (newUserHttpClient == null) {
            newUserHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .addInterceptor(new NetworkConnectionInterceptor(context))
                    .addInterceptor(chain -> {
                        Request request = chain.request();
                        Response response = chain.proceed(request);
                        LoggerUtils.debug("body", Objects.requireNonNull(response.body()).toString());
                        return response;
                    })
                    //here we can add Interceptor for dynamical adding headers
                    .addNetworkInterceptor(chain -> {
                        Request request = chain.request().newBuilder()
                                .build();
                        return chain.proceed(request);
                    })
                    //here we adding Interceptor for full level logging
                    .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build();
        }
        return newUserHttpClient;
    }


    public static Retrofit retrofit() {
        if (newUserAPIService == null)
            return loggedUserAPIService;
        return newUserAPIService;
    }
}