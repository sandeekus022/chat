package com.app.trustChat.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.app.trustChat.MyApplication;

public class NetworkUtils {

    public static  boolean isConnected(Context context){
        if(context == null)
            context = MyApplication.getInstance();
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }
}
