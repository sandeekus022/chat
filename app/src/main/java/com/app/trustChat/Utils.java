package com.app.trustChat;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import com.google.firebase.firestore.DocumentReference;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
/**
 * Created by Rahulucky03 on 04-02-2019.
 */
public class Utils {


    private static final int RANDOM_COLOR_START_RANGE = 0;
    private static final int RANDOM_COLOR_END_RANGE = 9;
    public static String getID(@NonNull DocumentReference documentReference){
        return documentReference.getId();
    }
    @SuppressLint("ResourceAsColor")
    public static Drawable getColorCircleDrawable(int colorPosition) {
        try{
            return getColoredCircleDrawable(getCircleColor(colorPosition % RANDOM_COLOR_END_RANGE));
        }catch (Exception e){
            return getColoredCircleDrawable(R.color.random_color_3);
        }
    }

    public static int getCircleColor(@IntRange(from = RANDOM_COLOR_START_RANGE, to = RANDOM_COLOR_END_RANGE)
                                             int colorPosition) {
        String colorIdName = String.format("random_color_%d", colorPosition + 1);
        int colorId = MyApplication.getInstance().getResources()
                .getIdentifier(colorIdName, "color", MyApplication.getInstance().getPackageName());

        return getColor(colorId);
    }
    public static Drawable getColoredCircleDrawable(@ColorInt int color) {
        GradientDrawable drawable = (GradientDrawable) getDrawable(R.drawable.shape_circle);
        drawable.setColor(color);
        return drawable;
    }
    public static Drawable getDrawable(@DrawableRes int drawableId) {
        return MyApplication.getInstance().getResources().getDrawable(drawableId);
    }
    public static int getColor(@ColorRes int colorId) {
        return MyApplication.getInstance().getResources().getColor(colorId);
    }

}
