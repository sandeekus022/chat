package com.app.trustChat.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {
    private static final int DEFAULT_BUFFER_SIZE = 4096;

    public IOUtils() {
    }

    public static int copy(InputStream input, OutputStream output) throws IOException {
        long count = copyLarge(input, output);
        return count > 2147483647L ? -1 : (int)count;
    }

    public static long copyLarge(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[4096];
        long count = 0L;

        int n;
        for(boolean var5 = false; -1 != (n = input.read(buffer)); count += (long)n) {
            output.write(buffer, 0, n);
        }

        return count;
    }

    public static void closeInputStreams(InputStream... args) {
        InputStream[] var1 = args;
        int var2 = args.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            InputStream is = var1[var3];

            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException var6) {
                var6.printStackTrace();
            }
        }

    }

    public static void closeOutputStreams(OutputStream... args) {
        OutputStream[] var1 = args;
        int var2 = args.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            OutputStream outputStream = var1[var3];

            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException var6) {
                var6.printStackTrace();
            }
        }
    }


    public static File getFileFromUri(Context context,Uri uri){
        String filename=null;
        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            File file = new File(String.valueOf(uri));

            filename = file.getName();
        }
        else if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            Cursor returnCursor =
                    context.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null && returnCursor.moveToFirst()) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                filename = returnCursor.getString(nameIndex);
                returnCursor.close();
            }
        }
        return filename!=null?new File(filename):null;
    }
}
