
package com.app.trustChat.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.R;
import com.app.trustChat.model.PackageModel;
import com.app.trustChat.ui.packageGroup.premiumGroup.PackageDetailsRecyclerAdapter;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class MainUtils {



    public static void showSimpleOkayDialog(Context context, String title, String message,
                                            SimpleOkayDialogListener simpleOkayDialogListener) {
        if (context != null) {
            final Dialog dialog = new Dialog(context);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.simple_okay_dialog, null);
            // controls visibilty of title if title not needed just pass ""
            if (title.length() > 2)
                ((TextView) v.findViewById(R.id.tvHeadingText)).setText(title);
            else
                v.findViewById(R.id.tvHeadingText).setVisibility(View.GONE);

            ((TextView) v.findViewById(R.id.tvDescriptionText)).setText(message);

            v.findViewById(R.id.tvOk).setOnClickListener(v1 -> {
                if (simpleOkayDialogListener != null)
                    simpleOkayDialogListener.onOkayPress();
                dialog.dismiss();
            });


            try {
                dialog.setContentView(v);
                dialog.show();
            } catch (Exception ignored) {
                //Do not show dialog
            }
        }
    }

    public static void  showYesNoDialog(Context context, String title,String message, YesNoDialogListener listener) {
        if (context != null) {
            final Dialog dialog = new Dialog(context);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.dialog_yes_no, null);

            if (title.length() > 2)
                ((TextView) v.findViewById(R.id.tvHeadingText)).setText(title);
            else
                v.findViewById(R.id.tvHeadingText).setVisibility(View.GONE);

            ((TextView) v.findViewById(R.id.tvDescriptionText)).setText(message);

            v.findViewById(R.id.tvYes).setOnClickListener(v1 -> {
                if (listener != null)
                    listener.onYesClick();
                dialog.dismiss();
            });
            v.findViewById(R.id.tvNo).setOnClickListener(v12 -> dialog.dismiss());

            try {
                dialog.setContentView(v);
                dialog.show();
            } catch (Exception ignored) {
                //Do not show dialog
            }
        }
    }


    public interface SimpleOkayDialogListener {

        void onOkayPress();

    }

    public interface YesNoDialogListener {
        void onYesClick();

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //custom Pogress bar
    public static Dialog getChoosePackage(Context context, List<PackageModel> packageList) {
        Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_choose_package, null);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(v);
        Log.e("Package", "size----" + packageList.size());
        RecyclerView rvPackage = dialog.findViewById(R.id.rvPackage);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvPackage.setLayoutManager(linearLayoutManager);
        PackageDetailsRecyclerAdapter adapter = new PackageDetailsRecyclerAdapter(context, packageList, dialog);
        rvPackage.setAdapter(adapter);
        dialog.show();
        return dialog;
    }


}
