package com.app.trustChat.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtil {
    private static final int EOF = -1;
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    private FileUtil() {

    }

    public static File from(Context context, Uri uri) throws IOException {
        InputStream inputStream = context.getContentResolver().openInputStream(uri);
        String fileName = getFileName(context, uri);
        String[] splitName = splitFileName(fileName);
        File tempFile = File.createTempFile(splitName[0], splitName[1]);
        tempFile = rename(tempFile, fileName);
        tempFile.deleteOnExit();
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(tempFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (inputStream != null) {
            copy(inputStream, out);
            inputStream.close();
        }

        if (out != null) {
            out.close();
        }
        return tempFile;
    }

    private static String[] splitFileName(String fileName) {
        String name = fileName;
        String extension = "";
        int i = fileName.lastIndexOf(".");
        if (i != -1) {
            name = fileName.substring(0, i);
            extension = fileName.substring(i);
        }

        return new String[]{name, extension};
    }

    private static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf(File.separator);
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private static File rename(File file, String newName) {
        File newFile = new File(file.getParent(), newName);
        if (!newFile.equals(file)) {
            if (newFile.exists() && newFile.delete()) {
                Log.d("FileUtil", "Delete old " + newName + " file");
            }
            if (file.renameTo(newFile)) {
                Log.d("FileUtil", "Rename file to " + newName);
            }
        }
        return newFile;
    }

    private static long copy(InputStream input, OutputStream output) throws IOException {
        long count = 0;
        int n;
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        while (EOF != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }


    public static String getSubFolder(String mimeType) {
        if (mimeType != null) {
            switch (mimeType) {
                case "video/mp4":
                    return "video/";
                case "text/plain":
                case "application/msword":
                case "application/vnd.openxmlformats-officedocument.wordprocessing":
                case "application/vnd.ms-excel":
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                case "application/vnd.ms-powerpoint":
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                case "application/pdf":
                    return "doc/";
                case "image/gif":
                case "image/jpg":
                case "image/jpeg":
                case "image/png":
                    return "image/";
                default:
                    return "other/";

            }
        } else
            return "other/";
    }


    public static String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = MyApplication.getInstance().getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }


    public static Drawable getFileDrawable(Context context, String fileType) {
        String fileExtension = null;
        if (fileType != null) {
            fileExtension = fileType.replace(".", "");
            switch (fileExtension.toLowerCase()) {
                case "ppt":
                case "pot":
                case "pps":
                case "pptx":
                case "pptm":
                case "potx":
                case "potm":
                case "ppam":
                case "ppsx":
                case "ppsm":
                case "sldx":
                case "sldm":
                    return context.getResources().getDrawable(R.drawable.ppt);

                case "docx":
                case "doc":
                case "docm":
                case "dotx":
                case "dotm":
                case "docb":
                    return context.getResources().getDrawable(R.drawable.doc);
                case "xls":
                case "xlt":
                case "xlm":
                case "xlsx":
                case "xlsm":
                case "xltx":
                case "xltm":
                case "xlsb":
                case "xla":
                case "xlam":
                case "xll":
                case "xlw":
                    return context.getResources().getDrawable(R.drawable.xls);
                case "aac":
                case "m4a":
                case "amr":
                case "opus":
                case "mp3":
                    return context.getResources().getDrawable(R.drawable.mp3);
                case "pdf":
                    return context.getResources().getDrawable(R.drawable.pdf);
                case "xml":
                    return context.getResources().getDrawable(R.drawable.xml);
                case "html":
                    return context.getResources().getDrawable(R.drawable.html);
                case "zip":
                    return context.getResources().getDrawable(R.drawable.zip);
                case "gif":
                    return context.getResources().getDrawable(R.drawable.gif);
                case "txt":
                    return context.getResources().getDrawable(R.drawable.txt);
                case "png":
                    return context.getResources().getDrawable(R.drawable.png);
                case "jpeg":
                case "jpg":
                    return context.getResources().getDrawable(R.drawable.jpg);
                case "3gp":
                case "mp4":
                case "webm":
                case "avi":
                case "mov":
                    return context.getResources().getDrawable(R.drawable.video);
                default:
                    return context.getResources().getDrawable(R.drawable.unknown_file);
            }
        } else {
            return null;
        }
    }
}