package com.app.trustChat.callbacks;

import com.app.trustChat.model.Message;

public interface MessageCallback {
    void onSuccess(Message message);
}
