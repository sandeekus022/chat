package com.app.trustChat.model;

import java.util.HashMap;
import java.util.Map;

public class ChatModel {
    public Map<String, String> users = new HashMap<>() ;

    public static class FileInfo {
        public String fileName;
        public String fileSize;
        public String fileUrl;
    }
}
