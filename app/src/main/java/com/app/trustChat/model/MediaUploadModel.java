package com.app.trustChat.model;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MediaUploadModel {

    @SerializedName("status")
    @Expose
    private int Status;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("file")
    @Expose
    private String file;

    @PropertyName("status")
    public int getStatus() {
        return Status;
    }

    @PropertyName("status")
    public void setStatus(int status) {
        Status = status;
    }

    @PropertyName("image")
    public String getImage() {
        return image;
    }

    @PropertyName("image")
    public void setImage(String image) {
        this.image = image;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
