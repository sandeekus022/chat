package com.app.trustChat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("sponsor_id")
    @Expose
    private String sponsor_id;

    @SerializedName("sponsor_name")
    @Expose
    private String sponsor_name;

    @SerializedName("dp_img")
    @Expose
    private String dp_img;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("state")
    @Expose
    private String state;

    @SerializedName("otp")
    @Expose
    private String otp;

    @SerializedName("is_premium")
    @Expose
    private String is_premium;

    @SerializedName("pkg_id")
    @Expose
    private String pkg_id;

    @SerializedName("pk_amount")
    @Expose
    private String pk_amount;

    @SerializedName("is_admin")
    @Expose
    private String is_admin;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("join_date")
    @Expose
    private String join_date;

    @SerializedName("active_date")
    @Expose
    private String active_date;

    @SerializedName("trans_date")
    @Expose
    private String trans_date;

    @SerializedName("district")
    @Expose
    private String district;


    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getSponsor_id() {
        return sponsor_id;
    }

    public String getSponsor_name() {
        return sponsor_name;
    }

    public String getDp_img() {
        return dp_img;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getOtp() {
        return otp;
    }

    public String getIs_premium() {
        return is_premium;
    }

    public String getPkg_id() {
        return pkg_id;
    }

    public String getPk_amount() {
        return pk_amount;
    }

    public String getIs_admin() {
        return is_admin;
    }

    public String getStatus() {
        return status;
    }

    public String getJoin_date() {
        return join_date;
    }

    public String getActive_date() {
        return active_date;
    }

    public String getTrans_date() {
        return trans_date;
    }

    public String getDistrict() {
        return district;
    }
}



