package com.app.trustChat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;

public class DistrictModel {

    @SerializedName("district")
    @Expose
    private String district;

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }


    @NonNull
    @Override
    public String toString() {
        return this.district;
    }
}
