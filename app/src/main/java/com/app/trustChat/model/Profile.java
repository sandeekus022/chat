package com.app.trustChat.model;


import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class Profile implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("sponsor_id")
    @Expose
    private String sponsorId;
    @SerializedName("sponsor_name")
    @Expose
    private String sponsorName;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("dp_img")
    @Expose
    private String dpImg;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("pincode")
    @Expose
    private Object pincode;
    @SerializedName("dob")
    @Expose
    private Object dob;
    @SerializedName("device_id")
    @Expose
    private Object deviceId;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("is_premium")
    @Expose
    private String isPremium;
    @SerializedName("pin_no")
    @Expose
    private Object pinNo;
    @SerializedName("pkg_id")
    @Expose
    private String pkgId;
    @SerializedName("pk_amount")
    @Expose
    private String pkAmount;
    @SerializedName("ac_holder_name")
    @Expose
    private Object acHolderName;
    @SerializedName("ac_type")
    @Expose
    private Object acType;
    @SerializedName("bank_name")
    @Expose
    private Object bankName;
    @SerializedName("ac_no")
    @Expose
    private Object acNo;
    @SerializedName("ifsc_code")
    @Expose
    private Object ifscCode;
    @SerializedName("pancard")
    @Expose
    private Object pancard;
    @SerializedName("nominee_name")
    @Expose
    private Object nomineeName;
    @SerializedName("nominee_relation")
    @Expose
    private Object nomineeRelation;
    @SerializedName("is_admin")
    @Expose
    private String isAdmin;
    @SerializedName("asc_path")
    @Expose
    private Object ascPath;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("active_date")
    @Expose
    private Object activeDate;
    @SerializedName("password")
    @Expose
    private Object password;
    @SerializedName("is_block")
    @Expose
    private String isBlock;
    @SerializedName("trans_date")
    @Expose
    private String transDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(Object parentId) {
        this.parentId = parentId;
    }

    public String getDpImg() {
        return dpImg;
    }

    public void setDpImg(String dpImg) {
        this.dpImg = dpImg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Object getPincode() {
        return pincode;
    }

    public void setPincode(Object pincode) {
        this.pincode = pincode;
    }

    public Object getDob() {
        return dob;
    }

    public void setDob(Object dob) {
        this.dob = dob;
    }

    public Object getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Object deviceId) {
        this.deviceId = deviceId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(String isPremium) {
        this.isPremium = isPremium;
    }

    public Object getPinNo() {
        return pinNo;
    }

    public void setPinNo(Object pinNo) {
        this.pinNo = pinNo;
    }

    public String getPkgId() {
        return pkgId;
    }

    public void setPkgId(String pkgId) {
        this.pkgId = pkgId;
    }

    public String getPkAmount() {
        return pkAmount;
    }

    public void setPkAmount(String pkAmount) {
        this.pkAmount = pkAmount;
    }

    public Object getAcHolderName() {
        return acHolderName;
    }

    public void setAcHolderName(Object acHolderName) {
        this.acHolderName = acHolderName;
    }

    public Object getAcType() {
        return acType;
    }

    public void setAcType(Object acType) {
        this.acType = acType;
    }

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public Object getAcNo() {
        return acNo;
    }

    public void setAcNo(Object acNo) {
        this.acNo = acNo;
    }

    public Object getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(Object ifscCode) {
        this.ifscCode = ifscCode;
    }

    public Object getPancard() {
        return pancard;
    }

    public void setPancard(Object pancard) {
        this.pancard = pancard;
    }

    public Object getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(Object nomineeName) {
        this.nomineeName = nomineeName;
    }

    public Object getNomineeRelation() {
        return nomineeRelation;
    }

    public void setNomineeRelation(Object nomineeRelation) {
        this.nomineeRelation = nomineeRelation;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Object getAscPath() {
        return ascPath;
    }

    public void setAscPath(Object ascPath) {
        this.ascPath = ascPath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public Object getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(Object activeDate) {
        this.activeDate = activeDate;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public String getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(String isBlock) {
        this.isBlock = isBlock;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

}
