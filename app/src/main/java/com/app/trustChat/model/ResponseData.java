package com.app.trustChat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseData {

    @SerializedName("details")
    @Expose
    private PackageModel packageModel;

    public PackageModel getPackageModel() {
        return packageModel;
    }
}
