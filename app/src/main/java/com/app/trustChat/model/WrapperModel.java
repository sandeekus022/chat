package com.app.trustChat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WrapperModel {

    @SerializedName("mobile")
    @Expose
    private String moile;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("Success")
    @Expose
    private String success;



    @SerializedName("sponsor")
    @Expose
    private String sponsor;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("is_premium")
    @Expose
    private String is_premium;

    @SerializedName("details")
    @Expose
    private PackageModel details;

    public String getStatus() {
        return status;
    }

    public String getIs_premium() {
        return is_premium;
    }

    public PackageModel getDetails() {
        return details;
    }

    public String getSponsor() {
        return sponsor;
    }

    public String getMoile() {
        return moile;
    }

    public String getMsg() {
        return msg;
    }

    public String getSuccess() {
        return success;
    }
}
