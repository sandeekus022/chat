package com.app.trustChat.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "user_table")
public class UserModel implements Parcelable {

    public UserModel() {
    }

    public UserModel(String id, String name2, String status2, String image2) {
        this.userId = id;
        this.usernm = name2;
        this.userStatus = status2;
        this.userphoto = image2;
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "userId")
    @SerializedName("userid")
    @Expose
    private String userId;

    @ColumnInfo(name = "usernm")
    @SerializedName("usernm")
    @Expose
    private String usernm;

    @ColumnInfo(name = "email")
    @SerializedName("email")
    @Expose
    private String email;

    @ColumnInfo(name = "mobile")
    @SerializedName("mobile")
    @Expose
    private String mobile;

    @ColumnInfo(name = "token")
    @SerializedName("token")
    @Expose
    private String token;

    @ColumnInfo(name = "userphoto")
    @SerializedName("userphoto")
    @Expose
    private String userphoto;


    @ColumnInfo(name = "usermsg")
    @SerializedName("usermsg")
    @Expose
    private String usermsg;

    @ColumnInfo(name = "userStatus")
    @SerializedName("userStatus")
    @Expose
    private String userStatus;

    @ColumnInfo(name = "inChat")
    @SerializedName("inchat")
    @Expose
    private String inChat;

    @ColumnInfo(name = "lastSeen")
    @SerializedName("lastseen")
    @Expose
    private Long lastSeen;

    @ColumnInfo(name = "lastupdate")
    @SerializedName("lastupdate")
    @Expose
    private Long lastupdate;

    @ColumnInfo(name = "typingTo")
    @SerializedName("typingTo")
    @Expose
    private String typingTo;

    @ColumnInfo(name = "state")
    @SerializedName("state")
    @Expose
    private String state;

    @ColumnInfo(name = "district")
    @SerializedName("district")
    @Expose
    private String district;

    @ColumnInfo(name = "mood")
    @SerializedName("mood")
    @Expose
    private String mood;

    @ColumnInfo(name = "readUsers")
    @SerializedName("readUsers")
    @Expose
    private String readUsers;

    @ColumnInfo(name = "isContact")
    @SerializedName("isContact")
    @Expose
    private boolean isContact;

    private boolean isSelected;

    private boolean isAlreadyExist;

    public boolean isAlreadyExist() {
        return isAlreadyExist;
    }

    public void setAlreadyExist(boolean alreadyExist) {
        isAlreadyExist = alreadyExist;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    protected UserModel(Parcel in) {
        userId = in.readString();
        usernm = in.readString();
        email = in.readString();
        mobile = in.readString();
        token = in.readString();
        userphoto = in.readString();
        usermsg = in.readString();
        userStatus = in.readString();
        inChat = in.readString();
        if (in.readByte() == 0) {
            lastSeen = null;
        } else {
            lastSeen = in.readLong();
        }
        if (in.readByte() == 0) {
            lastupdate = null;
        } else {
            lastupdate = in.readLong();
        }
        typingTo = in.readString();
        state = in.readString();
        district = in.readString();
        mood = in.readString();
        readUsers = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    @PropertyName("readUsers")
    public String getReadUsers() {
        return readUsers;
    }

    @PropertyName("readUsers")
    public void setReadUsers(String readUsers) {
        this.readUsers = readUsers;
    }

    @PropertyName("inchat")
    public String getInChat() {
        return inChat;
    }

    @PropertyName("inchat")
    public void setInChat(String inChat) {
        this.inChat = inChat;
    }

    @PropertyName("typingto")
    public String getTypingTo() {
        return typingTo;
    }

    @PropertyName("typingto")
    public void setTypingTo(String typingTo) {
        this.typingTo = typingTo;
    }

    @PropertyName("userid")
    public String getUserId() {
        return userId;
    }

    @PropertyName("userid")
    public void setUserId(String userid) {
        this.userId = userid;
    }


    @PropertyName("usernm")
    public String getUsernm() {
        return usernm;
    }

    @PropertyName("usernm")
    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    @PropertyName("token")
    public String getToken() {
        return token;
    }

    @PropertyName("token")
    public void setToken(String token) {
        this.token = token;
    }

    @PropertyName("userphoto")
    public String getUserphoto() {
        return userphoto;
    }

    @PropertyName("userphoto")
    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto;
    }

    @PropertyName("usermsg")
    public String getUsermsg() {
        return usermsg;
    }

    @PropertyName("usermsg")
    public void setUsermsg(String usermsg) {
        this.usermsg = usermsg;
    }

    @PropertyName("userStatus")
    public String getUserStatus() {
        return userStatus;
    }

    @PropertyName("userStatus")
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    @PropertyName("lastseen")
    public Long getLastSeen() {
        return lastSeen;
    }

    @PropertyName("lastseen")
    public void setLastSeen(Long lastSeen) {
        this.lastSeen = lastSeen;
    }

    @PropertyName("email")
    public String getEmail() {
        return email;
    }

    @PropertyName("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @PropertyName("mobile")
    public String getMobile() {
        return mobile;
    }

    @PropertyName("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @PropertyName("mood")
    public String getMood() {
        return mood;
    }

    @PropertyName("mood")
    public void setMood(String mood) {
        this.mood = mood;
    }

    @PropertyName("lastupdate")
    public Long getLastupdate() {
        return lastupdate;
    }

    @PropertyName("lastupdate")
    public void setLastupdate(Long lastupdate) {
        this.lastupdate = lastupdate;
    }

    @PropertyName("isContact")
    public boolean isContact() {
        return isContact;
    }

    @PropertyName("isContact")
    public void setContact(boolean contact) {
        isContact = contact;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!UserModel.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final UserModel other = (UserModel) obj;
        if ((this.userId == null) ? (other.userId != null) : !this.userId.equals(other.userId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.userId != null ? this.userId.hashCode() : 0);
        return hash;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(usernm);
        dest.writeString(email);
        dest.writeString(mobile);
        dest.writeString(token);
        dest.writeString(userphoto);
        dest.writeString(usermsg);
        dest.writeString(userStatus);
        dest.writeString(inChat);
        if (lastSeen == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(lastSeen);
        }
        if (lastupdate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(lastupdate);
        }
        dest.writeString(typingTo);
        dest.writeString(state);
        dest.writeString(district);
        dest.writeString(mood);
        dest.writeString(readUsers);
    }
}
