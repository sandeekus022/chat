package com.app.trustChat.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GroupUser implements Parcelable {
    private String name;
    private String mobile;
    private boolean isAdmin;
    private String mood;
    private int messageCount;
    private String photo;
    private String id;
    private String token;
    private boolean isBlocked;
    public GroupUser() {
    }

    public GroupUser(String name,String mobile,boolean isAdmin, String mood, int messageCount, String photo, String id,String token,boolean isBlocked) {
        this.name = name;
        this.mobile = mobile;
        this.isAdmin = isAdmin;
        this.mood = mood;
        this.messageCount = messageCount;
        this.photo = photo;
        this.id = id;
        this.token = token;
        this.isBlocked = isBlocked;
    }

    protected GroupUser(Parcel in) {
        name = in.readString();
        mobile = in.readString();
        isAdmin = in.readByte() != 0;
        mood = in.readString();
        messageCount = in.readInt();
        photo = in.readString();
        id = in.readString();
        token = in.readString();
        isBlocked = in.readByte()!=0;
    }

    public static final Creator<GroupUser> CREATOR = new Creator<GroupUser>() {
        @Override
        public GroupUser createFromParcel(Parcel in) {
            return new GroupUser(in);
        }

        @Override
        public GroupUser[] newArray(int size) {
            return new GroupUser[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(mobile);
        dest.writeByte((byte) (isAdmin ? 1 : 0));
        dest.writeString(mood);
        dest.writeInt(messageCount);
        dest.writeString(photo);
        dest.writeString(id);
        dest.writeString(token);
        dest.writeByte((byte)(isBlocked?1:0));
    }


}
