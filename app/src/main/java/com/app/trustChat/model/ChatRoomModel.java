package com.app.trustChat.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "chat_room_table")
public class ChatRoomModel implements Parcelable{

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "roomId")
    @SerializedName("roomId")
    @Expose
    private String roomId;


    @ColumnInfo(name = "isGroup")
    @SerializedName("isGroup")
    @Expose
    private boolean isGroup;

    @ColumnInfo(name = "userId")
    @SerializedName("userId")
    @Expose
    private String userId;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    @Expose
    private String title;

    @ColumnInfo(name = "subject")
    @SerializedName("subject")
    @Expose
    private String subject;

    @ColumnInfo(name = "photo")
    @SerializedName("photo")
    @Expose
    private String photo;

    @ColumnInfo(name = "lastMsg")
    @SerializedName("lastMsg")
    @Expose
    private String lastMsg;

    @ColumnInfo(name = "unreadCount")
    @SerializedName("unreadCount")
    @Expose
    private Integer unreadCount;

    @ColumnInfo(name = "opponentId")
    @SerializedName("opponentId")
    @Expose
    private String opponentId;

    @ColumnInfo(name =  "members")
    @SerializedName("members")
    @Expose
    private String members;

    @ColumnInfo(name =  "status")
    @SerializedName("status")
    @Expose
    private String status;

    @ColumnInfo(name = "updatedAt")
    @SerializedName("updatedAt")
    @Expose
    private Long updatedAt;

    @ColumnInfo(name = "createdAt")
    @SerializedName("createdAt")
    @Expose
    private Long createdAt;

    private boolean isRoomSelected;

    public ChatRoomModel() {
    }

    protected ChatRoomModel(Parcel in) {
        roomId = in.readString();
        isGroup = in.readByte() != 0;
        userId = in.readString();
        title = in.readString();
        subject = in.readString();
        photo = in.readString();
        lastMsg = in.readString();
        if (in.readByte() == 0) {
            unreadCount = null;
        } else {
            unreadCount = in.readInt();
        }
        opponentId = in.readString();
        members = in.readString();
        status = in.readString();
        if (in.readByte() == 0) {
            updatedAt = null;
        } else {
            updatedAt = in.readLong();
        }
        if (in.readByte() == 0) {
            createdAt = null;
        } else {
            createdAt = in.readLong();
        }
    }

    public static final Creator<ChatRoomModel> CREATOR = new Creator<ChatRoomModel>() {
        @Override
        public ChatRoomModel createFromParcel(Parcel in) {
            return new ChatRoomModel(in);
        }

        @Override
        public ChatRoomModel[] newArray(int size) {
            return new ChatRoomModel[size];
        }
    };

    @PropertyName("roomId")
    public String getRoomId() {
        return roomId;
    }

    @PropertyName("roomId")
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    @PropertyName("userId")
    public String getUserId() {
        return userId;
    }

    @PropertyName("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @PropertyName("title")
    public String getTitle() {
        return title;
    }

    @PropertyName("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @PropertyName("subject")
    public String getSubject() {
        return subject;
    }

    @PropertyName("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @PropertyName("photo")
    public String getPhoto() {
        return photo;
    }

    @PropertyName("photo")
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @PropertyName("lastMsg")
    public String getLastMsg() {
        return lastMsg;
    }

    @PropertyName("lastMsg")
    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    @PropertyName("opponentId")
    public String getOpponentId() {
        return opponentId;
    }

    @PropertyName("opponentId")
    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    @PropertyName("members")
    public String getMembers() {
        return members;
    }

    @PropertyName("members")
    public void setMembers(String members) {
        this.members = members;
    }

    @PropertyName("updatedAt")
    public Long getUpdatedAt() {
        return updatedAt;
    }

    @PropertyName("updatedAt")
    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    @PropertyName("createdAt")
    public Long getCreatedAt() {
        return createdAt;
    }

    @PropertyName("createdAt")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @PropertyName("unreadCount")
    public Integer getUnreadCount() {
        return unreadCount;
    }

    @PropertyName("unreadCount")
    public void setUnreadCount(Integer unreadCount) {
        this.unreadCount = unreadCount;
    }

    @PropertyName("isGroup")
    public boolean isGroup() {
        return isGroup;
    }

    @PropertyName("isGroup")
    public void setGroup(boolean group) {
        isGroup = group;
    }

    @PropertyName("status")
    public String getStatus() {
        return status;
    }

    @PropertyName("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(roomId);
        dest.writeByte((byte) (isGroup ? 1 : 0));
        dest.writeString(userId);
        dest.writeString(title);
        dest.writeString(subject);
        dest.writeString(photo);
        dest.writeString(lastMsg);
        if (unreadCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(unreadCount);
        }
        dest.writeString(opponentId);
        dest.writeString(members);
        dest.writeString(status);
        if (updatedAt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(updatedAt);
        }
        if (createdAt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(createdAt);
        }
    }

    public boolean isRoomSelected() {
        return isRoomSelected;
    }

    public void setRoomSelected(boolean roomSelected) {
        isRoomSelected = roomSelected;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatRoomModel that = (ChatRoomModel) o;
        return roomId.equals(that.roomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomId);
    }
}
