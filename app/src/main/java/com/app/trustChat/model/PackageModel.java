package com.app.trustChat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackageModel {

    String addParticipantName, addParticipantAbout, addParticipantImg;

    @SerializedName("subs_status")
    @Expose
    String subs_status;
    @SerializedName("mobile")
    @Expose
    String mobile;
    @SerializedName("pkg_id")
    @Expose
    String pkg_id;
    @SerializedName("pkg_name")
    @Expose
    String pkg_name;
    @SerializedName("pkg_amount")
    @Expose
    String pkg_amount;
    @SerializedName("pkg_validity")
    @Expose
    String pkg_validity;

    @SerializedName("pkg_receipt")
    @Expose
    String pkg_receipt;
    @SerializedName("tranx_id")
    @Expose
    String tranx_id;

    @SerializedName("tranx_date")
    @Expose
    String tranx_date;

    @SerializedName("start_date")
    @Expose
    String start_date;
    @SerializedName("renew_date")
    @Expose
    String renew_date;
    @SerializedName("ac_no")
    @Expose
    String ac_no;
    @SerializedName("ac_holder")
    @Expose
    String ac_holder;
    @SerializedName("ifsc")
    @Expose
    String ifsc;
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("msg")
    @Expose
    String msg;

    @SerializedName("id")
    @Expose
    String id;


    public String getMobile() {
        return mobile;
    }

    public String getPkg_id() {
        return pkg_id;
    }

    public String getPkg_receipt() {
        return pkg_receipt;
    }

    public String getTranx_id() {
        return tranx_id;
    }

    public String getTranx_date() {
        return tranx_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getRenew_date() {
        return renew_date;
    }

    public String getAc_no() {
        return ac_no;
    }

    public String getAc_holder() {
        return ac_holder;
    }

    public String getIfsc() {
        return ifsc;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public String getId() {
        return id;
    }

    public String getPkg_name() {
        return pkg_name;
    }

    public String getPkg_amount() {
        return pkg_amount;
    }

    public String getPkg_validity() {
        return pkg_validity;
    }

    public String getAddParticipantName() {
        return addParticipantName;
    }

    public String getAddParticipantAbout() {
        return addParticipantAbout;
    }

    public PackageModel(String addParticipantName, String addParticipantAbout, String addParticipantImg) {
        this.addParticipantName = addParticipantName;
        this.addParticipantAbout = addParticipantAbout;
        this.addParticipantImg = addParticipantImg;
    }
}
