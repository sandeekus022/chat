package com.app.trustChat.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "call_table")
public class CallLog {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private long id;

    @NonNull
    @ColumnInfo(name = "userId")
    private String userId;

    @NonNull
    @ColumnInfo(name = "startTime")
    private long startTime;
    @NonNull
    @ColumnInfo(name = "callType")
    private String callType;//incoming or outgoing
    @NonNull
    @ColumnInfo(name = "isVideoCall")
    private boolean isVideoCall;
    @NonNull
    @ColumnInfo(name = "userName")
    private String userName;
    @NonNull
    @ColumnInfo(name = "userPhoto")
    private String userPhoto;

    public CallLog(String userId,long startTime, String callType, boolean isVideoCall, String userName, String userPhoto) {
        this.userId = userId;
        this.startTime = startTime;
        this.callType = callType;
        this.isVideoCall = isVideoCall;
        this.userName = userName;
        this.userPhoto = userPhoto;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public boolean isVideoCall() {
        return isVideoCall;
    }

    public void setVideoCall(boolean videoCall) {
        isVideoCall = videoCall;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public long getId() {
        return id;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public void setId(long id) {
        this.id = id;
    }
}
