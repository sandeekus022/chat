package com.app.trustChat.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

@Entity(tableName = "msg_table")
public class Message implements Parcelable {

    public Message() {
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "msgId")
    @PropertyName("msgId")
    @Expose
    private String msgId;

    @ColumnInfo(name = "roomId")
    @PropertyName("roomId")
    @Expose
    private String roomId;


    @ColumnInfo(name = "UId")
    @SerializedName("UId")
    @Expose
    private String UId;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    private String name;

    @ColumnInfo(name = "msg")
    @SerializedName("msg")
    @Expose
    private String msg;

    @ColumnInfo(name = "msgType")
    @SerializedName("msgType")
    @Expose
    private String msgType;          // 0: msg, 1: image, 2: file

    @ColumnInfo(name = "mediaUrl")
    @SerializedName("mediaUrl")
    @Expose
    private String mediaUrl;

    @ColumnInfo(name = "mimeType")
    @SerializedName("mimeType")
    @Expose
    private String mimeType;

    @ColumnInfo(name = "mediaType")
    @SerializedName("mediaType")
    @Expose
    private String mediaType;

    @ColumnInfo(name = "createdAt")
    @SerializedName("createdAt")
    @Expose
    private Long createdAt;

    @Ignore
    @SerializedName("readUsers")
    @Expose
    private String readUsers;

    @ColumnInfo(name = "fileName")
    @SerializedName("fileName")
    @Expose
    private String fileName;

    @ColumnInfo(name = "fileSize")
    @SerializedName("fileSize")
    @Expose
    private String fileSize;


    @ColumnInfo(name = "isSeen")
    @SerializedName("isSeen")
    @Expose
    private String isSeen; //See @reference MessageStatus.class


    @ColumnInfo(name = "contacts")
    @SerializedName("contacts")
    @Expose
    private String contacts;

    @ColumnInfo(name = "location")
    @SerializedName("location")
    @Expose
    private String location;

    @ColumnInfo(name = "mediaMessageStatus")
    @SerializedName("mediaMessageStatus")
    @Expose
    private String mediaMessageStatus;

    @ColumnInfo(name = "fileUri")
    @SerializedName("fileUri")
    @Expose
    private String fileUri;



    private boolean isSelected;

    protected Message(Parcel in) {
        msgId = in.readString();
        roomId = in.readString();
        UId = in.readString();
        name = in.readString();
        msg = in.readString();
        msgType = in.readString();
        mediaUrl = in.readString();
        mimeType = in.readString();
        mediaType = in.readString();
        contacts = in.readString();
        location = in.readString();
        mediaMessageStatus = in.readString();
        fileUri = in.readString();
        if (in.readByte() == 0) {
            createdAt = null;
        } else {
            createdAt = in.readLong();
        }
        readUsers = in.readString();
        fileName = in.readString();
        fileSize = in.readString();
        isSeen = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @PropertyName("msgId")
    public String getMsgId() {
        return msgId;
    }

    @PropertyName("roomId")
    public String getRoomId() {
        return roomId;
    }

    @PropertyName("roomId")
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    @PropertyName("msgId")
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    @PropertyName("UId")
    public String getUId() {
        return UId;
    }

    @PropertyName("UId")
    public void setUId(String UId) {
        this.UId = UId;
    }

    @PropertyName("name")
    public String getName() {
        return name;
    }

    @PropertyName("name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("msg")
    public String getMsg() {
        return msg;
    }

    @PropertyName("msg")
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @PropertyName("msgType")
    public String getMsgType() {
        return msgType;
    }

    @PropertyName("msgType")
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @PropertyName("mediaUrl")
    public String getMediaUrl() {
        return mediaUrl;
    }

    @PropertyName("mediaUrl")
    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    @PropertyName("mimeType")
    public String getMimeType() {
        return mimeType;
    }

    @PropertyName("mimeType")
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @PropertyName("mediaType")
    public String getMediaType() {
        return mediaType;
    }

    @PropertyName("mediaType")
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    @PropertyName("createdAt")
    public Long getCreatedAt() {
        return createdAt;
    }

    @PropertyName("createdAt")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }


    @PropertyName("readUsers")
    public String getReadUsers() {
        return readUsers;
    }

    @PropertyName("readUsers")
    public void setReadUsers(String readUsers) {
        this.readUsers = readUsers;
    }

    @PropertyName("fileName")
    public String getFileName() {
        return fileName;
    }

    @PropertyName("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @PropertyName("fileSize")
    public String getFileSize() {
        return fileSize;
    }

    @PropertyName("fileSize")
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    @PropertyName("isSeen")
    public String getIsSeen() {
        return isSeen;
    }

    @PropertyName("isSeen")
    public void setIsSeen(String isSeen) {
        this.isSeen = isSeen;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @PropertyName("contacts")
    public String getContacts() {
        return contacts;
    }

    @PropertyName("contacts")
    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    @PropertyName("location")
    public String getLocation() {
        return location;
    }

    @PropertyName("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @PropertyName("mediaMessageStatus")
    public String getMediaMessageStatus() {
        return mediaMessageStatus;
    }

    @PropertyName("mediaMessageStatus")
    public void setMediaMessageStatus(String mediaMessageStatus) {
        this.mediaMessageStatus = mediaMessageStatus;
    }

    @PropertyName("fileUri")
    public String getFileUri() {
        return fileUri;
    }

    @PropertyName("fileUri")
    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(msgId);
        dest.writeString(roomId);
        dest.writeString(UId);
        dest.writeString(name);
        dest.writeString(msg);
        dest.writeString(msgType);
        dest.writeString(mediaUrl);
        dest.writeString(mimeType);
        dest.writeString(mediaType);
        dest.writeString(contacts);
        dest.writeString(location);
        dest.writeString(mediaMessageStatus);
        dest.writeString(fileUri);
        if (createdAt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(createdAt);
        }
        dest.writeString(readUsers);
        dest.writeString(fileName);
        dest.writeString(fileSize);
        dest.writeString(isSeen);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return msgId.equals(message.msgId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msgId);
    }
}