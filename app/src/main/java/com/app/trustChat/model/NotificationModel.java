package com.app.trustChat.model;

public class NotificationModel {
    public String to;

    public Notification notification = new Notification();
    public Data data = new Data();

    public static class Notification {
        public String title;
        public String body;
    }

    public static class Data {
        public String title;
        public Body body;
    }

    public static class Body{
        public String message;
        public String activityName;
        public String roomId;
        public String toUid;
        public boolean isGroup;
        public String roomPhoto;
        public String senderName;
    }


}
