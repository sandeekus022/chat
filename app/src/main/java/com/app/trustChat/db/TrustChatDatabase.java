package com.app.trustChat.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.app.trustChat.dao.CallDao;
import com.app.trustChat.dao.ChatRoomDao;
import com.app.trustChat.dao.MessageDao;
import com.app.trustChat.dao.UserDao;
import com.app.trustChat.model.CallLog;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



@Database(entities = {UserModel.class, Message.class, ChatRoomModel.class, CallLog.class}, version = 1, exportSchema = false)
public abstract class TrustChatDatabase extends RoomDatabase {

    public abstract UserDao userDao();
    public abstract MessageDao messageDao();
    public abstract ChatRoomDao getChatRoomDao();
    public abstract CallDao getCallDao();

    private static volatile TrustChatDatabase database;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static TrustChatDatabase getDatabase(Context context) {
        if (database == null) {
            synchronized (TrustChatDatabase.class) {
                if (database == null) {
                    database = Room.databaseBuilder(context.getApplicationContext(),
                            TrustChatDatabase.class, "trust_app_database")
                            .build();
                }
            }
        }
        return database;
    }

}
