package com.app.trustChat.ui.otpVerificationFragment;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.app.trustChat.base.BaseViewModel;
import com.app.trustChat.model.OtpVerificationModel;
import com.app.trustChat.networking.APICallbackListener;
import com.app.trustChat.networking.ApiError;
import com.app.trustChat.networking.GenericApiCaller;
import com.app.trustChat.networking.LoggerUtils;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.utils.MainUtils;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Response;

public class OtpVerificationViewModel extends BaseViewModel<OtpVerificationNavigator> {
    // TODO: Implement the ViewModel
    public MutableLiveData<String> otp1= new MutableLiveData<>();
    public MutableLiveData<String> otp2= new MutableLiveData<>();
    public MutableLiveData<String> otp3= new MutableLiveData<>();
    public MutableLiveData<String> otp4= new MutableLiveData<>();
    public String mobileNumber;
    private String otp;

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void onContinue(View v){
        Context context = v.getContext();
        if(validate(context)){
            getNavigator().callVerificationApi(otp);
        }
    }
    public boolean validate(Context context){
        otp="";
        otp = otp1.getValue()+otp2.getValue()+otp3.getValue()+otp4.getValue();
        if(otp.length()!=4) {
            Toast.makeText(context, "Please enter valid OTP", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }




}
