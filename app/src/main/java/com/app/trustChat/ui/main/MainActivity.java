package com.app.trustChat.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.app.trustChat.R;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
      //  crashNow();
        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.loginFragment);
    }

}
