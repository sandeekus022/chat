package com.app.trustChat.ui.login;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import com.app.trustChat.BR;
import com.app.trustChat.R;
import com.app.trustChat.base.BaseFragment;
import com.app.trustChat.databinding.LoginFragmentBinding;
import com.app.trustChat.model.LoginModel;
import com.app.trustChat.model.WrapperModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.main.MainActivity;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.utils.MainUtils;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class LoginFragment extends BaseFragment<LoginFragmentBinding, LoginViewModel>
        implements LoginNavigator {

    private LoginViewModel mViewModel;
    private View v;

    @Override
    public int getLayoutId() {
        return R.layout.login_fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public LoginViewModel getViewModel() {
        if (mViewModel == null)
            mViewModel = new LoginViewModel();
        mViewModel.setNavigator(this);
        return mViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefManager.getInstance(getContext())
                .set(SharedPrefManager.Key.ALREADY_REGISTETED, false);
        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.PROFILE_PIC, "");
        SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.SESSION, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        v = view;
    }


    public void openVerificationFragment() {
        String countryCode = getViewDataBinding().tvCountryCode.getSelectedCountryCode();
        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.COUNTRY_CODE, countryCode);
        Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_otpVerificationFragment);
        //Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_profileFragment);
    }

    @Override
    public void showMobileValidation(String validationMsg) {
        getViewDataBinding().etMobileNumber.requestFocus();
        getViewDataBinding().etMobileNumber.setError(validationMsg);
    }


    @Override
    public void callLoginApi(String mobileNumber, String sponsor) {
        Context context = getContext();
        ProgressDialog.show(getChildFragmentManager());

        CompositeDisposable disposable = new CompositeDisposable();
        Single<Response<LoginModel>> caller = RetrofitBuilder.getService(context)
                .login(mobileNumber, sponsor);
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<LoginModel>>() {
                    @Override
                    public void onSuccess(Response<LoginModel> response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        if (response.body().getStatus() == 1 || response.body().getStatus() == 2 || response.body().getStatus() == 5) {
                            SharedPrefManager.getInstance(context)
                                    .set(SharedPrefManager.Key.IS_ADMING, response.body().getIsAdmin());
                            SharedPrefManager.getInstance(context).
                                    set(SharedPrefManager.Key.MOBILE, mobileNumber);
                            MainUtils.showSimpleOkayDialog(context, "Message",
                                    "Please enter OTP sent to your number\n to verify you mobile number",
                                    () -> openVerificationFragment());
                        } else if (response.body().getStatus() == 3) {
                            MainUtils.showSimpleOkayDialog(context, "Message", response.body().getMsg(),
                                    () -> {

                                    });
                        } else if (response.body().getStatus() == 4) {
                            MainUtils.showSimpleOkayDialog(context, "Message", response.body().getMsg(),
                                    () -> {

                                    });
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());
                        if (e instanceof java.net.UnknownHostException) {
                            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }));
    }

    @Override
    public void callReferralCode(String number) {
        getDefaultReferral(number);
    }

    public void getDefaultReferral(String mobileNumber) {
        Single<WrapperModel> caller = RetrofitBuilder.getService(getContext())
                .getDefaultSponsor(mobileNumber);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<WrapperModel>() {
                    @Override
                    public void onSuccess(WrapperModel response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        // mViewDataBinding.etReferrlCode.setText(response.getSponsor());
                        if (response.getStatus().equalsIgnoreCase("Success")) {
                            MainUtils.showSimpleOkayDialog(getContext(), "Message", response.getMsg(),
                                    () -> {
                                        ((MainActivity) getActivity()).finish();
                                    });
                        } else if (response.getStatus().equalsIgnoreCase("Failed")) {
                            MainUtils.showSimpleOkayDialog(getContext(), "Message", response.getMsg(),
                                    () -> {
                                    });
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());
                    }
                }));
    }
}
