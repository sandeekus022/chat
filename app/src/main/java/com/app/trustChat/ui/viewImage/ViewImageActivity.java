package com.app.trustChat.ui.viewImage;

import android.os.Bundle;
import android.widget.ImageView;

import com.app.trustChat.R;
import com.bumptech.glide.Glide;

import androidx.appcompat.app.AppCompatActivity;

public class ViewImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        ImageView imageView  = findViewById(R.id.imageView);
        String url = getIntent().getStringExtra("imageUrl");
        Glide.with(ViewImageActivity.this)
                .load(url)
                .into(imageView);

    }
}
