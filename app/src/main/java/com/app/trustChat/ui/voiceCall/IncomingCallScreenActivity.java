package com.app.trustChat.ui.voiceCall;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.CallLog;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.CallLogRepository;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.services.SinchService;
import com.bumptech.glide.Glide;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.video.VideoCallListener;

import java.util.List;

public class IncomingCallScreenActivity extends BaseActivity {

    static final String TAG = IncomingCallScreenActivity.class.getSimpleName();
    private String mCallId;
    private AudioPlayer mAudioPlayer;
    private boolean mAcceptVideo = true;
    private String userId;
    String callerName;
    String callerPhoto;
    TextView tvUserName;
    ImageView userImage1, mUserLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming);

        ImageView answer = findViewById(R.id.answerButton);
        answer.setOnClickListener(mClickListener);
        ImageView decline = findViewById(R.id.declineButton);
        decline.setOnClickListener(mClickListener);

        mAudioPlayer = new AudioPlayer(this);
        mAudioPlayer.playRingtone();
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
    }

    @Override
    protected void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
            userId = call.getRemoteUserId();
            new GetUserDetailAsync(userId).execute();
            ((TextView) findViewById(R.id.yoohooCalling)).setText(getString(call.getDetails().isVideoOffered() ? R.string.video_calling_in : R.string.voice_calling_in));

        } else {
            Log.e(TAG, "Started with invalid callId, aborting");
            finish();
        }
    }

    @Override
    protected void onServiceDisconnected() {

    }

    private void answerClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.answer();
            Intent intent = new Intent(this, CallScreenAudioVideoActivity.class);
            intent.putExtra("CALL_TYPE", "Incoming call");
            intent.putExtra(SinchService.CALL_ID, mCallId);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    private void declineClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended, cause: " + cause.toString());
            if (cause.toString().equals("CANCELED")) {
                CallLog callLog = new CallLog(userId, call.getDetails().getStartedTime(), "missed call", call.getDetails().isVideoOffered(), callerName, callerPhoto);
                new SaveCallLogAsync(callLog).execute();

            }
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
            // Display some kind of icon showing it's a video call
            // and pass it to the CallScreenActivity via Intent and mAcceptVideo
            mAcceptVideo = true;
        }

        @Override
        public void onVideoTrackPaused(Call call) {
            // Display some kind of icon showing it's a video call
        }

        @Override
        public void onVideoTrackResumed(Call call) {
            // Display some kind of icon showing it's a video call
        }
    }

    private OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.answerButton:
                    answerClicked();
                    break;
                case R.id.declineButton:
                    declineClicked();
                    break;
            }
        }
    };


    private class GetUserDetailAsync extends AsyncTask<Void, Void, UserModel> {

        String userId;

        public GetUserDetailAsync(String userId) {
            this.userId = userId;
        }

        @Override
        protected UserModel doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            return repository.getUser(userId);
        }

        @Override
        protected void onPostExecute(UserModel userModel) {
            super.onPostExecute(userModel);
            if (userModel != null) {
                tvUserName = findViewById(R.id.tvUserName);
                userImage1 = findViewById(R.id.userImage1);
                mUserLogo = findViewById(R.id.mUserLogo);
                tvUserName.setText(userModel.getUsernm());
                Glide.with(getApplicationContext()).load(userModel.getUserphoto())
                        .into(userImage1);

                Glide.with(getApplicationContext()).load(userModel.getUserphoto())
                        .into(mUserLogo);
                mUserLogo.setClipToOutline(true);
                callerName = userModel.getUsernm();
                callerPhoto = userModel.getUserphoto();
            }
        }
    }

    private class SaveCallLogAsync extends AsyncTask<Void, Void, Long> {

        CallLog log;

        public SaveCallLogAsync(CallLog log) {
            this.log = log;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            CallLogRepository repo = new CallLogRepository(MyApplication.getInstance());
            return repo.insertLog(log);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            mAudioPlayer.stopRingtone();
            finish();
        }
    }
}
