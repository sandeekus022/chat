package com.app.trustChat.ui.groupProfileDetails;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupDetailModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.MediaUploadModel;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.networking.APICallbackListener;
import com.app.trustChat.networking.ApiError;
import com.app.trustChat.networking.GenericApiCaller;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.chat.ChatActivity;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.chatRoom.ChatRoomFragment;
import com.app.trustChat.ui.groupChat.AddUsersToGroupActivity;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.ui.singleChatProfileDetails.SingleProfileDetailActivity;
import com.app.trustChat.ui.userProfile.UserProfileActivity;
import com.app.trustChat.utils.BitmapUtils;
import com.app.trustChat.utils.FileUtil;
import com.app.trustChat.utils.HeaderView;
import com.app.trustChat.utils.Util9;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.common.reflect.TypeToken;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class GroupDetailsActivity extends AppCompatActivity implements MessageCallback {
    private UserModel userModel;
    public static final int GALLERY_PICK_CODE = 10;
    public static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private RecyclerView rvAddParticipants;
    private String roomId;
    private Map<String, GroupUser> users = new HashMap<>();
    private RelativeLayout rlExit;
    private RelativeLayout rlAddParticipants;
    private boolean isGroupMember;
    private boolean isAdmin;
    private AddParticipantsAdapter adapter;
    private CardView cvDeleteGroup;
    private List<GroupUser> arrayList = new ArrayList<>();
    TextView tvTotalParticipants, name, status;
    ImageView ivProfileImage, ivDefault, ivCamera;
    LinearLayout llBack;
    private String mTempPhotoPath;
    private String currentPhotoPath;
    private String gruopPicURl;
    private List<UserModel> appUsers = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);
        findByViewIds();
        roomId = getIntent().getStringExtra("roomId");
        userModel = MyApplication.getInstance().getCurrentUser();
        getAppUsers();
    }

    void getAppUsers() {
        String listGson = SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USERS_LIST, null);
        List<UserModel> list = new Gson().fromJson(listGson, new TypeToken<ArrayList<UserModel>>() {
        }.getType());
        for (UserModel user : list) {
            if (user.getEmail() != null)
                appUsers.add(user);
        }

    }


    private void findByViewIds() {
        name = findViewById(R.id.name);
        status = findViewById(R.id.status);
        llBack = findViewById(R.id.llBack);
        cvDeleteGroup = findViewById(R.id.cvDeleteGroup);
        ivCamera = findViewById(R.id.ivCamera);
        rlExit = findViewById(R.id.rlExit);
        rvAddParticipants = findViewById(R.id.rvAddParticipants);
        rlAddParticipants = findViewById(R.id.rlAddParticipants);
        ivProfileImage = findViewById(R.id.ivProfileImage);
        ivDefault = findViewById(R.id.ivDefault);
        tvTotalParticipants = findViewById(R.id.tvTotalParticipants);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvAddParticipants.setLayoutManager(layoutManager);
        adapter = new AddParticipantsAdapter(arrayList, this);
        rvAddParticipants.setAdapter(adapter);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rlAddParticipants.setOnClickListener(v -> {
            Intent intent = new Intent(GroupDetailsActivity.this, AddUsersToGroupActivity.class);
            intent.putExtra("roomId", roomId);
            startActivity(intent);
        });
        ivCamera.setOnClickListener(v -> openDialogChooser());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void exitFromGroup(View view) {
        ProgressDialog.show(getSupportFragmentManager());
        users.remove(userModel.getUserId());
        new LeftGroupAsync().execute();
    }

    public void deleteGroup(View view) {
        ProgressDialog.show(getSupportFragmentManager());
        new DeleteGroupAsync().execute();
    }

    public class AddParticipantsAdapter extends RecyclerView.Adapter<AddParticipantsAdapter.ViewHolder> {
        private List<GroupUser> listCategoryList;
        Context context;


        public AddParticipantsAdapter(List<GroupUser> listCategoryList, Context context) {
            this.listCategoryList = listCategoryList;
            this.context = context;
        }

        @Override
        public AddParticipantsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_group_members, parent, false);
            AddParticipantsAdapter.ViewHolder viewHolder = new AddParticipantsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull AddParticipantsAdapter.ViewHolder holder, int position) {
            GroupUser packageModel = listCategoryList.get(position);
            holder.setData(context, packageModel);
            holder.itemView.setOnClickListener(v -> {
                if (!listCategoryList.get(position).getId().equalsIgnoreCase(userModel.getUserId()))
                    showOption(listCategoryList.get(position));
            });

            holder.itemView.setOnLongClickListener(v -> {
                if (!listCategoryList.get(position).getId().equalsIgnoreCase(userModel.getUserId()))
                    showOption(listCategoryList.get(position));
                return true;
            });
        }

        @Override
        public int getItemCount() {

            return listCategoryList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView image;
            private TextView tvParticipantsName, tvAbout, tvGroupAdmin;


            ViewHolder(@NonNull View itemView) {
                super(itemView);
                tvGroupAdmin = itemView.findViewById(R.id.tvGroupAdmin);
                tvParticipantsName = itemView.findViewById(R.id.tvParticipantsName);
                tvAbout = itemView.findViewById(R.id.tvAbout);
                image = itemView.findViewById(R.id.ivProfile);
            }

            private void setData(Context context, GroupUser groupUser) {
                tvParticipantsName.setText(groupUser.getMobile());
                for (UserModel userModel : appUsers) {
                    if (userModel.getUserId().equalsIgnoreCase(groupUser.getId())) {
                        groupUser.setName(userModel.getUsernm());
                        groupUser.setPhoto(userModel.getUserphoto());
                        tvParticipantsName.setText(groupUser.getName());
                        break;
                    }
                }

                if(groupUser.getId().equalsIgnoreCase(userModel.getUserId())){
                    tvParticipantsName.setText("You");
                }


                tvAbout.setText(groupUser.getMood());
                String imageUrl = groupUser.getPhoto();
                imageUrl = imageUrl.replaceAll(" ", "%20");
                if (groupUser.getPhoto() != null) {
                    Glide.with(context).load(imageUrl)
                            .into(image);
                }
                if (groupUser.isAdmin()) {
                    tvGroupAdmin.setVisibility(View.VISIBLE);
                } else {
                    tvGroupAdmin.setVisibility(View.GONE);
                }
            }

        }

    }

    public void showOption(GroupUser user) {
        Dialog dialog = new Dialog(GroupDetailsActivity.this, R.style.Theme_Dialog);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_room_user_options, null);
        TextView tvStartChat = v.findViewById(R.id.tvStartChat);
        TextView tvViewContact = v.findViewById(R.id.tvViewContact);
        TextView tvRemoveUser = v.findViewById(R.id.tvRemoveUser);
        TextView tvMakeAdmin = v.findViewById(R.id.tvMakeAdmin);

        if (!isGroupMember) {
            tvRemoveUser.setVisibility(View.GONE);
            tvMakeAdmin.setVisibility(View.GONE);
        }
        if (isAdmin) {
            tvRemoveUser.setVisibility(View.VISIBLE);
            tvMakeAdmin.setVisibility(View.VISIBLE);
        } else {
            tvRemoveUser.setVisibility(View.GONE);
            tvMakeAdmin.setVisibility(View.GONE);
        }
        if (user.isAdmin()) {
            tvMakeAdmin.setVisibility(View.GONE);
        }
        tvStartChat.setText(MessageFormat.format("Message {0}", user.getName()));
        tvViewContact.setText(MessageFormat.format("View {0}", user.getName()));
        tvRemoveUser.setText(MessageFormat.format("Remove {0}", user.getName()));
        dialog.setContentView(v);
        dialog.show();
        tvMakeAdmin.setOnClickListener(v13 -> {
            makeAdmin(user);
            dialog.dismiss();
        });
        tvRemoveUser.setOnClickListener(v12 -> {
            removeUserFromRoom(user);
            dialog.dismiss();
        });
        tvStartChat.setOnClickListener(v1 -> {
            Intent intent = new Intent(GroupDetailsActivity.this, ChatActivity.class);
            intent.putExtra("roomTitle", user.getName());
            intent.putExtra("roomId", getRoomId(user.getId()));
            intent.putExtra("roomImage", user.getPhoto());
            intent.putExtra("toUid", user.getId());
            intent.putExtra("subject", "");
            intent.putExtra("LAST_MSG", "");
            dialog.dismiss();
            startActivity(intent);
        });
        tvViewContact.setOnClickListener(v12 -> {
            Intent intent = new Intent(GroupDetailsActivity.this, SingleProfileDetailActivity.class);
            intent.putExtra("toUid", user.getId());
            intent.putExtra("isMe", false);
            dialog.dismiss();
            startActivity(intent);
        });
    }

    public void makeAdmin(GroupUser groupUser) {
        ProgressDialog.show(getSupportFragmentManager());
        Map<String, GroupUser> newMap = new HashMap<>();
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
            if (gUser.getId().equalsIgnoreCase(groupUser.getId())) {
                gUser.setAdmin(true);
            }
            newMap.put(gUser.getId(), gUser);
        }

        users.clear();
        users.putAll(newMap);
        new LeftGroupAsync().execute();
    }


    public void removeUserFromRoom(GroupUser groupUser) {
        ProgressDialog.show(getSupportFragmentManager());
        Map<String, GroupUser> newMap = new HashMap<>();
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
            if (!gUser.getId().equalsIgnoreCase(groupUser.getId())) {
                newMap.put(gUser.getId(), gUser);
            }
        }

        users.clear();
        users.putAll(newMap);
        new LeftGroupAsync().execute();
    }

    class LeftGroupAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            chatRoom.setMembers(new Gson().toJson(users));
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel roomModel) {
            super.onPostExecute(roomModel);
            updateRoom(roomModel);
        }
    }

    private void updateRoom(ChatRoomModel roomModel) {
        ChatRoomService.updateGroupOnline(GroupDetailsActivity.this, roomModel, false, this);
        getRoomDetails();
    }

    @Override
    public void onSuccess(Message message) {
        ProgressDialog.hide(getSupportFragmentManager());
        getRoomDetails();
    }

    private String getRoomId(String userId) {
        Integer myId = Integer.valueOf(userModel.getUserId());
        Integer opponentId = Integer.valueOf(userId);
        return myId > opponentId ? myId + "_" + opponentId : opponentId + "_" + myId;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRoomDetails();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }


    private void getRoomDetails() {
        FirebaseFirestore.getInstance().collection("rooms")
                .document(roomId).addSnapshotListener((documentSnapshot, e) -> {
            GroupDetailModel object = documentSnapshot.toObject(GroupDetailModel.class);
            users = (Map<String, GroupUser>) documentSnapshot.get("members");
            object.setCreatedAt((Long) documentSnapshot.get("createdAt"));
            setAddParticipants(users, object);
        });
    }

    private void setAddParticipants(Map<String, GroupUser> users, GroupDetailModel object) {
        arrayList.clear();
        adapter.notifyDataSetChanged();
        isGroupMember = false;
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
            if (groupUser.getId().equalsIgnoreCase(userModel.getUserId())) {
                isGroupMember = true;
            }
            arrayList.add(groupUser);
        }
        if (!isGroupMember) {
            cvDeleteGroup.setVisibility(View.VISIBLE);
            rlExit.setVisibility(View.GONE);
            rlAddParticipants.setVisibility(View.GONE);
        } else {
            cvDeleteGroup.setVisibility(View.GONE);
            rlExit.setVisibility(View.VISIBLE);
            rlAddParticipants.setVisibility(View.VISIBLE);
        }
        for (GroupUser user : arrayList) {
            if (user.isAdmin() && user.getId().equalsIgnoreCase(userModel.getUserId())) {
                isAdmin = true;
                break;
            }
        }
        if (!isAdmin) {
            rlAddParticipants.setVisibility(View.GONE);
        }

        String imageUrl = object.getPhoto();
        imageUrl = imageUrl.replaceAll(" ", "%20");
        adapter.notifyDataSetChanged();
        Log.e("Group", "pic---" + object.getPhoto());
        if (object.getPhoto().isEmpty()) {
            ivDefault.setVisibility(View.VISIBLE);
            ivProfileImage.setVisibility(View.GONE);
        } else {
            ivDefault.setVisibility(View.GONE);
            ivProfileImage.setVisibility(View.VISIBLE);
            try {
                Glide.with(getApplicationContext()).load(imageUrl)
                        .into(ivProfileImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        tvTotalParticipants.setText(arrayList.size() + " Participants");


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        if (object.getCreatedAt() != null) {
            name.setText(object.getTitle());
            status.setText(object.getSubject() + " " + dateFormat.format(new Date(object.getCreatedAt())));
        }
    }

    private class DeleteGroupAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            repository.deleteChatRoomById(roomId);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ProgressDialog.hide(getSupportFragmentManager());
            Intent intent = new Intent(GroupDetailsActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void openDialogChooser() {
        final Dialog registerUser = new Dialog(this);
        registerUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerUser.setContentView(R.layout.chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(registerUser.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        registerUser.getWindow().setAttributes(lp);
        registerUser.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        registerUser.setCancelable(false);
        registerUser.show();

        LinearLayout galleryId = registerUser.findViewById(R.id.llGallery);
        LinearLayout cameraId = registerUser.findViewById(R.id.llCamera);
        RelativeLayout relativeLayout = registerUser.findViewById(R.id.rlDailog);

        cameraId.setOnClickListener(v -> {
            registerUser.dismiss();
            launchCamera();
        });

        galleryId.setOnClickListener(v -> {
            registerUser.dismiss();
            startActivityForResult(Intent.createChooser(getGalleryPickerIntent(), "Select Profile Picture"), GALLERY_PICK_CODE);

        });


        relativeLayout.setOnClickListener(v -> registerUser.dismiss());

    }

    public Intent getGalleryPickerIntent() {

        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        return pictureIntent;
    }

    private void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = BitmapUtils.createTempImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mTempPhotoPath = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(this, BitmapUtils.FILE_PROVIDER_AUTHORITY, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String str = "ImageView";
        if (requestCode == GALLERY_PICK_CODE && resultCode == Activity.RESULT_OK && data != null) {
            try {
                Log.e("UCrop", "data---" + data.getData());
                Uri destinationUri = Uri.fromFile(getImageFile());
                openCropActivity(data.getData(), destinationUri);
            } catch (Exception ex) {
                Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_ACTION_PICK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap mResultsBitmap = BitmapUtils.resamplePic(this, mTempPhotoPath);
            Uri resultUri = Uri.fromFile(new File(BitmapUtils.saveImage(this, mResultsBitmap)));
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                drawable.setCircular(true);
                ivProfileImage.setImageDrawable(drawable);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                File file = FileUtil.from(this, resultUri);
                Glide.with(getApplicationContext()).load(file)
                        .into(ivProfileImage);
                uploadGroupImage(this, file);
                ProgressDialog.show(getSupportFragmentManager());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            Uri resultUri = UCrop.getOutput(data);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                drawable.setCircular(true);
                Glide.with(getApplicationContext()).load(drawable)
                        .into(ivProfileImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File file = null;
            try {
                file = FileUtil.from(getApplicationContext(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ProgressDialog.show(getSupportFragmentManager());
            uploadGroupImage(this, file);
            Log.e("UCrop", "resultUri---" + resultUri);

        } else if (resultCode == UCrop.RESULT_ERROR) {
            Throwable cropError = UCrop.getError(data);
            Log.e("UCrop", "cropError---" + cropError);
        }

    }

    private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists()) {
            System.out.println("File exists");
        } else {
            System.out.println("File not exists");
        }
        File file = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }

    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(this, R.color.colorOrange));
        UCrop.of(sourceUri, destinationUri)
                //.withAspectRatio(5f, 5f)
                .start(this);
    }

    private void uploadGroupImage(Context context, File file) {
        String mobileNumber = userModel.getMobile();
        CompositeDisposable disposable = new CompositeDisposable();
        String filename = Util9.getUniqueValue(String.valueOf(userModel.getUserId()));
        MultipartBody.Part doc;
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), filename);
        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        doc = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        Single<Response<MediaUploadModel>> caller = RetrofitBuilder.getService(this)
                .uploadFile(doc, mobile, fileName);

        disposable.add(caller.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<MediaUploadModel>>() {
                    @Override
                    public void onSuccess(Response<MediaUploadModel> mediaUploadModelResponse) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        gruopPicURl = mediaUploadModelResponse.body().getFile();
                        updateRoomImageOnFirebase();
                        new UpdateRoomPic().execute();
                        Glide.with(getApplicationContext()).load(gruopPicURl)
                                .into(ivProfileImage);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());
                    }
                }));
    }

    private void updateRoomImageOnFirebase() {
        DocumentReference roomRef = FirebaseFirestore.getInstance()
                .collection("rooms").document(roomId);
        Map<String, Object> data = new HashMap<>();
        data.put("photo", gruopPicURl);
        roomRef.update(data);
    }

    private class UpdateRoomPic extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ChatRoomRepository repo = new ChatRoomRepository(MyApplication.getInstance());
            repo.updateRoomPhoto(roomId, gruopPicURl);
            return null;
        }
    }
}