package com.app.trustChat.ui.login;

public interface LoginNavigator {
    void showMobileValidation(String validationMsg);
    void callLoginApi(String value, String sponsor);

    void callReferralCode(String value);
}
