package com.app.trustChat.ui.otpVerificationFragment;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import com.app.trustChat.BR;
import com.app.trustChat.R;
import com.app.trustChat.base.BaseFragment;
import com.app.trustChat.constants.UserStatus;
import com.app.trustChat.databinding.OtpVerificationFragmentBinding;
import com.app.trustChat.model.Contacts;
import com.app.trustChat.model.OtpVerificationModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.utils.MainUtils;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class OtpVerificationFragment extends BaseFragment<OtpVerificationFragmentBinding,
        OtpVerificationViewModel> implements OtpVerificationNavigator {

    private OtpVerificationViewModel mViewModel;
    private String mobileNumber;
    private View view;
    private String fcmToken;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFCMToken();
    }

    @Override
    public int getLayoutId() {
        return R.layout.otp_verification_fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public OtpVerificationViewModel getViewModel() {
        if (mViewModel == null)
            mViewModel = new OtpVerificationViewModel();
        mViewModel.setNavigator(this);
        return mViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        mobileNumber = SharedPrefManager.getInstance().get(SharedPrefManager.Key.MOBILE, "");
        String countryCode = SharedPrefManager.getInstance().get(SharedPrefManager.Key.COUNTRY_CODE, "");
        getViewModel().setMobileNumber(mobileNumber);

        getViewDataBinding().tvMobileNumber.setText(String.format(getString(R.string.country_code_mobile_number), countryCode, mobileNumber));
        getViewDataBinding().editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    getViewDataBinding().editText2.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        getViewDataBinding().editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    getViewDataBinding().editText3.requestFocus();
                else if (s.length() <= 0)
                    getViewDataBinding().editText1.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= 0)
                    getViewDataBinding().editText1.requestFocus();
            }
        });
        getViewDataBinding().editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    getViewDataBinding().editText4.requestFocus();
                else if (s.length() == 0)
                    getViewDataBinding().editText2.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0)
                    getViewDataBinding().editText2.requestFocus();
            }
        });
        getViewDataBinding().editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    getViewDataBinding().editText3.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void openProfileFragment() {
        Navigation.findNavController(view).navigate(R.id.action_otpVerificationFragment_to_profileFragment);
    }

    @Override
    public void callVerificationApi(String otp) {
        Context context = getContext();
        ProgressDialog.show(getChildFragmentManager());
        CompositeDisposable disposable = new CompositeDisposable();
        Single<Response<OtpVerificationModel>> caller = RetrofitBuilder.getService(context)
                .verifyLogin(mobileNumber, "MTIzNA==", "1234");
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<OtpVerificationModel>>() {
                    @Override
                    public void onSuccess(Response<OtpVerificationModel> response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        if (response.body().getStatus().equalsIgnoreCase("success")) {


                            SharedPrefManager.getInstance(context).
                                    set(SharedPrefManager.Key.MOBILE, mobileNumber);
                            SharedPrefManager.getInstance(context).
                                    set(SharedPrefManager.Key.USER_ID, response.body().getUserId());

                            SharedPrefManager.getInstance(context).
                                    set(SharedPrefManager.Key.NAME, response.body().getName());

                            SharedPrefManager.getInstance(context).
                                    set(SharedPrefManager.Key.EMAIL, response.body().getEmail());


                            if (response.body().getMsg().equalsIgnoreCase("Please update profile.")) {
                                MainUtils.showSimpleOkayDialog(context, "Message",
                                        "Mobile Number Verified Successfully\n Please set your profile.",
                                        () -> openProfileFragment());

                            } else if (response.body().getMsg().equalsIgnoreCase("Mobile already exists.")) {
                                MainUtils.showSimpleOkayDialog(context, "Message",
                                        response.body().getMsg(),
                                        () -> saveProfile(response));

                            }
                        } else if (response.body().getStatus().equalsIgnoreCase("failed")) {
                            MainUtils.showSimpleOkayDialog(context, "Message",
                                    response.body().getMsg(),
                                    () -> {

                                    });
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));

    }

    private void saveProfile(Response<OtpVerificationModel> response) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        String name = response.body().getProfile().getName();
        String email = response.body().getProfile().getEmail();
        String mobile = response.body().getProfile().getMobile();
        String city = response.body().getProfile().getCity();
        String state = response.body().getProfile().getState();
        String district = response.body().getProfile().getDistrict();
        String image = response.body().getProfile().getDp_img();
        String user_id = response.body().getProfile().getUser_id();

        String uid = SharedPrefManager.getInstance(getContext()).
                get(SharedPrefManager.Key.USER_ID, user_id);
        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.PROFILE_PIC, image);
        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.NAME, name);
        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.EMAIL, email);


        UserModel userModel = new UserModel();
        userModel.setUserId(uid);
        userModel.setUsernm(name);
        userModel.setEmail(email);
        userModel.setMobile(mobile);
        userModel.setToken(fcmToken);
        userModel.setLastSeen(System.currentTimeMillis());
        userModel.setUserphoto(image == null ? "" : image);
        userModel.setUserStatus(UserStatus.ONLINE);
        userModel.setTypingTo(UserStatus.TYPING_TO_NO_ONE);
        userModel.setUsermsg("");
        userModel.setMood("Hi, I am using TrustApp");
        userModel.setState(state);
        userModel.setDistrict(district);
        userModel.setLastSeen(System.currentTimeMillis());
        SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.USER, new Gson().toJson(userModel));
        ProgressDialog.show(getChildFragmentManager());
        db.collection("users").document(uid)
                .set(userModel)
                .addOnCompleteListener(task -> {
                    Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
                })
                .addOnSuccessListener(aVoid -> createSessionAndLoadHomeScreen())
                .addOnFailureListener(e -> Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show());
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            fcmToken = instanceIdResult.getToken();
            Log.e("fcmToken", fcmToken);
        });
    }

    private void createSessionAndLoadHomeScreen() {
        new ContactAsync().execute();
    }

    private class ContactAsync extends AsyncTask<Void, Void, Map<String, Contacts>> {
        @Override
        protected void onPreExecute() {
            ProgressDialog.show(getChildFragmentManager());
            super.onPreExecute();
        }

        @Override
        protected Map<String, Contacts> doInBackground(Void... voids) {
            Map<String, Contacts> nameList = new HashMap<>();
            ContentResolver cr = getContext().getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);
            if ((cur != null ? cur.getCount() : 0) > 0) {
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor crPhones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = ?", new String[]{id}, null);


                        while (crPhones.moveToNext()) {
                            String phone = crPhones.getString(crPhones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String phoneNumber = phone.replaceAll("[^a-zA-Z0-9]", "");
                            if (phoneNumber.length() >= 10) {
                                String mobileNumber = phoneNumber.substring(phoneNumber.length() - 10);
                                nameList.put(mobileNumber, new Contacts(name, mobileNumber));
                            }
                        }
                        crPhones.close();
                    }
                }
            }
            if (cur != null) {
                cur.close();
            }
            return nameList;
        }

        @Override
        protected void onPostExecute(Map<String, Contacts> mobileArray) {
            super.onPostExecute(mobileArray);
            if (mobileArray.size() > 0) {
                FirebaseFirestore.getInstance().collection("users").orderBy("usernm").get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ProgressDialog.hide(getChildFragmentManager());
                        QuerySnapshot documentSnapshot = task.getResult();
                        List<UserModel> list = documentSnapshot.toObjects(UserModel.class);
                        Map<String, UserModel> userMap = new HashMap<>();
                        for (UserModel u : list) {
                            userMap.put(u.getMobile(), u);
                        }
                        List<UserModel> filteredList = new ArrayList<>();
                        for (Contacts user : mobileArray.values()) {
                            if (userMap.containsKey(user.getMobile())) {
                                UserModel userModel = userMap.get(user.getMobile());
                                userModel.setContact(true);
                                userModel.setUsernm(user.getName());
                                filteredList.add(userModel);
                            } else {
                                UserModel userModel = new UserModel();
                                userModel.setUserId(user.getMobile());
                                userModel.setContact(false);
                                userModel.setMobile(user.getMobile());
                                userModel.setUsernm(user.getName());
                                filteredList.add(userModel);
                            }
                        }
                        SharedPrefManager prefManager = SharedPrefManager.getInstance(getContext());
                        prefManager.set(SharedPrefManager.Key.USERS_LIST, new Gson().toJson(filteredList));
                        SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.SESSION, true);
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
            } else {
                SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.SESSION, true);
                Intent intent = new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();
            }

        }
    }

}
