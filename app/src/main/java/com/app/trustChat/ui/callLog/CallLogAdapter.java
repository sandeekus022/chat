package com.app.trustChat.ui.callLog;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.R;
import com.app.trustChat.model.CallLog;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.CallLogViewHolder> {
    List<CallLog> logList;
    SimpleDateFormat format = new SimpleDateFormat("MMM dd, hh:mm:aa", Locale.getDefault());
    private Context context;

    public CallLogAdapter(List<CallLog> logList) {
        this.logList = logList;
    }

    @NonNull
    @Override
    public CallLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_log_call, parent, false);
        context = parent.getContext();
        return new CallLogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallLogViewHolder holder, int position) {
        Glide.with(context).load(logList.get(position).getUserPhoto())
                .placeholder(R.drawable.ic_user)
                .into(holder.ivImage);
        holder.userName.setText(logList.get(position).getUserName());

        Log.e("CallAdapter", "call---" + logList.get(position).getCallType());
        Log.e("CallAdapter", "video call---" + logList.get(position).isVideoCall());
        if (logList.get(position).getCallType().contains("missed call")) {
            holder.icCallType.setImageDrawable(context.getDrawable(R.drawable.ic_missed_call));
        } else if (logList.get(position).getCallType().contains("Incoming")) {
            holder.icCallType.setImageDrawable(context.getDrawable(R.drawable.ic_incoming_call));
        } else {
            holder.icCallType.setImageDrawable(context.getDrawable(R.drawable.ic_outgoing_call));
        }
        if (logList.get(position).isVideoCall()) {
            holder.ivCall.setImageDrawable(context.getDrawable(R.drawable.ic_videocam));
        } else {
            holder.ivCall.setImageDrawable(context.getDrawable(R.drawable.ic_call));
        }

        String time = getDate(logList.get(position).getStartTime());
        holder.tvTime.setText(time);

    }

    @Override
    public int getItemCount() {
        return logList.size();
    }

    public class CallLogViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImage;
        ImageView icCallType, ivCall;
        TextView userName;
        TextView tvTime;

        public CallLogViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            icCallType = itemView.findViewById(R.id.icCallType);
            ivCall = itemView.findViewById(R.id.ivCall);
            userName = itemView.findViewById(R.id.userName);
            tvTime = itemView.findViewById(R.id.tvTime);
        }
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("MMM dd, hh:mm aa", cal).toString();
        return date;
    }
}
