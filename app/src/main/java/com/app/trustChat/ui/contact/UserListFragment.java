package com.app.trustChat.ui.contact;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.Contacts;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.services.UpdateContactsService;
import com.app.trustChat.ui.chat.ChatActivity;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.ui.otpVerificationFragment.OtpVerificationFragment;
import com.app.trustChat.utils.MainUtils;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.reflect.TypeToken;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.UserDataReader;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class UserListFragment extends AppCompatActivity {
    private RecyclerViewAdapter firestoreAdapter;
    private RecyclerView recyclerView;
    List<UserModel> actualList = new ArrayList<>();
    private UserModel currentUser;
    private FloatingActionButton btnRefresh;
    private LinearLayout rlSearch;
    private static List<UserModel> tempList = new ArrayList<>();

    /*private progressBar*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_list_fragment);
        tempList.clear();
        init();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        currentUser = MyApplication.getInstance().getCurrentUser();

        filterContactList();

        setOnClickListener();
    }

    private EditText etSearch;
    private ImageView ivClearSearch;

    private void init() {
        ivClearSearch = findViewById(R.id.ivClearSearch);
        recyclerView = findViewById(R.id.recyclerView);
        btnRefresh =  findViewById(R.id.btnRefresh);
        rlSearch = findViewById(R.id.rlSearch);
        etSearch = findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ivClearSearch.setVisibility(View.VISIBLE);
                } else {
                    ivClearSearch.setVisibility(View.GONE);
                }
                filterList(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setOnClickListener() {
        ivClearSearch.setOnClickListener(v -> etSearch.setText(""));
        findViewById(R.id.ivCloseSearch).setOnClickListener(v -> hideSearchBar());
        findViewById(R.id.ivBack).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.ivSearch).setOnClickListener(v -> {
            showSearchBar();

        });
        btnRefresh.setImageDrawable(getDrawable(R.drawable.ic_sync));
        btnRefresh.show();
        btnRefresh.setOnClickListener(v -> {
            refreshContacts();
        });

    }

    private void refreshContacts() {
        new ContactAsync().execute();
    }

    @Override
    public void onBackPressed() {
        if (isSearchBarVisible) {
            exitReveal(rlSearch);
            isSearchBarVisible = false;
        } else {
            super.onBackPressed();
        }
    }

    private void filterContactList() {
        firestoreAdapter = new RecyclerViewAdapter(actualList);
        recyclerView.setAdapter(firestoreAdapter);
        SharedPrefManager prefManager = SharedPrefManager.getInstance(UserListFragment.this);
        String listGson = prefManager.get(SharedPrefManager.Key.USERS_LIST, null);
        List<UserModel> list = new Gson().fromJson(listGson, new TypeToken<ArrayList<UserModel>>() {
        }.getType());
        tempList.addAll(list);
        actualList.addAll(list);
        Collections.sort(actualList, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
        firestoreAdapter.notifyDataSetChanged();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }


    class RecyclerViewAdapter extends RecyclerView.Adapter<CustomViewHolder> {

        List<UserModel> list;

        RecyclerViewAdapter(List<UserModel> list) {
            this.list = list;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CustomViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_user, parent, false));
        }

        @Override
        public void onBindViewHolder(CustomViewHolder viewHolder, int position) {
            String myUid = SharedPrefManager.getInstance(UserListFragment.this)
                    .get(SharedPrefManager.Key.USER_ID, "");
            final UserModel user = list.get(position);
            viewHolder.user_name.setText(user.getUsernm());
            viewHolder.user_msg.setText("Invite");
            viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
            viewHolder.user_photo.setVisibility(View.GONE);
            viewHolder.tvImageAvatar.setText(String.valueOf(user.getUsernm().charAt(0)));

            if (user.getEmail() != null) {
                if (myUid.equals(user.getUserId())) {
                    viewHolder.itemView.setVisibility(View.INVISIBLE);
                    viewHolder.itemView.getLayoutParams().height = 0;
                    return;
                }
                viewHolder.user_msg.setText(user.getMood() == null ? "Hey I am using TrustApp" : user.getMood());

                if (!user.getUserphoto().isEmpty()) {
                    Glide.with(UserListFragment.this).load(user.getUserphoto())
                            .placeholder(R.drawable.ic_user)
                            .into(viewHolder.user_photo);
                    viewHolder.user_photo.setVisibility(View.VISIBLE);
                    viewHolder.tvImageAvatar.setVisibility(View.GONE);
                } else {
                    viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
                    viewHolder.user_photo.setVisibility(View.GONE);
                    viewHolder.tvImageAvatar.setText(String.valueOf(user.getUsernm().charAt(0)));
                }
            }
            viewHolder.itemView.setOnClickListener(v -> {
                if (list.get(position).getEmail() != null) {
                    Intent intent = new Intent(UserListFragment.this, ChatActivity.class);
                    intent.putExtra("roomImage", user.getUserphoto());
                    intent.putExtra("roomTitle", user.getUsernm());
                    intent.putExtra("roomId", getRoomId(user.getUserId()));
                    intent.putExtra("isGroup", false);
                    intent.putExtra("toUid", user.getUserId());
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("opponentUser", user);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                } else {
                    sendSMS(list.get(position).getMobile());
                }
            });
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }


        private void filterList(String query) {
            if (query.length() > 0) {
                List<UserModel> queryList = new ArrayList<>();
                for (UserModel room : tempList) {
                    if (room.getUsernm() != null)
                        if (room.getUsernm().toLowerCase().contains(query.toLowerCase()))
                            queryList.add(room);
                }
                list.clear();
                list.addAll(queryList);
            } else {
                list.clear();
                list.addAll(tempList);
            }
            Collections.sort(list, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
            notifyDataSetChanged();
        }

    }

    private String getRoomId(String userId) {
        Integer myId = Integer.valueOf(currentUser.getUserId());
        Integer opponentId = Integer.valueOf(userId);
        return myId > opponentId ? myId + "_" + opponentId : opponentId + "_" + myId;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    private static class CustomViewHolder extends RecyclerView.ViewHolder {
        CircleImageView user_photo;
        TextView user_name;
        TextView tvImageAvatar;
        TextView user_msg;

        CustomViewHolder(View view) {
            super(view);
            user_photo = view.findViewById(R.id.user_photo);
            user_name = view.findViewById(R.id.user_name);
            user_msg = view.findViewById(R.id.user_msg);
            tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
        }
    }


    private class ContactDbAsync extends AsyncTask<Void, Void, List<UserModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<UserModel> doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            List<UserModel> list = repository.getUsers();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);
            if (userModels != null && userModels.size() > 0) {
                actualList.clear();
                actualList.addAll(userModels);
                tempList.addAll(userModels);
                Collections.sort(actualList, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
                firestoreAdapter.notifyDataSetChanged();
            }
        }
    }

    public void createChattingRoom(UserModel opponent, DocumentReference room) {

        String roomId = getRoomId(opponent.getUserId());

        Intent intent = new Intent(UserListFragment.this, ChatActivity.class);
        intent.putExtra("subject", currentUser.getUsernm() + " created this group");
        intent.putExtra("roomTitle", opponent.getUsernm());
        intent.putExtra("roomImage", opponent.getUserphoto());
        intent.putExtra("roomId", roomId);
        startActivity(intent);
    }

    private void sendSMS(String number) {
        String sb = "\nLet's chat on Trust App! it's fast, simple and secure app we can use to message and call each other for free. Get it at\n\n" +
                "https://play.google.com/store/apps/details?id=" +
                getPackageName() +
                "\n\nIf you like this application then share with your buddies and support Made in India motive and don’t forget to rate us on play store!";

        Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
        smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.setType("text/plain");
        smsIntent.putExtra("sms_body", sb);
        smsIntent.setData(Uri.parse("sms:" + number));
        startActivity(smsIntent);
    }

    private boolean isSearchBarVisible;

    private void showSearchBar() {
        isSearchBarVisible = true;
        //tabLayout.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterReveal(rlSearch);
        } else {
            rlSearch.setVisibility(View.VISIBLE);
        }
        etSearch.requestFocus();
        if (etSearch.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void enterReveal(View myView) {
        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / 2;

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void exitReveal(View myView) {
        // previously visible view

        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth() / 2;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.GONE);
            }
        });

        // start the animation
        anim.start();
    }


    private void hideSearchBar() {
        etSearch.clearFocus();
        isSearchBarVisible = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            exitReveal(rlSearch);
        } else {
            rlSearch.setVisibility(View.GONE);
        }
        MainUtils.hideKeyboard(this);
    }

    public void filterList(String query) {
        firestoreAdapter.filterList(query);
    }


    private class ContactAsync extends AsyncTask<Void, Void, Map<String, Contacts>> {
        @Override
        protected void onPreExecute() {
            ProgressDialog.show(getSupportFragmentManager());
            super.onPreExecute();
        }

        @Override
        protected Map<String, Contacts> doInBackground(Void... voids) {
            Map<String, Contacts> nameList = new HashMap<>();
            ContentResolver cr = getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);
            if ((cur != null ? cur.getCount() : 0) > 0) {
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor crPhones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = ?", new String[]{id}, null);


                        while (crPhones.moveToNext()) {
                            String phone = crPhones.getString(crPhones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String phoneNumber = phone.replaceAll("[^a-zA-Z0-9]", "");
                            if (phoneNumber.length() >= 10) {
                                String mobileNumber = phoneNumber.substring(phoneNumber.length() - 10);
                                nameList.put(mobileNumber, new Contacts(name, mobileNumber));
                            }
                        }
                        crPhones.close();
                    }
                }
            }
            if (cur != null) {
                cur.close();
            }
            return nameList;
        }

        @Override
        protected void onPostExecute(Map<String, Contacts> mobileArray) {
            super.onPostExecute(mobileArray);
            if (mobileArray.size() > 0) {
                FirebaseFirestore.getInstance().collection("users").orderBy("usernm").get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot documentSnapshot = task.getResult();
                        List<UserModel> list = documentSnapshot.toObjects(UserModel.class);
                        Map<String, UserModel> userMap = new HashMap<>();
                        for (UserModel u : list) {
                            userMap.put(u.getMobile(), u);
                        }
                        List<UserModel> filteredList = new ArrayList<>();
                        for (Contacts user : mobileArray.values()) {
                            if (userMap.containsKey(user.getMobile())) {
                                UserModel userModel = userMap.get(user.getMobile());
                                userModel.setContact(true);
                                userModel.setUsernm(user.getName());
                                filteredList.add(userModel);
                            } else {
                                UserModel userModel = new UserModel();
                                userModel.setUserId(user.getMobile());
                                userModel.setContact(false);
                                userModel.setMobile(user.getMobile());
                                userModel.setUsernm(user.getName());
                                filteredList.add(userModel);
                            }
                        }
                        ProgressDialog.hide(getSupportFragmentManager());
                        SharedPrefManager prefManager = SharedPrefManager.getInstance(UserListFragment.this);
                        prefManager.set(SharedPrefManager.Key.USERS_LIST, new Gson().toJson(filteredList));
                        tempList.clear();
                        actualList.clear();
                        filterContactList();
                    }
                });
            }

        }
    }
}

