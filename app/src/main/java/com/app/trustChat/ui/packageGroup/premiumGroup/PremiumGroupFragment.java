package com.app.trustChat.ui.packageGroup.premiumGroup;


import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.BR;
import com.app.trustChat.R;
import com.app.trustChat.base.BaseFragment;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.constants.MediaMessageStatus;
import com.app.trustChat.constants.MessageStatus;
import com.app.trustChat.constants.MessageType;
import com.app.trustChat.customViews.AttachmentOption;
import com.app.trustChat.customViews.AttachmentOptionsListener;
import com.app.trustChat.customViews.AudioRecordView;
import com.app.trustChat.databinding.PremiumRoomFragmentBinding;
import com.app.trustChat.model.ChatModel;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.Contacts;
import com.app.trustChat.model.Locations;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.PackageModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.model.WrapperModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.services.FileUploaderService;
import com.app.trustChat.ui.AttachContactActivity;
import com.app.trustChat.ui.MediaPlayerActivity;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.ui.packageGroup.adminGroup.AdminGroupFragment;
import com.app.trustChat.ui.viewImage.ViewImageActivity;
import com.app.trustChat.utils.BitmapUtils;
import com.app.trustChat.utils.FileUtil;
import com.app.trustChat.utils.MainUtils;
import com.app.trustChat.utils.Util9;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.WriteBatch;
import com.google.gson.Gson;
import com.vanniktech.emoji.EmojiPopup;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

public class PremiumGroupFragment extends Fragment implements MessageCallback, AudioRecordView.RecordingListener, AttachmentOptionsListener {
    private MediaRecorder mRecorder;
    private ListenerRegistration listenerRegistration;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int PICK_FROM_CAMERA = 1011;
    private static final int PICK_FROM_FILE = 2;
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1111;
    private static final int REQUEST_SEND_LOCATION = 5555;
    private static final int REQUEST_SEND_CONTACT = 6666;
    private static final int REQUEST_TAKE_AUDIO = 4444;
    private static final String FILE_BASE_URL = "http://trustapp.co.in/assets/storage/";
    private RecyclerViewAdapter mAdapter;
    private SimpleDateFormat dateFormatDay = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatHour = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
    private String roomID;
    private String myUid;
    private String userId;
    private UserModel currentUser;
    private List<Message> messageList = new ArrayList<>();
    private ChatRoomModel chatRoomModel = new ChatRoomModel();
    private LinearLayoutManager linearLayoutManager;
    private FirebaseFirestore fireStore = null;
    private EmojiPopup emojIcon;
    private AudioRecordView audioRecordView;
    private long time;
    private View binding;
    private RecyclerView recyclerViewMessages;
    private String mTempPhotoPath;
    private Bitmap mResultsBitmap;
    private String recordingFileUri;
    private File recordingFile;
    private boolean isRecordingCancelled;
    private PremiumGroupViewModel mViewModel;
    private View rootView;
    private List<PackageModel> packageList = new ArrayList<>();
    private RelativeLayout rlViewPackage;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }


    String mobile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roomID = "PremiumGroup";
        UserModel currentUser = new Gson().fromJson(SharedPrefManager.getInstance(getContext())
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        myUid = String.valueOf(currentUser.getUserId());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_chatting, container, false);
        fireStore = FirebaseFirestore.getInstance();
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(getContext())
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        mobile = currentUser.getMobile();

        audioRecordView = new AudioRecordView();
        audioRecordView.initView((FrameLayout) rootView.findViewById(R.id.layoutMain));
        binding = audioRecordView.setContainerView(R.layout.premium_room_fragment);
        rlViewPackage = binding.findViewById(R.id.rlViewPackage);
        audioRecordView.setRecordingListener(this);

        audioRecordView.setAttachmentOptions(AttachmentOption.getDefaultList(), this);

        audioRecordView.removeAttachmentOptionAnimation(false);

        recyclerViewMessages = binding.findViewById(R.id.recyclerViewMessages);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewMessages.setLayoutManager(linearLayoutManager);
        recyclerViewMessages.getItemAnimator().setChangeDuration(0);

        setListener();

        audioRecordView.getEmojiView().setOnClickListener(v -> {
            audioRecordView.hideAttachmentOptionView();
            emojIcon.toggle();
        });

        audioRecordView.getCameraView().setOnClickListener(v -> {
            audioRecordView.hideAttachmentOptionView();
        });

        audioRecordView.getSendView().setOnClickListener(v -> {
            String msg = audioRecordView.getMessageView().getText().toString().trim();
            audioRecordView.getMessageView().setText("");
            sendMessage(msg.trim(), MessageType.MESSAGE_TYPE_TEXT,
                    null, "", "", "", "", null, "");
        });


        emojIcon = EmojiPopup.Builder.fromRootView(audioRecordView.getRootView()).setOnEmojiPopupShownListener(() -> {
            if (audioRecordView.getAttachmentView().getVisibility() == View.VISIBLE) {

            }
        }).build(audioRecordView.getMessageView());
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(getContext())
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        userId = currentUser.getUserId();

        myUid = SharedPrefManager.getInstance(getContext())
                .get(SharedPrefManager.Key.USER_ID, "");
        setView();
        return rootView;
    }

    private void setListener() {
        audioRecordView.getEmojiView().setOnClickListener(v -> {
            audioRecordView.hideAttachmentOptionView();
            emojIcon.toggle();
        });
        getPackageDetails();
        Log.e("Premium", "mobile----" + mobile);
        postSubscription(mobile);
        audioRecordView.getCameraView().setOnClickListener(v -> audioRecordView.hideAttachmentOptionView());
    }

    private void sendMessage(final String msg, String msgType,
                             final ChatModel.FileInfo fileInfo, String mediaType, String mimeType,
                             String contact, String location, File file, String localUrl) {
        Message message = new Message();
        message.setMsg(msg);
        message.setMsgId(currentUser.getUserId() + "_" + System.currentTimeMillis());
        message.setRoomId(roomID);
        message.setUId(myUid);
        message.setName(currentUser.getUsernm());
        message.setCreatedAt(System.currentTimeMillis());
        message.setIsSeen(MessageStatus.UNSENT);
        message.setMsgType(msgType);
        message.setReadUsers("");
        if (!location.isEmpty())
            message.setLocation(location);
        if (!contact.isEmpty())
            message.setContacts(contact);
        if (fileInfo != null) {
            message.setFileName(fileInfo.fileName);
            message.setMediaUrl(fileInfo.fileUrl);
            message.setFileSize(fileInfo.fileSize);
            message.setMediaType(mediaType);
            message.setMimeType(mimeType);
            message.setMediaMessageStatus(MediaMessageStatus.UPLOADED);
            FileUploaderService.uploadFile(getContext(), file, message.getMsgId(), roomID, currentUser.getMobile(), fileInfo.fileName);
        }
        Map<String, Object> messages = new HashMap<>();
        messages.put("msgId", message.getMsgId());
        messages.put("roomId", message.getRoomId());
        messages.put("UId", message.getUId());
        messages.put("name", message.getName());
        messages.put("msg", message.getMsg());
        messages.put("msgType", message.getMsgType());
        messages.put("createdAt", System.currentTimeMillis());
        messages.put("isSeen", MessageStatus.SENT);
        messages.put("contacts", message.getContacts());
        messages.put("locatiom", message.getLocation());
        message.setIsSeen(MessageStatus.SENT);
        List<String> readUsers = new ArrayList<>();
        readUsers.add(message.getUId());
        messages.put("readUsers", new Gson().toJson(readUsers));
        if (message.getFileName() != null) {
            messages.put("fileName", message.getFileName());
            messages.put("fileSize", message.getFileSize());
            messages.put("mediaUrl", message.getMediaUrl());
            messages.put("mediaType", message.getMediaType());
            messages.put("mimeType", message.getMimeType());
            messages.put("mediaMessageStatus", message.getMediaMessageStatus());
        }

        new Handler().post(() -> {
            DocumentReference docRef = fireStore.collection("rooms").document(roomID);
            docRef.get().addOnCompleteListener(task -> {
                if (!task.isSuccessful()) {
                    return;
                }

                WriteBatch batch = fireStore.batch();

                // save last message
                batch.set(docRef, messages, SetOptions.merge());

                // save message
                batch.set(docRef.collection("messages").document(), messages);

                // inc unread message count

                batch.commit();
            });
        });
    }

    private void getUserInfoFromServer(String id) {
        fireStore.collection("users").document(id).get().addOnSuccessListener(documentSnapshot -> {
            UserModel userModel = documentSnapshot.toObject(UserModel.class);
            mAdapter = new RecyclerViewAdapter();
            recyclerViewMessages.setAdapter(mAdapter);
        });
    }

    public void setView() {
        getUserInfoFromServer(myUid);
        recyclerViewMessages.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (mAdapter != null & bottom < oldBottom) {
                final int lastAdapterItem = mAdapter.getItemCount() - 1;
                recyclerViewMessages.post(() -> {
                    int recyclerViewPositionOffset = -1000000;
                    View bottomView = linearLayoutManager.findViewByPosition(lastAdapterItem);
                    if (bottomView != null) {
                        recyclerViewPositionOffset = 0 - bottomView.getHeight();
                    }
                    linearLayoutManager.scrollToPositionWithOffset(lastAdapterItem, recyclerViewPositionOffset);
                });
            }
        });
        rlViewPackage.setOnClickListener(v -> {
            openPackageDialog();
        });

        audioRecordView.getLayoutMessage().setVisibility(View.GONE);
        audioRecordView.getImageViewAudio().setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Message message) {
        int pos = 0;
        if (message != null) {
            for (int i = 0; i < messageList.size(); i++) {
                if (messageList.get(i).getMsgId().equalsIgnoreCase(message.getMsgId())) {
                    messageList.get(i).setIsSeen(message.getIsSeen());
                    pos = i;
                    break;
                }
            }
            mAdapter.notifyItemChanged(pos);
        }
    }

    public void openPackageDialog() {
        MainUtils.getChoosePackage(getContext(), packageList);
    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<MessageViewHolder> {
        private List<Message> messageList;

        RecyclerViewAdapter() {
            messageList = new ArrayList<Message>();
            startListening();
        }

        @Override
        public int getItemViewType(int position) {
            Message message = new Message();
            if (messageList != null)
                message = messageList.get(position);
            if (myUid.equals(message.getUId())) {
                switch (message.getMsgType()) {
                    case "1":
                        return R.layout.item_chatimage_right;
                    case "2":
                        return R.layout.item_chatfile_right;
                    case "3":
                        return R.layout.item_chatvideo_right;
                    case "4":
                        return R.layout.item_chatlocation_right;
                    case "5":
                        return R.layout.item_chat_contact_right;
                    case "6":
                        return R.layout.item_audio_right;
                    default:
                        return R.layout.item_chatmsg_right;
                }
            } else {
                switch (message.getMsgType()) {
                    case "1":
                        return R.layout.item_chatimage_left;
                    case "2":
                        return R.layout.item_chatfile_left;
                    case "3":
                        return R.layout.item_chatvideo_left;
                    case "4":
                        return R.layout.item_chatlocation_left;
                    case "5":
                        return R.layout.item_chat_contact_left;
                    case "6":
                        return R.layout.item_audio_left;
                    default:
                        return R.layout.item_chatmsg_left;
                }
            }
        }

        @NonNull
        @Override
        public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
            return new MessageViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int position) {
            messageViewHolder.itemView.setLongClickable(true);
            final Message message = messageList.get(position);
            if (!message.getUId().equalsIgnoreCase(myUid)) {
                if (chatRoomModel.isGroup()) {
                    messageViewHolder.msg_name.setText(message.getName());
                    messageViewHolder.msg_name.setVisibility(View.VISIBLE);
                } else {
                    messageViewHolder.msg_name.setVisibility(View.GONE);
                }
            }

            if ("1".equalsIgnoreCase(message.getMsgType()) || "2".equalsIgnoreCase(message.getMsgType())
                    || "3".equalsIgnoreCase(message.getMsgType())
                    || "6".equalsIgnoreCase(message.getMsgType())) {
                if (message.getMediaMessageStatus() != null) {
                    switch (message.getMediaMessageStatus()) {
                        case MediaMessageStatus.FAILED:
                        case MediaMessageStatus.IN_PROGRESS:
                            messageViewHolder.mediaProgress.setVisibility(View.VISIBLE);
                            break;
                        case MediaMessageStatus.UPLOADED:
                            messageViewHolder.mediaProgress.setVisibility(View.GONE);
                            break;
                    }
                }
            }

            if ("0".equals(message.getMsgType())) {                                      // text message
                messageViewHolder.msg_item.setText(message.getMsg());
            } else if ("1".equalsIgnoreCase(message.getMsgType())) {                                                                // image transfer
                messageViewHolder.realName = message.getMsg();
                loadThumb(message.getMediaUrl(), messageViewHolder.img_item);
                messageViewHolder.img_item.setOnClickListener(v -> {
                    Intent imageIntent = new Intent(getContext(), ViewImageActivity.class);
                    imageIntent.putExtra("imageUrl", message.getMediaUrl());
                    startActivity(imageIntent);
                });
            } else if ("2".equals(message.getMsgType())) {                                      // file transfer
                messageViewHolder.filename = message.getFileName();
                messageViewHolder.realName = message.getMsg();
                messageViewHolder.img_item
                        .setImageDrawable(FileUtil.getFileDrawable(getContext(), message.getMediaType()));
                messageViewHolder.tvFileName.setText(message.getFileName());
                messageViewHolder.msgBox.setOnClickListener(v -> {
                    Intent doc = new Intent(Intent.ACTION_VIEW);
                    doc.setData(Uri.parse(message.getMediaUrl()));
                    startActivity(doc);
                });
            } else if ("3".equalsIgnoreCase(message.getMsgType())) {
                loadThumb(message.getMediaUrl(), messageViewHolder.img_item);
            } else if ("4".equalsIgnoreCase(message.getMsgType())) {
                loadThumb(message.getMediaUrl(), messageViewHolder.img_item);
                Locations locations = new Gson().fromJson(message.getLocation(), Locations.class);
                messageViewHolder.img_item.setOnClickListener(v -> {
                    //if (locations != null)
                    //showDirections(locations.getLat(), locations.getLng());
                });
            } else if ("5".equals(message.getMsgType())) {
                Contacts contacts = new Gson().fromJson(message.getContacts(), Contacts.class);
                messageViewHolder.tvUserName.setText(contacts.getName());
                messageViewHolder.tvMobile.setText(contacts.getMobile());
                messageViewHolder.tvImageAvatar.setText(String.valueOf(contacts.getName().charAt(0)));
                messageViewHolder.tvSave.setOnClickListener(v -> addAsContactConfirmed(contacts));
            } else if ("6".equalsIgnoreCase(message.getMsgType())) {
                messageViewHolder.seekbar.setEnabled(false);
                messageViewHolder.ivPlay.setOnClickListener(v -> {
                    /*Intent playVideo = new Intent(Intent.ACTION_VIEW);
                    playVideo.setDataAndType(Uri.parse(message.getMediaUrl()), "audio/*");
                    startActivity(playVideo);*/
                    Intent intent = new Intent(getContext(), MediaPlayerActivity.class);
                    intent.putExtra("MEDIA_URL", message.getMediaUrl());
                    startActivity(intent);
                });

            }


            if ("3".equalsIgnoreCase(message.getMsgType())) {
                messageViewHolder.img_item.setOnClickListener(v -> {
                    Intent playVideo = new Intent(Intent.ACTION_VIEW);
                    playVideo.setDataAndType(Uri.parse(message.getMediaUrl()), "video/mp4");
                    startActivity(playVideo);
                });
            }


            if (message.getIsSeen().equalsIgnoreCase(MessageStatus.UNSENT)) {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_unsent));
            } else if (message.getIsSeen().equalsIgnoreCase(MessageStatus.SENT)) {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_delivered));
            } else if (message.getIsSeen().equalsIgnoreCase(MessageStatus.DELIVERED)) {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_delivered));
            } else {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_read));
            }

            messageViewHolder.divider.setVisibility(View.INVISIBLE);
            messageViewHolder.divider.getLayoutParams().height = 0;
            messageViewHolder.timestamp.setText("");
            if (message.getCreatedAt() == null) {
                return;
            }

            String day = dateFormatDay.format(message.getCreatedAt());
            String timestamp = dateFormatHour.format(message.getCreatedAt());
            messageViewHolder.timestamp.setText(timestamp);

            if (position == 0) {
                messageViewHolder.divider_date.setText(day);
                messageViewHolder.divider.setVisibility(View.VISIBLE);
                messageViewHolder.divider.getLayoutParams().height = 60;
            } else {
                Message beforeMsg = messageList.get(position - 1);
                String beforeDay = dateFormatDay.format(beforeMsg.getCreatedAt());

                if (!day.equals(beforeDay)) {
                    messageViewHolder.divider_date.setText(day);
                    messageViewHolder.divider.setVisibility(View.VISIBLE);
                    messageViewHolder.divider.getLayoutParams().height = 60;
                }
            }

            if (message.isSelected()) {
                messageViewHolder.itemView.setBackgroundColor(Color.parseColor("#2CFF5722"));
            } else {
                messageViewHolder.itemView.setBackgroundColor(Color.parseColor("#00FFFFFF"));

            }


        }

        @Override
        public int getItemCount() {
            return messageList != null ? messageList.size() : 0;
        }

        public void startListening() {
            if (messageList != null)
                messageList.clear();

            CollectionReference roomRef = fireStore.collection("rooms").document(roomID).collection("messages");
            // my chatting room information
            listenerRegistration = roomRef.orderBy("createdAt").addSnapshotListener((documentSnapshots, e) -> {
                if (e != null) {
                    return;
                }

                Message message;
                for (DocumentChange change : documentSnapshots.getDocumentChanges()) {
                    switch (change.getType()) {
                        case ADDED:
                            message = change.getDocument().toObject(Message.class);
                            if (messageList != null)
                                messageList.add(message);
                            notifyItemInserted(change.getNewIndex());
                            break;
                        case MODIFIED:
                            message = change.getDocument().toObject(Message.class);
                            if (messageList != null)
                                messageList.set(change.getOldIndex(), message);
                            notifyItemChanged(change.getOldIndex());
                            break;
                        case REMOVED:
                            if (messageList != null)
                                messageList.remove(change.getOldIndex());
                            notifyItemRemoved(change.getOldIndex());
                            break;
                    }
                }
                if (messageList != null)
                    recyclerViewMessages.scrollToPosition(messageList.size() - 1);
            });
        }
    }

    public void addAsContactConfirmed(Contacts person) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, person.getName());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, person.getMobile());
        startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private static class MessageViewHolder extends RecyclerView.ViewHolder {
        ProgressBar mediaProgress;
        ImageView user_photo;
        RelativeLayout msgBox;
        TextView msg_item;
        TextView tvFileName;
        VideoView videoView;
        ImageView img_item;
        ImageView ivMsgSeen;
        TextView msg_name;
        TextView timestamp;
        LinearLayout divider;
        TextView divider_date;
        ImageView ivPlay;
        TextView tvImageAvatar;
        TextView tvUserName;
        TextView tvMobile;
        TextView tvSave;
        String filename;
        String realName;
        SeekBar seekbar;
        ImageView ivForward;

        MessageViewHolder(View view) {
            super(view);
            mediaProgress = view.findViewById(R.id.mediaProgress);
            ivForward = view.findViewById(R.id.ivForward);
            msgBox = view.findViewById(R.id.msgBox);
            ivPlay = view.findViewById(R.id.ivPlay);
            user_photo = view.findViewById(R.id.user_photo);
            seekbar = view.findViewById(R.id.seekbar);
            msg_item = view.findViewById(R.id.msg_item);
            videoView = view.findViewById(R.id.videoView);
            img_item = view.findViewById(R.id.img_item);
            ivMsgSeen = view.findViewById(R.id.ivMsgSeen);
            timestamp = view.findViewById(R.id.timestamp);
            tvFileName = view.findViewById(R.id.tvFileName);
            msg_name = view.findViewById(R.id.msg_name);
            divider = view.findViewById(R.id.divider);
            divider_date = view.findViewById(R.id.divider_date);
            tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvMobile = view.findViewById(R.id.tvMobile);
            tvSave = view.findViewById(R.id.tvSave);
        }
    }


    private void loadThumb(String url, ImageView view) {
        Glide.with(getContext())
                .load(url)
                .apply(new RequestOptions().override(1000, 1000))
                .into(view);
    }


    @Override
    public void onClick(AttachmentOption attachmentOption) {
        audioRecordView.hideAttachmentOptionView();
        Intent intent = new Intent();
        switch (attachmentOption.getId()) {
            case AttachmentOption.DOCUMENT_ID:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("*/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_FILE);
                break;
            case AttachmentOption.CAMERA_ID:
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);
                break;
            case AttachmentOption.GALLERY_ID:
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_FROM_ALBUM);
                break;
            case AttachmentOption.AUDIO_ID:
                Intent intent_upload = new Intent();
                intent_upload.setType("audio/*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload, REQUEST_TAKE_AUDIO);
                break;
            case AttachmentOption.LOCATION_ID:
                //openPlacePicker();
                break;
            case AttachmentOption.CONTACT_ID:
                attachContacts();
                break;
        }
    }

    private void attachContacts() {
        Intent intent = new Intent(getContext(), AttachContactActivity.class);
        startActivityForResult(intent, REQUEST_SEND_CONTACT);
    }

    @Override
    public void onRecordingStarted() {
        startRecording();
    }

    @Override
    public void onRecordingLocked() {
    }

    @Override
    public void onRecordingCompleted() {
        int recordTime = (int) ((System.currentTimeMillis() / (1000)) - time);
        if (recordTime > 5)
            stopRecording();
        else {
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            } catch (Exception e) {
                Log.e("TAG", e.getLocalizedMessage());
            }
        }

    }

    @Override
    public void onRecordingCanceled() {
        isRecordingCancelled = true;
    }

    private void startRecording() {
        recordingFileUri = getContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC).getAbsolutePath() + "/trustApp";
        File recordingDir = new File(recordingFileUri);
        boolean success = true;
        if (!recordingDir.exists()) {
            success = recordingDir.mkdirs();
        }
        if (success) {
            recordingFileUri += "/trustChat_Recording" + currentUser.getUserId() + "_" + System.currentTimeMillis() + ".wav";
            recordingFile = new File(recordingFileUri);
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile(recordingFile.getAbsolutePath());
            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Toast.makeText(getContext(), "prepare() failed", Toast.LENGTH_SHORT).show();
            }
            mRecorder.start();
        } else {
            Toast.makeText(getContext(), "Unable to create file", Toast.LENGTH_SHORT).show();
        }
    }

    private void stopRecording() {
        try {
            mRecorder.stop();
            if (!isRecordingCancelled)
                sendAudioMessage();
        } catch (Exception e) {
            recordingFile.delete();  //you must delete the outputfile when the recorder stop failed.

        } finally {
            mRecorder.release();
            mRecorder = null;
        }

    }

    private void sendAudioMessage() {
        String filename = Util9.getUniqueValue(userId);
        Uri fileUri = Uri.fromFile(new File(recordingFileUri));
        File file = null;
        ChatModel.FileInfo fileInfo = getFileDetailFromUri(getContext(), fileUri);
        String mimeType = null;
        try {
            file = FileUtil.from(getContext(), fileUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (fileUri != null)
            mimeType = FileUtil.getMimeType(fileUri);
        String getSubFolder = FileUtil.getSubFolder(mimeType);
        assert file != null;
        String fileExt = file.getPath().substring(file.getPath().lastIndexOf("."));

        assert fileInfo != null;
        fileInfo.fileUrl = FILE_BASE_URL + getSubFolder + filename + fileExt;
        fileInfo.fileName = filename;
        sendMessage("Audio", MessageType.MESSAGE_TYPE_AUDIO, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
    }

    private static ChatModel.FileInfo getFileDetailFromUri(final Context context, final Uri uri) {
        if (uri == null) {
            return null;
        }

        ChatModel.FileInfo fileDetail = new ChatModel.FileInfo();
        // File Scheme.
        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            File file = new File(String.valueOf(uri));
            fileDetail.fileName = file.getName();
            fileDetail.fileSize = Util9.size2String(file.length());
        } else if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            Cursor returnCursor =
                    context.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null && returnCursor.moveToFirst()) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                fileDetail.fileName = returnCursor.getString(nameIndex);
                fileDetail.fileSize = Util9.size2String(returnCursor.getLong(sizeIndex));
                returnCursor.close();
            }
        }

        return fileDetail;
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_SEND_CONTACT) {
            List<UserModel> contactList = new ArrayList<>();
            contactList.addAll(data.getExtras().getParcelableArrayList("CONTACT_LIST"));
            for (UserModel userModel : contactList) {
                Contacts contacts = new Contacts(userModel.getUsernm(), userModel.getMobile());
                String contact = new Gson().toJson(contacts);
                sendMessage("Contact", MessageType.MESSAGE_TYPE_CONTACT, null, "", "", contact, "", null, "");
            }
            return;
        }

        File file = null;
        ChatModel.FileInfo fileInfo = null;
        String filename = Util9.getUniqueValue(userId);
        String mimeType = null;
        Uri fileUri = null;

        if (requestCode == PICK_FROM_CAMERA) {
            mResultsBitmap = BitmapUtils.resamplePic(getContext(), mTempPhotoPath);
            fileUri = Uri.fromFile(new File(BitmapUtils.saveImage(getContext(), mResultsBitmap)));

            fileInfo = getFileDetailFromUri(getContext(), fileUri);
            try {
                file = FileUtil.from(getContext(), fileUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (fileUri != null)
                mimeType = FileUtil.getMimeType(fileUri);
        }
        if (requestCode == PICK_FROM_ALBUM || requestCode == PICK_FROM_FILE ||
                requestCode == REQUEST_TAKE_GALLERY_VIDEO || requestCode == REQUEST_TAKE_AUDIO) {
            fileUri = data.getData();
            fileInfo = getFileDetailFromUri(getContext(), fileUri);
            try {
                file = FileUtil.from(getContext(), fileUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (fileUri != null)
                mimeType = FileUtil.getMimeType(fileUri);
        }

        String getSubFolder = FileUtil.getSubFolder(mimeType);
        assert file != null;
        String fileExt = file.getPath().substring(file.getPath().lastIndexOf("."));

        assert fileInfo != null;
        fileInfo.fileUrl = FILE_BASE_URL + getSubFolder + filename + fileExt;
        fileInfo.fileName = filename;
        if (requestCode == PICK_FROM_ALBUM || requestCode == PICK_FROM_CAMERA) {
            sendMessage("Image", MessageType.MESSAGE_TYPE_IMAGE, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
        } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
            sendMessage("Video", MessageType.MESSAGE_TYPE_VIDEO, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
        } else if (requestCode == REQUEST_TAKE_AUDIO) {
            sendMessage("Audio", MessageType.MESSAGE_TYPE_AUDIO, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
        } else {
            sendMessage("Attachment", MessageType.MESSAGE_TYPE_FILE, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
        }
    }


    public void getPackageDetails() {
        ProgressDialog.show(getChildFragmentManager());
        Single<List<PackageModel>> caller = RetrofitBuilder.getService(getContext())
                .getPackageDetails();
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<PackageModel>>() {
                    @Override
                    public void onSuccess(List<PackageModel> response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        packageList.clear();
                        packageList.addAll(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());
                    }
                }));
    }

    private void postSubscription(String mobile) {
        // ProgressDialog.show(getChildFragmentManager());
        Single<WrapperModel> caller = RetrofitBuilder.getService(getContext())
                .getSubscriptionDetails(mobile);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<WrapperModel>() {
                    @Override
                    public void onSuccess(WrapperModel response) {
                        ProgressDialog.hide(getChildFragmentManager());

                        if (response.getIs_premium().equalsIgnoreCase("0")) {
                            MainUtils.showSimpleOkayDialog(getContext(), "Alert",
                                    response.getDetails().getMsg(), () -> {
                                        audioRecordView.getLayoutMessage().setVisibility(View.GONE);
                                        audioRecordView.getImageViewAudio().setVisibility(View.GONE);
                                        rlViewPackage.setVisibility(View.VISIBLE);
                                    });

                        } else if (response.getIs_premium().equalsIgnoreCase("1")) {
                            MainUtils.showSimpleOkayDialog(getContext(), "Alert", response.getDetails().getMsg(), () -> {
                                rlViewPackage.setVisibility(View.GONE);
                                audioRecordView.getLayoutMessage().setVisibility(View.VISIBLE);
                                audioRecordView.getImageViewAudio().setVisibility(View.VISIBLE);
                            });
                        } else if (response.getIs_premium().equalsIgnoreCase("2")) {
                            MainUtils.showSimpleOkayDialog(getContext(), "Alert", response.getDetails().getMsg(), () -> {
                                audioRecordView.getLayoutMessage().setVisibility(View.GONE);
                                audioRecordView.getImageViewAudio().setVisibility(View.GONE);
                                rlViewPackage.setVisibility(View.VISIBLE);
                            });
                        } else {
                            audioRecordView.getLayoutMessage().setVisibility(View.GONE);
                            audioRecordView.getImageViewAudio().setVisibility(View.GONE);
                            rlViewPackage.setVisibility(View.VISIBLE);
                        }
                    }


                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());

                    }
                }));
    }

}
