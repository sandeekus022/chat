package com.app.trustChat.ui.singleChatProfileDetails;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupDetailModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.utils.HeaderView;
import com.app.trustChat.utils.MainUtils;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SingleChatProfileDeatilsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, MessageCallback {

    private HeaderView toolbarHeaderView;
    private HeaderView floatHeaderView;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private ImageView edit;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private boolean isHideToolbarView = false;
    private TextView description;
    private Map<String, GroupUser> users = new HashMap<>();
    private boolean isGroupMember;
    private UserModel currentUser;
    private String title;
    private String roomId;
    private boolean isBlocked;
    private TextView tvBlock;//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chat_profile_deatils);
        title = getIntent().getStringExtra("title");
        roomId = getIntent().getStringExtra("roomId");
        findByViewIds();
        getRoomDetails();
        getProfile(getIntent().getStringExtra("toUid"));
        currentUser = MyApplication.getInstance().getCurrentUser();
    }

    private void getProfile(String toUid) {
        FirebaseFirestore.getInstance().collection("users")
                .document(toUid).addSnapshotListener((documentSnapshot, e) -> {
            UserModel userModel = documentSnapshot.toObject(UserModel.class);
            setUserDetails(users, userModel);
        });
    }


    private void findByViewIds() {
        tvBlock = findViewById(R.id.tvBlock);
        toolbarHeaderView = findViewById(R.id.toolbar_header_view);
        edit = findViewById(R.id.edit);
        floatHeaderView = findViewById(R.id.float_header_view);
        appBarLayout = findViewById(R.id.appbar);
        toolbar = findViewById(R.id.toolbar);
        description = findViewById(R.id.description);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        /*Replace with own custom image */
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.photo);
        int color = getDominantColor(icon);
        collapsingToolbarLayout.setContentScrimColor(color);
        collapsingToolbarLayout.setStatusBarScrimColor(color);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(darker(color, 0.8f));
        }
        /*Replace with own custom image */
        toolbar.setNavigationOnClickListener(v -> onBackPressed());


        findViewById(R.id.cvBlock).setOnClickListener(v -> {
            String msg = isBlocked ? "Unblock " + title + "?" : "Block " + title;
            MainUtils.showYesNoDialog(SingleChatProfileDeatilsActivity.this,
                    "Alert", msg, () -> blockUnblockUser());
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
            edit.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }

    public static int darker(int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb(a,
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

    private int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    private void setUserDetails(Map<String, GroupUser> users, UserModel userModel) {
        appBarLayout.addOnOffsetChangedListener(this);
        toolbarHeaderView.bindTo(title, userModel.getUserStatus());
        floatHeaderView.bindTo(title, userModel.getUserStatus());
        TextView tvSingleAbout = findViewById(R.id.tvSingleAbout);
        TextView tvDate = findViewById(R.id.tvDate);
        TextView tvMobileNumber = findViewById(R.id.tvMobileNumber);
        ImageView ivProfile = findViewById(R.id.ivProfile);
        ImageView ivProfileImage = findViewById(R.id.image);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM YYYY", Locale.getDefault());
        if (userModel.getLastupdate() != null) {
            String time = dateFormat.format(userModel.getLastupdate());
            tvDate.setText(time);
        }
        if (userModel.getMood() != null)
            tvSingleAbout.setText(userModel.getMood());

        tvMobileNumber.setText(userModel.getMobile());

        if (userModel.getUserphoto() != null) {
            Glide.with(getApplicationContext()).load(userModel.getUserphoto())
                    .into(ivProfileImage);

        }
    }

    private void getRoomDetails() {
        FirebaseFirestore.getInstance().collection("rooms")
                .document(roomId).addSnapshotListener((documentSnapshot, e) -> {
            GroupDetailModel object = documentSnapshot.toObject(GroupDetailModel.class);
            users = (Map<String, GroupUser>) documentSnapshot.get("members");
            updateBlock();
        });
    }

    private void updateBlock() {
        if (users != null) {
            for (Object user : users.values()) {
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(user);
                GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
                if (!gUser.getId().equalsIgnoreCase(currentUser.getUserId())) {
                    if (gUser.isBlocked()) {
                        isBlocked = true;
                    } else {
                        isBlocked = false;
                    }
                    break;
                }
            }
        }else{
            findViewById(R.id.cvBlock).setVisibility(View.GONE);
        }
        updateUi();

    }


    private void blockUnblockUser() {
        Map<String, GroupUser> newMap = new HashMap<>();
        if (users != null) {
            for (Object user : users.values()) {
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(user);
                GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
                if (!gUser.getId().equalsIgnoreCase(currentUser.getUserId())) {
                    gUser.setBlocked(!isBlocked);
                }
                newMap.put(gUser.getId(), gUser);
            }
            users.clear();
            users.putAll(newMap);
            ProgressDialog.show(getSupportFragmentManager());
            new UpdateGroupAsync().execute();
        }
    }

    @Override
    public void onSuccess(Message message) {
        getRoomDetails();
        ProgressDialog.hide(getSupportFragmentManager());
    }

    private void updateUi() {
        if (isBlocked) {
            tvBlock.setText("Unblock");
        } else {
            tvBlock.setText("Block");
        }
    }

    class UpdateGroupAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            chatRoom.setMembers(new Gson().toJson(users));
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel roomModel) {
            super.onPostExecute(roomModel);
            updateRoom(roomModel);
        }
    }


    private void updateRoom(ChatRoomModel roomModel) {
        ChatRoomService.updateGroupOnline(SingleChatProfileDeatilsActivity.this, roomModel, false, this);
    }

}
