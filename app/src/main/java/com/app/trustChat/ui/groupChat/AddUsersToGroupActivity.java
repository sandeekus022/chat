package com.app.trustChat.ui.groupChat;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.databinding.ActivityAddUsersToGroupBinding;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.main.ProgressDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class AddUsersToGroupActivity extends AppCompatActivity implements MessageCallback {
    List<UserModel> actualList = new ArrayList<>();
    private List<UserModel> selectedUsers = new ArrayList<>();
    private RecyclerViewAdapter adapter;
    ActivityAddUsersToGroupBinding binding;
    private String roomId;
    private ImageView ivBack;
    Map<String, GroupUser> users = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_users_to_group);
        adapter = new RecyclerViewAdapter(actualList);
        binding.recyclerView.setAdapter(adapter);
        roomId = getIntent().getStringExtra("roomId");
        new GetRoomDetailsAsync().execute();
        binding.addUsersBtn.setOnClickListener(v -> {
            addUsersToGroup();
        });
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void addUsersToGroup() {
        if (selectedUsers.size() > 0) {
            new UpdateGroupAsync().execute();
        } else {
            Toast.makeText(this, "Please select contacts to add to group", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(Message message) {
        ProgressDialog.hide(getSupportFragmentManager());
        finish();
    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder> {
        List<UserModel> list;

        RecyclerViewAdapter(List<UserModel> list) {
            this.list = list;
        }

        @NonNull
        @Override
        public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_user, parent, false);
            return new CustomViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewHolder viewHolder, int position) {
            final UserModel userModel = list.get(position);
            String myUid = SharedPrefManager.getInstance(AddUsersToGroupActivity.this)
                    .get(SharedPrefManager.Key.USER_ID, "");
            if (myUid.equals(userModel.getUserId())) {
                viewHolder.itemView.setVisibility(View.INVISIBLE);
                viewHolder.itemView.getLayoutParams().height = 0;
                return;
            }

            viewHolder.user_name.setText(userModel.getUsernm());

            if (userModel.isAlreadyExist()) {
                viewHolder.tvUserMood.setText("Already in group");
            } else {
                viewHolder.tvUserMood.setText(userModel.getMood() == null ? "Hey I am using TrustApp" : userModel.getMood());
            }

            if (userModel.getUserphoto() != null) {
                if (!userModel.getUserphoto().isEmpty()) {
                    Glide.with(AddUsersToGroupActivity.this).load(userModel.getUserphoto())
                            .into(viewHolder.user_photo);
                    viewHolder.user_photo.setVisibility(View.VISIBLE);
                    viewHolder.tvImageAvatar.setVisibility(View.GONE);
                } else {
                    viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
                    viewHolder.user_photo.setVisibility(View.GONE);
                    viewHolder.tvImageAvatar.setText(String.valueOf(userModel.getUsernm().charAt(0)));
                }
            }

            if (userModel.isSelected()) {
                viewHolder.ivSelect.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ivSelect.setVisibility(View.GONE);
            }
            if (!userModel.isAlreadyExist()) {
                viewHolder.itemView.setOnClickListener(v -> {
                    list.get(position).setSelected(!list.get(position).isSelected());
                    if (list.get(position).isSelected()) {
                        selectedUsers.add(userModel);
                    } else {
                        selectedUsers.remove(userModel);
                    }
                    notifyItemChanged(position);
                });
            }
        }


        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }


        class CustomViewHolder extends RecyclerView.ViewHolder {
            ImageView user_photo;
            RelativeLayout ivSelect;
            TextView user_name;
            TextView tvImageAvatar;
            TextView tvUserMood;

            CustomViewHolder(View view) {
                super(view);
                user_photo = view.findViewById(R.id.user_photo);
                ivSelect = view.findViewById(R.id.ivSelect);
                tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
                user_name = view.findViewById(R.id.user_name);
                tvUserMood = view.findViewById(R.id.tvUserMood);
            }

        }

    }


    private class ContactDbAsync extends AsyncTask<Void, Void, List<UserModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<UserModel> doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            List<UserModel> list = repository.getUsers();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);
            if (userModels != null && userModels.size() > 0) {
                actualList.clear();
                for(UserModel model:userModels){
                    if(model.isContact()){
                        actualList.add(model);
                    }
                }
                for (int i = 0; i < actualList.size(); i++) {
                    if (users.containsKey(actualList.get(i).getUserId())) {
                        actualList.get(i).setAlreadyExist(true);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    class GetRoomDetailsAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel roomModel) {
            super.onPostExecute(roomModel);
            users = new Gson().fromJson(
                    roomModel.getMembers(), new TypeToken<HashMap<String, Object>>() {
                    }.getType());
            new ContactDbAsync().execute();
        }
    }

    private class UpdateGroupAsync extends AsyncTask<Void, Void, ChatRoomModel> {

        @Override
        protected ChatRoomModel doInBackground(Void... ChatRoomModel) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel room = repository.getChatRoom(roomId);
            return room;
        }

        @Override
        protected void onPostExecute(ChatRoomModel roomModel) {
            super.onPostExecute(roomModel);
            Map<String, GroupUser> userMap = new Gson().fromJson(
                    roomModel.getMembers(), new TypeToken<HashMap<String, Object>>() {
                    }.getType());
            for (int i = 0; i < selectedUsers.size(); i++) {
                UserModel model = selectedUsers.get(i);
                GroupUser user = new GroupUser();
                user.setName(model.getUsernm());
                user.setAdmin(false);
                user.setMood(model.getMood());
                user.setMessageCount(0);
                user.setId(model.getUserId());
                user.setPhoto(model.getUserphoto());
                userMap.put(model.getUserId(), user);
            }
            roomModel.setMembers(new Gson().toJson(userMap));
            ProgressDialog.show(getSupportFragmentManager());
            updateRoom(roomModel);
        }
    }


    private void updateRoom(ChatRoomModel roomModel) {
        ChatRoomService.updateGroupOnline(AddUsersToGroupActivity.this, roomModel, false, this);
    }
}
