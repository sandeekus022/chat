package com.app.trustChat.ui.voiceCall;


import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.CallLog;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.CallLogRepository;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.services.SinchService;
import com.bumptech.glide.Glide;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallState;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class CallScreenActivity extends BaseActivity {

    static final String TAG = CallScreenActivity.class.getSimpleName();
    static final String ADDED_LISTENER = "addedListener";
    static final String VIEWS_TOGGLED = "viewsToggled";

    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;
    private boolean mAddedListener = false;
    private boolean mLocalVideoViewAdded = false;
    private boolean mRemoteVideoViewAdded = false;
    private LinearLayout layoutBottomButton;
    private TextView mCallDuration, yoohooCalling;
    private TextView mCallState;
    View tintBlue;
    private TextView mCallerName;
    private ImageView mUserFullImage, mUserLogo;
    boolean mToggleVideoViewPositions = false;
    private String callType, screenShowType;
    private boolean isVideoCall;
    String callerName;
    String callerPhoto;
    ImageView logo;
    String userId;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
        savedInstanceState.putBoolean(VIEWS_TOGGLED, mToggleVideoViewPositions);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
        mToggleVideoViewPositions = savedInstanceState.getBoolean(VIEWS_TOGGLED);
    }

    RelativeLayout remoteVideo, localVideo;
    ImageView endCallButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_screen);

        mAudioPlayer = new AudioPlayer(this);
        mCallDuration = findViewById(R.id.callDuration);
        yoohooCalling = findViewById(R.id.yoohoo_calling);
        localVideo = findViewById(R.id.localVideo);
        remoteVideo = findViewById(R.id.remoteVideo);
        logo = findViewById(R.id.logo);
        layoutBottomButton = findViewById(R.id.layout_btns);
        tintBlue = findViewById(R.id.tintBlue);
        mUserLogo = findViewById(R.id.mUserLogo);
        mCallerName = findViewById(R.id.remoteUser);
        mUserFullImage = findViewById(R.id.userImage1);
        mCallState = findViewById(R.id.callState);
        endCallButton = findViewById(R.id.hangupButton);

        if (getIntent().hasExtra("CALL_TYPE")) {
            callType = getIntent().getStringExtra("CALL_TYPE");
        }
        if (getIntent().hasExtra("SCREEN_SHOW")) {
            screenShowType = getIntent().getStringExtra("SCREEN_SHOW");
        }
        Log.e("Video", "screenShowType----" + screenShowType);

        endCallButton.setOnClickListener(v -> endCall());

        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            if (!mAddedListener) {
                call.addCallListener(new SinchCallListener());
                mAddedListener = true;
            }
        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            finish();
        }

        updateUI();
    }

    @Override
    protected void onServiceDisconnected() {

    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            userId = call.getRemoteUserId();
            new GetUserDetailAsync(userId).execute();
            mCallState.setText("Ringing");
            isVideoCall = call.getDetails().isVideoOffered();
            Log.e("Video", "updateUI----" + isVideoCall);
            if (call.getDetails().isVideoOffered()) {
                if (call.getState() == CallState.ESTABLISHED) {
                    setVideoViewsVisibility(true, true);
                } else {
                    setVideoViewsVisibility(true, false);
                }
            }
            yoohooCalling.setText(getString(isVideoCall ? R.string.video_calling : R.string.voice_calling));
            tintBlue.setVisibility(isVideoCall ? View.GONE : View.VISIBLE);

         /*   remoteVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isVideoCall) {
                        startAlphaAnimation();
                    }
                }
            });*/

        } else {
            setVideoViewsVisibility(false, false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mDurationTask.cancel();
        mTimer.cancel();
        removeVideoViews();
    }

    @Override
    public void onStart() {
        super.onStart();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
        updateUI();
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private String formatTimespan(int totalSeconds) {
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallDuration.setText(formatTimespan(call.getDetails().getDuration()));
        }
    }

    private ViewGroup getVideoView(boolean localView) {
        if (mToggleVideoViewPositions) {
            localView = !localView;
        }
        return localView ? findViewById(R.id.localVideo) : findViewById(R.id.remoteVideo);
    }

    private void addLocalView() {
        if (mLocalVideoViewAdded || getSinchServiceInterface() == null) {
            return; //early
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                ViewGroup localView = getVideoView(true);
                localView.addView(vc.getLocalView());
                localView.setOnClickListener(v -> vc.toggleCaptureDevicePosition());
                mLocalVideoViewAdded = true;
                vc.setLocalVideoZOrder(!mToggleVideoViewPositions);
            });
        }
    }

    private void addRemoteView() {
        if (mRemoteVideoViewAdded || getSinchServiceInterface() == null) {
            return; //early
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                ViewGroup remoteView = getVideoView(false);
                remoteView.addView(vc.getRemoteView());
                remoteView.setOnClickListener((View v) -> {
                    removeVideoViews();
                    mToggleVideoViewPositions = !mToggleVideoViewPositions;
                    addRemoteView();
                    addLocalView();
                });
                mRemoteVideoViewAdded = true;
                vc.setLocalVideoZOrder(!mToggleVideoViewPositions);
            });
        }
    }


    private void removeVideoViews() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                if ((ViewGroup) (vc.getRemoteView().getParent()) != null)
                    ((ViewGroup) (vc.getRemoteView().getParent())).removeView(vc.getRemoteView());
                if ((ViewGroup) (vc.getLocalView().getParent()) != null)
                    ((ViewGroup) (vc.getLocalView().getParent())).removeView(vc.getLocalView());
                mLocalVideoViewAdded = false;
                mRemoteVideoViewAdded = false;
            });
        }
    }

    private void setVideoViewsVisibility(final boolean localVideoVisibile, final boolean remoteVideoVisible) {
        if (getSinchServiceInterface() == null)
            return;
        if (mRemoteVideoViewAdded == false) {
            addRemoteView();
        }
        if (mLocalVideoViewAdded == false) {
            addLocalView();
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                vc.getLocalView().setVisibility(localVideoVisibile ? View.VISIBLE : View.GONE);
                vc.getRemoteView().setVisibility(remoteVideoVisible ? View.VISIBLE : View.GONE);
            });
        }
    }

    private class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
            CallLog callLog = new CallLog(userId,call.getDetails().getStartedTime(), callType, isVideoCall, callerName, callerPhoto);
            new SaveCallLogAsync(callLog).execute();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
            mAudioPlayer.stopProgressTone();
            mCallState.setText(call.getState().toString());
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            AudioController audioController = getSinchServiceInterface().getAudioController();
            audioController.disableSpeaker();
            if (call.getDetails().isVideoOffered()) {
                setVideoViewsVisibility(true, true);
            }
            Log.d(TAG, "Call offered video: " + call.getDetails().isVideoOffered());
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {

        }

        @Override
        public void onVideoTrackAdded(Call call) {

        }

        @Override
        public void onVideoTrackPaused(Call call) {

        }

        @Override
        public void onVideoTrackResumed(Call call) {

        }
    }


    private class GetUserDetailAsync extends AsyncTask<Void, Void, UserModel> {
        String userId;

        public GetUserDetailAsync(String userId) {
            this.userId = userId;
        }

        @Override
        protected UserModel doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            return repository.getUser(userId);
        }

        @Override
        protected void onPostExecute(UserModel userModel) {
            super.onPostExecute(userModel);
            if (userModel != null) {
                mCallerName.setText(userModel.getUsernm());
                callerName = userModel.getUsernm();
                callerPhoto = userModel.getUserphoto();
                Glide.with(getApplicationContext()).load(callerPhoto)
                        .into(mUserFullImage);
                Glide.with(getApplicationContext()).load(callerPhoto)
                        .into(mUserLogo);
                mUserLogo.setClipToOutline(true);
            }
        }
    }


    private class SaveCallLogAsync extends AsyncTask<Void, Void, Long> {

        CallLog log;

        public SaveCallLogAsync(CallLog log) {
            this.log = log;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            CallLogRepository repo = new CallLogRepository(MyApplication.getInstance());
            return repo.insertLog(log);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            endCall();
        }
    }

    private boolean alphaInvisible;

    public void startAlphaAnimation() {
        float f = 0.0f;
        float f2 = alphaInvisible ? 0.0f : 1.0f;
        if (alphaInvisible) {
            f = 1.0f;
        }
        AlphaAnimation animation1 = new AlphaAnimation(f2, f);
        animation1.setDuration(500);
        animation1.setStartOffset(25);
        animation1.setFillAfter(true);
        this.logo.startAnimation(animation1);
        yoohooCalling.startAnimation(animation1);
        mUserLogo.startAnimation(animation1);
        this.mCallerName.startAnimation(animation1);
        this.mCallState.startAnimation(animation1);
        this.mCallDuration.startAnimation(animation1);
        this.layoutBottomButton.startAnimation(animation1);
        endCallButton.startAnimation(animation1);
        alphaInvisible = true ^ alphaInvisible;
    }

}
