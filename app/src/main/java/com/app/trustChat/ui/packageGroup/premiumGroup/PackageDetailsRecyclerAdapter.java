package com.app.trustChat.ui.packageGroup.premiumGroup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.R;
import com.app.trustChat.databinding.ItemPackageDetailsBinding;
import com.app.trustChat.model.PackageModel;
import com.app.trustChat.ui.payment.PaymentActivity;

import java.util.List;

public class PackageDetailsRecyclerAdapter extends RecyclerView.Adapter<PackageDetailsRecyclerAdapter.MyItemHolder> {
    private List<PackageModel> arrayLists;
    private Context activity;
    public int mSelectedItem = -1;
    Dialog dialog;

    public PackageDetailsRecyclerAdapter(Context activity, List<PackageModel> arrayLists, Dialog dialog) {
        this.activity = activity;
        this.arrayLists = arrayLists;
        this.dialog= dialog;
    }

    @NonNull
    @Override
    public MyItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemPackageDetailsBinding itemPackageDetailsBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_package_details, viewGroup, false);
        return new MyItemHolder(itemPackageDetailsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyItemHolder viewHolder, int i) {
        PackageModel packageModel = arrayLists.get(i);
        viewHolder.itemPackageDetailsBinding.setPackageViewModel(packageModel);
        viewHolder.itemPackageDetailsBinding.llPackageSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(activity, PaymentActivity.class);
                in.putExtra("id", packageModel.getPkg_id());
                in.putExtra("name", packageModel.getPkg_name());
                in.putExtra("amount", packageModel.getPkg_amount());
                in.putExtra("validity", packageModel.getPkg_validity());
                activity.startActivity(in);
                dialog.dismiss();
            }

        });

    }


    @Override
    public int getItemCount() {
        if (arrayLists != null) {
            return arrayLists.size();
        } else {
            return 0;
        }
    }

    class MyItemHolder extends RecyclerView.ViewHolder {

        private ItemPackageDetailsBinding itemPackageDetailsBinding;

        public MyItemHolder(@NonNull ItemPackageDetailsBinding itemPackageDetailsBinding) {
            super(itemPackageDetailsBinding.getRoot());

            this.itemPackageDetailsBinding = itemPackageDetailsBinding;
           /* View.OnClickListener clickListener = v -> {
                mSelectedItem = getAdapterPosition();
                notifyDataSetChanged();

            };
            itemView.setOnClickListener(clickListener);
            itemSliderImageBinding.radioButton.setOnClickListener(clickListener);*/
        }

    }

}

