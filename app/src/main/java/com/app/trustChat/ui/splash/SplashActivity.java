package com.app.trustChat.ui.splash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.app.trustChat.R;
import com.app.trustChat.model.NotificationModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.chat.ChatActivity;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.main.MainActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

public class SplashActivity extends AppCompatActivity {


    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 151;
    private boolean isPaused;
    private boolean isToStartChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black));
            getWindow().setNavigationBarColor(getResources().getColor(R.color.black));
        }

        setContentView(R.layout.activity_splash);
     //   crashNow();
        if (getIntent().hasExtra("body")) {

            NotificationModel.Body data = new Gson()
                    .fromJson(getIntent().getStringExtra("body"),
                            NotificationModel.Body.class);
            isToStartChat = true;
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("roomTitle", data.senderName);
            intent.putExtra("roomId", data.roomId);
            intent.putExtra("roomImage", data.roomPhoto);
            intent.putExtra("toUid", data.toUid);
            intent.putExtra("isGroup", data.isGroup);
            intent.putExtra("subject", "");
            intent.putExtra("LAST_MSG", data.message);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void crashNow() {
        throw new RuntimeException("home activity sddffgjrffadasfdhgjdcadfhsdAADS");
    }

    private void startNextActivity() {
        new Handler().postDelayed(() -> {
            boolean session = SharedPrefManager.getInstance(SplashActivity.this)
                    .get(SharedPrefManager.Key.SESSION, false);
            if (session) {
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            } else
                startActivity(new Intent(SplashActivity.this, MainActivity.class));

            finish();
        }, 200);
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                showSnackbar(R.string.permission_rationale, android.R.string.ok,
                        view -> requestPermissions());
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startNextActivity();
            } else {

                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        view -> {
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    getPackageName(), null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        });
            }
        }
    }


    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_CONTACTS);
        if (shouldProvideRationale) {
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(SplashActivity.this,
                                    new String[]{Manifest.permission.READ_CONTACTS},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (!isPaused) {
            if (!isToStartChat) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    startNextActivity();
                } else {
                    requestPermission();
                }
            }
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    view -> {
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.READ_CONTACTS,
                                        Manifest.permission.RECORD_AUDIO,
                                        Manifest.permission.MODIFY_AUDIO_SETTINGS,
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.READ_PHONE_STATE},
                                REQUEST_PERMISSIONS_REQUEST_CODE);
                    });
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.MODIFY_AUDIO_SETTINGS,
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    private void startChat(String roomId) {
        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        intent.putExtra("roomId", roomId);
        startActivity(intent);
        finish();
    }
}
