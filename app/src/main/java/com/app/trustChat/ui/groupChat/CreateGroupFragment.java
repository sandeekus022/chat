package com.app.trustChat.ui.groupChat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.constants.ChatRoomStatus;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.MediaUploadModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.ui.profile.ProfileFragment;
import com.app.trustChat.utils.BitmapUtils;
import com.app.trustChat.utils.FileUtil;
import com.app.trustChat.utils.Util9;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class CreateGroupFragment extends AppCompatActivity {
    List<UserModel> actualList = new ArrayList<>();
    public static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    public static final int GALLERY_PICK_CODE = 10;
    private List<UserModel> selectedUsers = new ArrayList<>();
    private RecyclerViewAdapter firestoreAdapter;
    private UserModel currentUser;
    private ConstraintLayout layoutGroupDetails;
    private FloatingActionButton makeRoomBtn;
    private RecyclerView recyclerView, rcTotalParticipants;
    private EditText etTitle;
    private ImageView ivUserImage;
    private String gruopPicURl;
    private AllContactRecyclerViewAdapter allContactRecyclerViewAdapter;
    private TextView tvNumberParticipants;
    private LinearLayout rlSearch;
    private String currentPhotoPath;

    private EditText etSearch;
    private ImageView ivClearSearch;
    private List<UserModel> tempList = new ArrayList<>();
    private boolean isSearchBarVisible;
    private String mTempPhotoPath;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_user);
        tvNumberParticipants = findViewById(R.id.tvNumberParticipants);
        recyclerView = findViewById(R.id.recyclerView);
        firestoreAdapter = new RecyclerViewAdapter(actualList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(firestoreAdapter);
        layoutGroupDetails = findViewById(R.id.layoutGroupDetails);
        etTitle = findViewById(R.id.etTitle);
        ivUserImage = findViewById(R.id.ivUserImage);
        rcTotalParticipants = findViewById(R.id.rcTotalParticipants);
        ivClearSearch = findViewById(R.id.ivClearSearch);
        ivClearSearch.setOnClickListener(v -> etSearch.setText(""));
        etSearch = findViewById(R.id.etSearch);
        rlSearch = findViewById(R.id.rlSearch);
        findViewById(R.id.ivSearch).setOnClickListener(v -> {
            showSearchBar();

        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ivClearSearch.setVisibility(View.VISIBLE);
                } else {
                    ivClearSearch.setVisibility(View.GONE);
                }
                filterList(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ImageView ivPickImage = findViewById(R.id.ivPickImage);
        FloatingActionButton btnCreateGroup = findViewById(R.id.btnCreateGroup);
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);


        makeRoomBtn = findViewById(R.id.makeRoomBtn);
        makeRoomBtn.setOnClickListener(makeRoomClickListener);


        findViewById(R.id.ivBack).setOnClickListener(v -> {
            onBackPressed();
        });

        ivPickImage.setOnClickListener(v -> openDialogChooser());
        btnCreateGroup.setOnClickListener(v -> {
            if (etTitle.getText().toString().isEmpty()) {
                etTitle.setError("Please Enter group title");
                etTitle.requestFocus();
            } else {
                CreateChattingRoom();
            }
        });

        filterContactList();
    }

    private void showSearchBar() {
        isSearchBarVisible = true;
        //tabLayout.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterReveal(rlSearch);
        } else {
            rlSearch.setVisibility(View.VISIBLE);
        }
        etSearch.requestFocus();
        if (etSearch.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    public void filterList(String query) {
        firestoreAdapter.filterList(query);
    }


    private void filterContactList() {
        rcTotalParticipants.setLayoutManager(new GridLayoutManager(this, 4));
        allContactRecyclerViewAdapter = new AllContactRecyclerViewAdapter(selectedUsers);
        rcTotalParticipants.setAdapter(allContactRecyclerViewAdapter);
        SharedPrefManager prefManager = SharedPrefManager.getInstance(CreateGroupFragment.this);
        String listGson = prefManager.get(SharedPrefManager.Key.USERS_LIST, null);
        List<UserModel> list = new Gson().fromJson(listGson, new TypeToken<ArrayList<UserModel>>() {
        }.getType());

        if (list != null && list.size() > 0) {
            actualList.clear();
            for (UserModel model : list) {
                if (model.isContact()) {
                    actualList.add(model);
                    tempList.add(model);
                }
            }
            Collections.sort(actualList, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String str = "ImageView";
        if (requestCode == GALLERY_PICK_CODE && resultCode == Activity.RESULT_OK && data != null) {
            try {
                Log.e("UCrop", "data---" + data.getData());
                Uri destinationUri = Uri.fromFile(getImageFile());
                openCropActivity(data.getData(), destinationUri);
            } catch (Exception ex) {
                Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_ACTION_PICK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap mResultsBitmap = BitmapUtils.resamplePic(this, mTempPhotoPath);
            Uri resultUri = Uri.fromFile(new File(BitmapUtils.saveImage(this, mResultsBitmap)));

            try {
                File file = FileUtil.from(this, resultUri);
                Glide.with(this).load(file)
                        .placeholder(R.drawable.ic_user)
                        .into(ivUserImage);
                ProgressDialog.show(getSupportFragmentManager());
                uploadGroupImage(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Activity.RESULT_OK) {
            Uri imageUri = UCrop.getOutput(data);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                drawable.setCircular(true);
                ivUserImage.setImageDrawable(drawable);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File file = null;
            try {
                file = FileUtil.from(this, imageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ProgressDialog.show(getSupportFragmentManager());
            uploadGroupImage(file);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Throwable cropError = UCrop.getError(data);
            Log.e("UCrop", "cropError---" + cropError);
        }

    }

    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(CreateGroupFragment.this, R.color.colorOrange));
        UCrop.of(sourceUri, destinationUri)
                //.withAspectRatio(5f, 5f)
                .start(this);
    }

    private void uploadGroupImage(File file) {
        String mobileNumber = currentUser.getMobile();
        CompositeDisposable disposable = new CompositeDisposable();
        String filename = Util9.getUniqueValue(String.valueOf(currentUser.getUserId()));
        MultipartBody.Part doc;
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), filename);
        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        doc = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        Single<Response<MediaUploadModel>> caller = RetrofitBuilder.getService(this)
                .uploadFile(doc, mobile, fileName);

        disposable.add(caller.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<MediaUploadModel>>() {
                    @Override
                    public void onSuccess(Response<MediaUploadModel> mediaUploadModelResponse) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        gruopPicURl = mediaUploadModelResponse.body().getFile();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());
                    }
                }));
    }

    Button.OnClickListener makeRoomClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            if (selectedUsers.size() < 2) {
                Util9.showMessage(CreateGroupFragment.this, "Please select 2 or more user");
                return;
            }
            selectedUsers.add(currentUser);
            setVisibility(false);
        }
    };


    public void CreateChattingRoom() {
        String title = etTitle.getText().toString();
        ChatRoomModel chatRoomModel = new ChatRoomModel();
        chatRoomModel.setRoomId(currentUser.getUserId() + "_" + System.currentTimeMillis());
        chatRoomModel.setGroup(true);
        chatRoomModel.setUserId(currentUser.getUserId());
        chatRoomModel.setTitle(title);
        chatRoomModel.setPhoto(gruopPicURl == null ? "" : gruopPicURl);
        chatRoomModel.setLastMsg(currentUser.getUsernm() + " created this group");
        chatRoomModel.setSubject(currentUser.getUsernm() + " created this group");
        chatRoomModel.setUnreadCount(0);
        chatRoomModel.setUpdatedAt(System.currentTimeMillis());
        chatRoomModel.setCreatedAt(System.currentTimeMillis());
        chatRoomModel.setStatus(ChatRoomStatus.OFFLINE);


        Map<String, GroupUser> users = new HashMap<>();

        for (UserModel user : selectedUsers) {
            GroupUser groupUser = new GroupUser();
            groupUser.setId(user.getUserId());
            groupUser.setName(user.getUsernm());
            groupUser.setMobile(user.getMobile());
            groupUser.setMood(user.getMood());
            groupUser.setMessageCount(0);
            groupUser.setToken(user.getToken());
            groupUser.setPhoto(user.getUserphoto());
            groupUser.setAdmin(user.getUserId() == currentUser.getUserId() ? true : false);
            users.put(user.getUserId(), groupUser);
        }
        chatRoomModel.setMembers(new Gson().toJson(users));
        new CreateRoomAsync(chatRoomModel).execute();
        ChatRoomService.createGroupOnline(CreateGroupFragment.this, chatRoomModel);
    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder> {
        List<UserModel> list;

        RecyclerViewAdapter(List<UserModel> list) {
            this.list = list;
        }

        @NonNull
        @Override
        public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_user, parent, false);
            return new CustomViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewHolder viewHolder, int position) {
            final UserModel userModel = list.get(position);
            String myUid = SharedPrefManager.getInstance(CreateGroupFragment.this)
                    .get(SharedPrefManager.Key.USER_ID, "");
            if (myUid.equals(userModel.getUserId())) {
                viewHolder.itemView.setVisibility(View.INVISIBLE);
                viewHolder.itemView.getLayoutParams().height = 0;
                return;
            }

            viewHolder.user_name.setText(userModel.getUsernm());

            if (userModel.getUserphoto() != null) {
                if (!userModel.getUserphoto().isEmpty()) {
                    Glide.with(CreateGroupFragment.this).load(userModel.getUserphoto())
                            .into(viewHolder.user_photo);
                    viewHolder.user_photo.setVisibility(View.VISIBLE);
                    viewHolder.tvImageAvatar.setVisibility(View.GONE);
                } else {
                    viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
                    viewHolder.user_photo.setVisibility(View.GONE);
                    viewHolder.tvImageAvatar.setText(String.valueOf(userModel.getUsernm().charAt(0)));
                }
            }

            if (userModel.isSelected()) {
                viewHolder.ivSelect.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ivSelect.setVisibility(View.GONE);
            }
            viewHolder.tvUserMood.setText(userModel.getMood() == null ? "Hey I am using TrustApp" : userModel.getMood());


            viewHolder.itemView.setOnClickListener(v -> {
                list.get(position).setSelected(!list.get(position).isSelected());
                if (list.get(position).isSelected()) {
                    selectedUsers.add(userModel);
                } else {
                    selectedUsers.remove(userModel);
                }
                notifyItemChanged(position);
                allContactRecyclerViewAdapter.notifyDataSetChanged();
                tvNumberParticipants.setText(selectedUsers.size() + " Participants");
            });
        }


        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void filterList(String query) {
            if (query.length() > 0) {
                List<UserModel> queryList = new ArrayList<>();
                for (UserModel room : tempList) {
                    if (room.getUsernm() != null)
                        if (room.getUsernm().toLowerCase().contains(query.toLowerCase()))
                            queryList.add(room);
                }
                list.clear();
                list.addAll(queryList);
            } else {
                list.clear();
                list.addAll(tempList);
            }
            Collections.sort(list, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
            notifyDataSetChanged();
        }


        class CustomViewHolder extends RecyclerView.ViewHolder {
            ImageView user_photo;
            RelativeLayout ivSelect;
            TextView user_name;
            TextView tvImageAvatar;
            TextView tvUserMood;

            CustomViewHolder(View view) {
                super(view);
                user_photo = view.findViewById(R.id.user_photo);
                ivSelect = view.findViewById(R.id.ivSelect);
                tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
                user_name = view.findViewById(R.id.user_name);
                tvUserMood = view.findViewById(R.id.tvUserMood);

            }

        }

    }

    private boolean isRoomDetailsVisible = false;
    private View.OnClickListener roomDetailsClickListener = v -> setVisibility(false);


    private void setVisibility(boolean isRoomDetailsVisible) {
        if (isRoomDetailsVisible) {
            recyclerView.setVisibility(View.VISIBLE);
            makeRoomBtn.setVisibility(View.VISIBLE);
            layoutGroupDetails.setVisibility(View.GONE);

        } else {
            recyclerView.setVisibility(View.GONE);
            makeRoomBtn.setVisibility(View.GONE);
            layoutGroupDetails.setVisibility(View.VISIBLE);
        }
        this.isRoomDetailsVisible = !isRoomDetailsVisible;
    }


    class AllContactRecyclerViewAdapter extends RecyclerView.Adapter<AllContactRecyclerViewAdapter.CustomViewHolder> {
        List<UserModel> list;

        AllContactRecyclerViewAdapter(List<UserModel> list) {
            this.list = list;
        }

        @NonNull
        @Override
        public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_group_member, parent, false);
            return new CustomViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewHolder viewHolder, int position) {
            UserModel userModel = list.get(position);
            if (currentUser.getUserId().equals(userModel.getUserId())) {
                viewHolder.itemView.setVisibility(View.INVISIBLE);
                viewHolder.itemView.getLayoutParams().height = 0;
                return;
            }

            if (!userModel.getUserphoto().isEmpty()) {
                Glide.with(CreateGroupFragment.this).load(userModel.getUserphoto())
                        .into(viewHolder.ivProfile);
                viewHolder.ivProfile.setVisibility(View.VISIBLE);
                viewHolder.tvImageAvatar.setVisibility(View.GONE);
            } else {
                viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
                viewHolder.ivProfile.setVisibility(View.GONE);
                viewHolder.tvImageAvatar.setText(String.valueOf(userModel.getUsernm().charAt(0)));
            }

            viewHolder.tvTitle.setText(userModel.getUsernm());
        }


        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }


        class CustomViewHolder extends RecyclerView.ViewHolder {
            ImageView ivProfile;
            TextView tvTitle;
            TextView tvImageAvatar;


            CustomViewHolder(View view) {
                super(view);
                ivProfile = view.findViewById(R.id.ivProfile);
                tvTitle = view.findViewById(R.id.tvTitle);
                tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
            }

        }

    }

    @Override
    public void onBackPressed() {
        if (isSearchBarVisible) {
            exitReveal(rlSearch);
            isSearchBarVisible = false;
        } else if (isRoomDetailsVisible) {
            selectedUsers.remove(currentUser);
            setVisibility(true);
        } else {
            super.onBackPressed();
        }
    }

    private class ContactDbAsync extends AsyncTask<Void, Void, List<UserModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<UserModel> doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            List<UserModel> list = repository.getUsers();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);
            if (userModels != null && userModels.size() > 0) {
                actualList.clear();
                for (UserModel model : userModels) {
                    if (model.isContact()) {
                        actualList.add(model);
                        tempList.add(model);
                    }
                }
                Collections.sort(actualList, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
                firestoreAdapter.notifyDataSetChanged();
            }
        }
    }

    class CreateRoomAsync extends AsyncTask<Void, Void, Long> {

        private ChatRoomModel chatRoomModel;

        public CreateRoomAsync(ChatRoomModel chatRoomModel) {
            this.chatRoomModel = chatRoomModel;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            return repository.createChatRoom(chatRoomModel);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            startActivity(new Intent(CreateGroupFragment.this, HomeActivity.class));
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void enterReveal(View myView) {
        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / 2;

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void exitReveal(View myView) {
        // previously visible view

        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth() / 2;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.GONE);
            }
        });

        // start the animation
        anim.start();
    }

    public void openDialogChooser() {
        final Dialog registerUser = new Dialog(this);
        registerUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerUser.setContentView(R.layout.chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(registerUser.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        registerUser.getWindow().setAttributes(lp);
        registerUser.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        registerUser.setCancelable(false);
        registerUser.show();

        LinearLayout galleryId = registerUser.findViewById(R.id.llGallery);
        LinearLayout cameraId = registerUser.findViewById(R.id.llCamera);
        RelativeLayout relativeLayout = registerUser.findViewById(R.id.rlDailog);

        cameraId.setOnClickListener(v -> {
            registerUser.dismiss();
            launchCamera();
        });

        galleryId.setOnClickListener(v -> {
            registerUser.dismiss();
            startActivityForResult(Intent.createChooser(getGalleryPickerIntent(), "Select Profile Picture"), GALLERY_PICK_CODE);

        });


        relativeLayout.setOnClickListener(v -> registerUser.dismiss());

    }

    private void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = BitmapUtils.createTempImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mTempPhotoPath = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(this, BitmapUtils.FILE_PROVIDER_AUTHORITY, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
            }
        }
    }

    public Intent getGalleryPickerIntent() {

        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        return pictureIntent;
    }

    private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists()) {
            System.out.println("File exists");
        } else {
            System.out.println("File not exists");
        }
        File file = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }

}
