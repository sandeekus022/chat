package com.app.trustChat.ui.chat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.services.UpdateContactsService;
import com.app.trustChat.ui.callLog.CallLogFragment;
import com.app.trustChat.ui.chatRoom.ChatRoomFragment;
import com.app.trustChat.ui.contact.UserListFragment;
import com.app.trustChat.ui.franchise.FranchiseActivity;
import com.app.trustChat.ui.groupChat.CreateGroupFragment;
import com.app.trustChat.ui.packageGroup.PackageGroupActivity;
import com.app.trustChat.ui.setting.SettingActivity;
import com.app.trustChat.ui.subscription.SubscriptionActivity;
import com.app.trustChat.ui.user.UserWebActivity;
import com.app.trustChat.ui.voiceCall.BaseActivity;
import com.app.trustChat.utils.MainUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends BaseActivity {
    private static Menu menu;
    public static boolean isLongClickEnabled;
    private DocumentReference firestore;
    private String userId;
    private UserModel currentUser;
    private FloatingActionButton makeRoomBtn;
    private MenuItem itemSearch;
    private boolean isSearchBarVisible;
    private static TabLayout tabLayout;
    private LinearLayout rlSearch;
    private EditText etSearch;
    private ImageView ivDelete;
    private ChatRoomFragment chatRoomFragment;
    private static RelativeLayout rlRoomSelection;
    private static TextView tvCount;
    private static Toolbar toolbar;

    public static void setLongClickEnabled(boolean b, int size) {
        isLongClickEnabled = b;
        if (size > 0) {
            tvCount.setText(String.valueOf(size));
            showRoomSelect();
        } else {
            clearRoomSelection();
        }
    }

    public static List<Message> forwardedMessageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        toolbar = findViewById(R.id.toolbar);
        rlSearch = findViewById(R.id.rlSearch);
        rlRoomSelection = findViewById(R.id.rlRoomSelection);
        setSupportActionBar(toolbar);
        tvCount = findViewById(R.id.tvCount);
        makeRoomBtn = findViewById(R.id.makeRoomBtn);
        if (getIntent().hasExtra("FORWARD_MESSAGES"))
            forwardedMessageList = getIntent().getExtras().getParcelableArrayList("FORWARD_MESSAGES");

        chatRoomFragment = new ChatRoomFragment();

        makeRoomBtn.setOnClickListener(v -> {
            Intent intent = new Intent(HomeActivity.this, UserListFragment.class);
            startActivity(intent);
        });

        userId = SharedPrefManager.getInstance(this).get(SharedPrefManager.Key.USER_ID, "");
        firestore = FirebaseFirestore.getInstance().collection("users").document(userId);
        // Set up the ViewPager with the sections adapter.
        ImageView ivClearSearch = findViewById(R.id.ivClearSearch);
        etSearch = findViewById(R.id.etSearch);
        findViewById(R.id.ivCloseSearch).setOnClickListener(v -> hideSearchBar());
        ivClearSearch.setOnClickListener(v -> etSearch.setText(""));

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ivClearSearch.setVisibility(View.VISIBLE);
                } else {
                    ivClearSearch.setVisibility(View.GONE);
                }
                chatRoomFragment.filterList(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        findViewById(R.id.ivClearSelection).setOnClickListener(v -> {
            clearRoomSelection();
        });
        UpdateContactsService.getContactAsync(this);

        findViewById(R.id.ivDelete).setOnClickListener(v -> {
            ChatRoomFragment.deleteRooms();
        });
        if (getIntent().hasExtra("roomId")) {
            String roomId = getIntent().getStringExtra("roomId");
            new GetRoomDetailsAsync(roomId);
        }
    }

    @Override
    protected void onServiceConnected() {
        if (currentUser != null)
            getSinchServiceInterface().startClient(currentUser.getUserId());
    }

    @Override
    protected void onServiceDisconnected() {
        // do not disconnect client
    }

    @Override
    protected void onResume() {
        super.onResume();
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        ViewPager mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {     // char room
                    makeRoomBtn.setVisibility(View.VISIBLE);
                    itemSearch.setVisible(true);
                }
                if (tab.getPosition() != 0) {
                    itemSearch.setVisible(false);
                    if (isSearchBarVisible)
                        hideSearchBar();
                    clearRoomSelection();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                makeRoomBtn.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_room_menu, menu);
        itemSearch = menu.findItem(R.id.menu_search);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_group:
                Intent intent = new Intent(HomeActivity.this, CreateGroupFragment.class);
                startActivity(intent);
                return true;
            case R.id.menu_search:
                showSearchBar();
                return true;
            case R.id.subscription:
                Intent intents = new Intent(this, SubscriptionActivity.class);
                startActivity(intents);
                return true;
            case R.id.package_group:
                Intent intentPackage = new Intent(HomeActivity.this, PackageGroupActivity.class);
                startActivity(intentPackage);
                return true;
            case R.id.setting:
                Intent intentSetting = new Intent(HomeActivity.this, SettingActivity.class);
                startActivity(intentSetting);
                return true;
            case R.id.franchise:
                Intent intentFranchise = new Intent(HomeActivity.this, FranchiseActivity.class);
                startActivity(intentFranchise);
                return true;
            case R.id.user:
                Intent intentUser = new Intent(HomeActivity.this, UserWebActivity.class);
                startActivity(intentUser);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }


    private class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        SectionsPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return chatRoomFragment;
            }
            return new CallLogFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


    @Override
    public void onBackPressed() {
        if (isSearchBarVisible) {
            hideSearchBar();
        } else if (isLongClickEnabled) {
            clearRoomSelection();
        } else {
            super.onBackPressed();
        }
    }

    private static void clearRoomSelection() {
        ChatRoomFragment.disableLongClick();
        isLongClickEnabled = false;
        rlRoomSelection.setVisibility(View.GONE);
        toolbar.setEnabled(true);
        menu.findItem(R.id.menu_search).setEnabled(true);
    }

    private void hideSearchBar() {
        chatRoomFragment.filterList("");
        etSearch.clearFocus();
        isSearchBarVisible = false;
        tabLayout.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            exitReveal(rlSearch);
        } else {
            rlSearch.setVisibility(View.GONE);
        }
        MainUtils.hideKeyboard(this);
    }

    private void showSearchBar() {
        isSearchBarVisible = true;
        tabLayout.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterReveal(rlSearch);
        } else {
            rlSearch.setVisibility(View.VISIBLE);
        }
        etSearch.requestFocus();
        if (etSearch.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private static void showRoomSelect() {
        rlRoomSelection.setVisibility(View.VISIBLE);
        toolbar.setEnabled(false);
        menu.findItem(R.id.menu_search).setEnabled(false);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void enterReveal(View myView) {
        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / 2;

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void exitReveal(View myView) {
        // previously visible view

        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth() / 2;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.GONE);
            }
        });

        // start the animation
        anim.start();
    }

    class GetRoomDetailsAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        String roomId;

        public GetRoomDetailsAsync(String roomId) {
            this.roomId = roomId;
        }

        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel chatRoom) {
            if (chatRoom != null) {
                startChat(chatRoom);
            }
        }
    }

    private void startChat(ChatRoomModel chatRoomModel) {
        Intent intent = new Intent(HomeActivity.this, ChatActivity.class);
        intent.putExtra("roomTitle", chatRoomModel.getTitle());
        intent.putExtra("roomId", chatRoomModel.getRoomId());
        intent.putExtra("roomImage", chatRoomModel.getPhoto());
        intent.putExtra("isGroup", chatRoomModel.isGroup());
        intent.putExtra("toUid", chatRoomModel.getOpponentId());
        intent.putExtra("subject", chatRoomModel.getSubject() == null ? "" : chatRoomModel.getSubject());
        intent.putExtra("LAST_MSG", chatRoomModel.getLastMsg());
        startActivity(intent);
    }

}
