package com.app.trustChat.ui.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.app.trustChat.BR;
import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.base.BaseActivity;
import com.app.trustChat.databinding.ProfileFragmentBinding;
import com.app.trustChat.model.DistrictModel;
import com.app.trustChat.model.OtpVerificationModel;
import com.app.trustChat.model.StateModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.main.ProgressDialog;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class ViewProfileFragment extends BaseActivity<ProfileFragmentBinding, ProfileViewModel>
        implements ProfileNavigator {

    private ProfileViewModel mViewModel;
    private String imageUrl;
    private ArrayAdapter<StateModel> stateAdapter;
    private ArrayAdapter<DistrictModel> districtAdapter;
    private List<StateModel> stateList = new ArrayList<>();
    private List<DistrictModel> districtList = new ArrayList<>();
    private String state;
    private String district;
    private boolean isFirstTime = true;
    private boolean isMe;

    @Override
    public int getLayoutId() {
        return R.layout.profile_fragment;
    }

    @Override
    public void setView() {
        String toUid = getIntent().getStringExtra("toUid");
        boolean isMe = getIntent().getBooleanExtra("isMe", false);
        FirebaseFirestore.getInstance().collection("users")
                .document(toUid).get().addOnSuccessListener(documentSnapshot -> {
            UserModel userModel = documentSnapshot.toObject(UserModel.class);
            mViewModel.name.setValue(userModel.getUsernm());
            mViewModel.mobile.setValue(userModel.getMobile());
            mViewModel.email.setValue(userModel.getEmail());
            mViewModel.district.setValue(userModel.getDistrict());
            mViewModel.state.setValue(userModel.getState());
            state = userModel.getState();
            district = userModel.getDistrict();
            getViewDataBinding().tvContactName.setText(userModel.getUsernm());
            mViewModel.setMobileNumber(userModel.getMobile());
            if (userModel.getUserphoto() != null) {
                if (!userModel.getUserphoto().isEmpty()) {
                    Glide.with(this).load(userModel.getUserphoto())
                            .error(R.drawable.ic_user)
                            .into(getViewDataBinding().ivUserImage);
                }
            }
            getProfile(userModel.getMobile());
        });
        getViewDataBinding().etEmail.setEnabled(false);

        if (isMe) {
            isMe = true;
            getViewDataBinding().textView3.setText("Update");
            getViewDataBinding().imageView3.setVisibility(View.VISIBLE);
        } else {
            isMe = false;
            getViewDataBinding().tvDistrict.setEnabled(false);
            getViewDataBinding().tvState.setEnabled(false);
            getViewDataBinding().etMobileNumber.setEnabled(false);
            getViewDataBinding().etName.setEnabled(false);
            getViewDataBinding().constraintLayout1.setVisibility(View.GONE);
            getViewDataBinding().imageView3.setVisibility(View.GONE);
        }

        getViewDataBinding().ivBack.setOnClickListener(v -> {
            onBackPressed();
        });
        getStates();

    }

    private void getProfile(String mobile) {
        Single<Response<OtpVerificationModel>> caller = RetrofitBuilder.getService(this)
                .getProfile(mobile);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<OtpVerificationModel>>() {
                    @Override
                    public void onSuccess(Response<OtpVerificationModel> response) {
                        ProgressDialog.hide(getSupportFragmentManager());
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());

                    }
                }));
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public ProfileViewModel getViewModel() {
        if (mViewModel == null)
            mViewModel = new ProfileViewModel();
        mViewModel.setNavigator(this);
        return mViewModel;
    }

    @Override
    public void pickProfileImage() {
        ImagePicker.Companion.with(this)
                .crop(1, 1)
                .compress(1024)
                .maxResultSize(1080, 1080)
                .start();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                drawable.setCircular(true);
                getViewDataBinding().ivUserImage.setImageDrawable(drawable);
            } catch (IOException e) {
                e.printStackTrace();
            }

            File file = ImagePicker.Companion.getFile(data);
            ProgressDialog.show(getSupportFragmentManager());
            getViewModel().uploadProfileImage(this, file);
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this,
                    ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT)
                    .show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showEmailValidation(String please_enter_email) {

    }

    @Override
    public void showNameValidation(String please_enter_name) {

    }

    @Override
    public void onBackPress() {

    }

    @Override
    public void setProfilePicureUrl(String image) {
        ProgressDialog.hide(getSupportFragmentManager());
        imageUrl = image;
    }

    @Override
    public void updaterProfile(String name, String email, String mobile) {
        saveUserToFirebase(name, email, mobile);
    }

    @Override
    public void saveUserToFirebase(String name, String email, String mobile) {
        UserModel userModel = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        if (imageUrl != null)
            userModel.setUserphoto(imageUrl);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (name != null || !name.isEmpty())
            userModel.setUsernm(name);
        userModel.setState(getViewDataBinding().tvState.getText().toString());
        userModel.setDistrict(getViewDataBinding().tvDistrict.getText().toString());
        ProgressDialog.show(getSupportFragmentManager());
        db.collection("users").document(userModel.getUserId())
                .set(userModel)
                .addOnSuccessListener(aVoid -> {
                    MyApplication.saveCurrentUser(userModel);
                    ProgressDialog.hide(getSupportFragmentManager());
                    Toast.makeText(ViewProfileFragment.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                });
    }

    @Override
    public void selectDistrict() {
        if (isMe)
            getViewDataBinding().spinnerDistrict.performClick();
    }

    @Override
    public void selectState() {
        isFirstTime = false;
        if (isMe)
            getViewDataBinding().spinnerState.performClick();
    }

    @Override
    public void showStateValidation(String please_select_state) {

    }

    @Override
    public void showDistValidation(String please_select_district) {

    }


    public void setDistrictAdapter(String state) {
        ProgressDialog.show(getSupportFragmentManager());
        Single<List<DistrictModel>> caller = RetrofitBuilder.getService(this)
                .getDistrict(state);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<DistrictModel>>() {
                    @Override
                    public void onSuccess(List<DistrictModel> response) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        districtList.clear();
                        districtList.addAll(response);
                        updateDistrictSpinner();
                    }


                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());

                    }
                }));
    }

    private void updateDistrictSpinner() {
        districtAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_spinner_item, districtList);
        getViewDataBinding().spinnerDistrict.setAdapter(districtAdapter);
        getViewDataBinding().spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getViewModel().district.setValue(districtList.get(position).getDistrict());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                getViewDataBinding().tvDistrict.setText(district);
            }
        });
    }

    private void getStates() {
        ProgressDialog.show(getSupportFragmentManager());
        Single<List<StateModel>> caller = RetrofitBuilder.getService(this)
                .getState();
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<StateModel>>() {
                    @Override
                    public void onSuccess(List<StateModel> response) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        stateList.clear();
                        stateList.addAll(response);
                        setStateAdapter();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());

                    }
                }));
    }

    public void setStateAdapter() {
        stateAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_spinner_item, stateList);
        getViewDataBinding().spinnerState.setAdapter(stateAdapter);
        getViewDataBinding().spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isFirstTime) {
                    getViewDataBinding().tvState.setText(stateList.get(position).getState());
                    setDistrictAdapter(getViewModel().state.getValue());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                getViewDataBinding().tvState.setText(state);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }
}
