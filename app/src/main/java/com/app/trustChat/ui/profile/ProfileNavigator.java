package com.app.trustChat.ui.profile;

public interface ProfileNavigator {
    void pickProfileImage();

    void showEmailValidation(String please_enter_email);

    void showNameValidation(String please_enter_name);


    void onBackPress();


    void setProfilePicureUrl(String image);

    void updaterProfile(String name, String email, String mobile);

    void saveUserToFirebase(String name, String email, String mobile);

    void selectDistrict();

    void selectState();

    void showStateValidation(String please_select_state);

    void showDistValidation(String please_select_district);
}
