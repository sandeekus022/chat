package com.app.trustChat.ui.login;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.app.trustChat.base.BaseViewModel;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {


    public MutableLiveData<String> mobileNumber = new MutableLiveData<>();
    public MutableLiveData<String> sponsor = new MutableLiveData<>();

    public void onContinue(View v) {
        /*     String referralCode = */
        Context context = v.getContext();
        if (validate()) {
            getNavigator().callLoginApi(mobileNumber.getValue(), sponsor.getValue());
        }
    }


    public void onGetReferral(View v) {

        getNavigator().callReferralCode(mobileNumber.getValue());
    }


    private boolean validate() {
        if (mobileNumber.getValue() == null || mobileNumber.getValue().isEmpty()) {
            getNavigator().showMobileValidation("Please enter mobile number");
            return false;
        }
        if (mobileNumber.getValue().length() < 10) {
            getNavigator().showMobileValidation("Please enter valid mobile number");
            return false;
        }

        return true;
    }
}
