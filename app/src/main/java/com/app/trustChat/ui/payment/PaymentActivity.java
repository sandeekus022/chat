package com.app.trustChat.ui.payment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.model.WrapperModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.main.ProgressDialog;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;

import java.io.File;
import java.util.Random;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PaymentActivity extends AppCompatActivity {
    private String name, amount, validity, id;
    private RelativeLayout rlViewPackage;
    private TextView tvPackageName, tvPackageValidity, tvPackageAmount, tvUplodDocument;
    private EditText etAmount, etAccountHolderName, etAccountNumber, etIfscCode, imageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        findByViewIds();
        setDataInformation();


        tvUplodDocument.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(PaymentActivity.this)
                        .crop(1, 1)
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start();
            }
        });
        rlViewPackage.setOnClickListener(v -> postPurchasePackage(mobile, id, ca_no, ac_holder, ifsc, randomId(), file));
    }

    private void findByViewIds() {
        tvPackageAmount = findViewById(R.id.tvPackageAmount);
        tvPackageValidity = findViewById(R.id.tvPackageValidity);
        tvPackageName = findViewById(R.id.tvPackageName);
        tvUplodDocument = findViewById(R.id.tvUplodDocument);
        rlViewPackage = findViewById(R.id.rlViewPackage);
        etAccountHolderName = findViewById(R.id.etAccountHolderName);
        etAccountNumber = findViewById(R.id.etAccountNumber);
        etIfscCode = findViewById(R.id.etIfscCode);
        etAmount = findViewById(R.id.etAmount);
        imageName = findViewById(R.id.imageName);
    }

    private String ca_no, ac_holder, ifsc, mobile;
    UserModel currentUser;

    private void setDataInformation() {
        id = getIntent().getStringExtra("id");
        name = getIntent().getStringExtra("name");
        amount = getIntent().getStringExtra("amount");
        validity = getIntent().getStringExtra("validity");
        tvPackageAmount.setText("Amount - " + amount);
        tvPackageValidity.setText("Validity - " + validity);
        tvPackageName.setText("Name - " + name);
        etAmount.setText(amount);
        ca_no = etAccountNumber.getText().toString();
        ac_holder = etAccountHolderName.getText().toString();
        ifsc = etIfscCode.getText().toString();
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        mobile = currentUser.getMobile();
        Log.e("Payment", "ca_no j---" + ca_no);
        Log.e("Payment", "ac_holder j---" + ac_holder);
        Log.e("Payment", "ifsc j---" + ifsc);
        Log.e("Payment", "ce j---" + amount);
    }

    File file;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            file = ImagePicker.Companion.getFile(data);
            Log.e("Payment", "ce j---" + file.getName());
            imageName.setText(file.getName());
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this,
                    ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT)
                    .show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }

    private String randomId() {
        Random r = new Random();
        return "9999999999";
    }

    private void postPurchasePackage(String mobile, String id, String ac_no, String ac_holder, String ifsc, String txnId, File file) {
        MultipartBody.Part requestImage = null;
        RequestBody image = RequestBody.create(MediaType.parse("image/*"), file);
        requestImage = MultipartBody.Part.createFormData("pkg_receipt", file.getName(), image);
        ProgressDialog.show(getSupportFragmentManager());
        RequestBody mobileNumber = RequestBody.create(MediaType.parse("multipart/form-data"), mobile);
        RequestBody ID = RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        RequestBody acNo = RequestBody.create(MediaType.parse("multipart/form-data"), ac_no);
        RequestBody acHolder = RequestBody.create(MediaType.parse("multipart/form-data"), ac_holder);
        RequestBody IFSC = RequestBody.create(MediaType.parse("multipart/form-data"), ifsc);
        RequestBody txnID = RequestBody.create(MediaType.parse("multipart/form-data"), txnId);
        Single<WrapperModel> caller = RetrofitBuilder.getService(this)
                .uploadPurchase(mobileNumber, ID, acNo, acHolder, IFSC, txnID, requestImage);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<WrapperModel>() {
                    @Override
                    public void onSuccess(WrapperModel response) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        Log.e("Response", "checkk----" + response);
                    }


                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());

                    }
                }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }

    public void back(View view) {
        onBackPressed();
    }
}