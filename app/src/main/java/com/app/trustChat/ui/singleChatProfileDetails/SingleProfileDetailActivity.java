package com.app.trustChat.ui.singleChatProfileDetails;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupDetailModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.utils.MainUtils;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SingleProfileDetailActivity extends AppCompatActivity implements MessageCallback {
    private Map<String, GroupUser> users = new HashMap<>();
    private UserModel currentUser;
    private String title;
    private String roomId;
    private boolean isBlocked;
    private TextView name, status, tvSingleAbout, tvDate, tvMobileNumber, tvBlock;
    private ImageView ivProfileImage, ivDefault;
    private LinearLayout llBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_profile_detail);

        title = getIntent().getStringExtra("title");
        roomId = getIntent().getStringExtra("roomId");
        findByViewIds();
        getRoomDetails();
        getProfile(getIntent().getStringExtra("toUid"));
        currentUser = MyApplication.getInstance().getCurrentUser();
    }


    private void findByViewIds() {
        name = findViewById(R.id.name);
        status = findViewById(R.id.status);
        tvBlock = findViewById(R.id.tvBlock);
        llBack = findViewById(R.id.llBack);
        ivProfileImage = findViewById(R.id.ivProfileImage);
        tvSingleAbout = findViewById(R.id.tvSingleAbout);
        tvDate = findViewById(R.id.tvDate);
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        ivDefault = findViewById(R.id.ivDefault);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.cvBlock).setOnClickListener(v -> {
            String msg = isBlocked ? "Unblock " + title + "?" : "Block " + title;
            MainUtils.showYesNoDialog(SingleProfileDetailActivity.this,
                    "Alert", msg, () -> blockUnblockUser());
        });
    }


    private void getProfile(String toUid) {
        FirebaseFirestore.getInstance().collection("users")
                .document(toUid).addSnapshotListener((documentSnapshot, e) -> {
            UserModel userModel = documentSnapshot.toObject(UserModel.class);
            setUserDetails(users, userModel);
        });
    }

    private void setUserDetails(Map<String, GroupUser> users, UserModel userModel) {
        name.setText(title);
        status.setText("(" + userModel.getUserStatus() + ")");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM YYYY", Locale.getDefault());
        if (userModel.getLastupdate() != null) {
            tvDate.setVisibility(View.VISIBLE);
            String time = dateFormat.format(userModel.getLastupdate());
            tvDate.setText(time);
        }


        if (userModel.getMood() != null)
            tvSingleAbout.setText(userModel.getMood());
        String imageUrl = userModel.getUserphoto();
        tvMobileNumber.setText("+91 " + userModel.getMobile());
        Log.e("Profile", "ikmage=" + userModel.getUserphoto());
        if (userModel.getUserphoto().isEmpty()) {
            ivDefault.setVisibility(View.VISIBLE);
            ivProfileImage.setVisibility(View.GONE);
        } else {
            ivDefault.setVisibility(View.GONE);
            ivProfileImage.setVisibility(View.VISIBLE);
            imageUrl = imageUrl.replaceAll(" ", "%20");
            Glide.with(getApplicationContext()).load(imageUrl)
                    .into(ivProfileImage);
        }
    }

    private void getRoomDetails() {
        FirebaseFirestore.getInstance().collection("rooms")
                .document(roomId).addSnapshotListener((documentSnapshot, e) -> {
            GroupDetailModel object = documentSnapshot.toObject(GroupDetailModel.class);
            users = (Map<String, GroupUser>) documentSnapshot.get("members");
            updateBlock();
        });
    }

    private void updateBlock() {
        if (users != null) {
            for (Object user : users.values()) {
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(user);
                GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
                if (!gUser.getId().equalsIgnoreCase(currentUser.getUserId())) {
                    if (gUser.isBlocked()) {
                        isBlocked = true;
                    } else {
                        isBlocked = false;
                    }
                    break;
                }
            }
        } else {
            findViewById(R.id.cvBlock).setVisibility(View.GONE);
        }
        updateUi();

    }


    private void blockUnblockUser() {
        Map<String, GroupUser> newMap = new HashMap<>();
        if (users != null) {
            for (Object user : users.values()) {
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(user);
                GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
                if (!gUser.getId().equalsIgnoreCase(currentUser.getUserId())) {
                    gUser.setBlocked(!isBlocked);
                }
                newMap.put(gUser.getId(), gUser);
            }
            users.clear();
            users.putAll(newMap);
            ProgressDialog.show(getSupportFragmentManager());
            new UpdateGroupAsync().execute();
        }
    }

    @Override
    public void onSuccess(Message message) {
        getRoomDetails();
        ProgressDialog.hide(getSupportFragmentManager());
    }

    private void updateUi() {
        if (isBlocked) {
            tvBlock.setText("Unblock");
        } else {
            tvBlock.setText("Block");
        }
    }

    class UpdateGroupAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            chatRoom.setMembers(new Gson().toJson(users));
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel roomModel) {
            super.onPostExecute(roomModel);
            updateRoom(roomModel);
        }
    }


    private void updateRoom(ChatRoomModel roomModel) {
        ChatRoomService.updateGroupOnline(SingleProfileDetailActivity.this, roomModel, false, this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}