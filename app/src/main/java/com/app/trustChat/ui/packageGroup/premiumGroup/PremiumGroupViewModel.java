package com.app.trustChat.ui.packageGroup.premiumGroup;

import com.app.trustChat.base.BaseViewModel;

public class PremiumGroupViewModel extends BaseViewModel<PremiumGroupNavigator> {

    public void viewPackageDetails() {
        getNavigator().openPackageDialog();
    }

}
