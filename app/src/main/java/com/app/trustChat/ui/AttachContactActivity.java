package com.app.trustChat.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.ui.chat.ChatActivity;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

public class AttachContactActivity extends AppCompatActivity {

    private RecyclerViewAdapter firestoreAdapter;
    public static final int REQUEST_READ_CONTACTS = 79;
    private RecyclerView recyclerView;
    List<UserModel> actualList = new ArrayList<>();
    private UserModel currentUser;
    private List<UserModel> selectedUsers = new ArrayList<>();

    /*private progressBar*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_list_fragment);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        currentUser = MyApplication.getInstance().getCurrentUser();
        findViewById(R.id.ivBack).setOnClickListener(v -> onBackPressed());
        filterContactList();
        ImageView ivSend = findViewById(R.id.btnRefresh);
        ivSend.setImageDrawable(getResources().getDrawable(R.drawable.ic_next_arrow));
        ivSend.setOnClickListener(v -> {
            if (selectedUsers.size() < 1) {
                Toast.makeText(this, "Please Select Contacts", Toast.LENGTH_SHORT).show();
            } else {
                Intent resultIntent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("CONTACT_LIST", (ArrayList<? extends Parcelable>) selectedUsers);
                resultIntent.putExtras(bundle);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

    }


    private void filterContactList() {
        firestoreAdapter = new RecyclerViewAdapter(actualList);
        recyclerView.setAdapter(firestoreAdapter);
        new ContactDbAsync().execute();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    filterContactList();
                } else {
                    filterContactList();
                }
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }


    class RecyclerViewAdapter extends RecyclerView.Adapter<CustomViewHolder> {

        List<UserModel> list;

        RecyclerViewAdapter(List<UserModel> list) {
            this.list = list;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CustomViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_user, parent, false));
        }

        @Override
        public void onBindViewHolder(CustomViewHolder viewHolder, int position) {
            String myUid = SharedPrefManager.getInstance(AttachContactActivity.this)
                    .get(SharedPrefManager.Key.USER_ID, "");
            final UserModel user = list.get(position);
            viewHolder.user_name.setText(user.getUsernm());
            viewHolder.user_msg.setText("Invite");
            viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
            viewHolder.user_photo.setVisibility(View.GONE);
            viewHolder.tvImageAvatar.setText(String.valueOf(user.getUsernm().charAt(0)));

            if (user.getEmail() != null) {
                if (myUid.equals(user.getUserId())) {
                    viewHolder.itemView.setVisibility(View.INVISIBLE);
                    viewHolder.itemView.getLayoutParams().height = 0;
                    return;
                }
                viewHolder.user_msg.setText(user.getMood() == null ? "Hey I am using TrustApp" : user.getMood());

                if (!user.getUserphoto().isEmpty()) {
                    Glide.with(AttachContactActivity.this).load(user.getUserphoto())
                            .into(viewHolder.user_photo);
                    viewHolder.user_photo.setVisibility(View.VISIBLE);
                    viewHolder.tvImageAvatar.setVisibility(View.GONE);
                } else {
                    viewHolder.tvImageAvatar.setVisibility(View.VISIBLE);
                    viewHolder.user_photo.setVisibility(View.GONE);
                    viewHolder.tvImageAvatar.setText(String.valueOf(user.getUsernm().charAt(0)));
                }
            }
            if (user.isSelected()) {
                viewHolder.ivSelect.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ivSelect.setVisibility(View.GONE);
            }
            viewHolder.itemView.setOnClickListener(v -> {

                if (!list.get(position).isSelected() && selectedUsers.size() > 4) {
                    Toast.makeText(AttachContactActivity.this, "Can not select more than 5 contacts", Toast.LENGTH_SHORT).show();
                } else {
                    list.get(position).setSelected(!list.get(position).isSelected());
                    if (list.get(position).isSelected()) {
                        selectedUsers.add(list.get(position));
                    } else {
                        selectedUsers.remove(list.get(position));
                    }
                    notifyItemChanged(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }
    }

    private String getRoomId(String userId) {
        Integer myId = Integer.valueOf(currentUser.getUserId());
        Integer opponentId = Integer.valueOf(userId);
        return myId > opponentId ? myId + "_" + opponentId : opponentId + "_" + myId;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    private static class CustomViewHolder extends RecyclerView.ViewHolder {
        CircleImageView user_photo;
        TextView user_name;
        TextView tvImageAvatar;
        TextView user_msg;
        RelativeLayout ivSelect;

        CustomViewHolder(View view) {
            super(view);
            user_photo = view.findViewById(R.id.user_photo);
            user_name = view.findViewById(R.id.user_name);
            user_msg = view.findViewById(R.id.user_msg);
            ivSelect = view.findViewById(R.id.ivSelect);
            tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
        }
    }


    private class ContactDbAsync extends AsyncTask<Void, Void, List<UserModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<UserModel> doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            List<UserModel> list = repository.getUsers();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);
            if (userModels != null && userModels.size() > 0) {
                actualList.clear();
                actualList.addAll(userModels);
                Collections.sort(actualList, (o1, o2) -> Boolean.compare(o2.isContact(), o1.isContact()));
                firestoreAdapter.notifyDataSetChanged();
            }
        }
    }

    public void createChattingRoom(UserModel opponent, DocumentReference room) {

        String roomId = getRoomId(opponent.getUserId());

        Intent intent = new Intent(AttachContactActivity.this, ChatActivity.class);
        intent.putExtra("subject", currentUser.getUsernm() + " created this group");
        intent.putExtra("roomTitle", opponent.getUsernm());
        intent.putExtra("roomImage", opponent.getUserphoto());
        intent.putExtra("roomId", roomId);
        startActivity(intent);
    }

}
