package com.app.trustChat.ui.userProfile;

public interface UserProfileNavigator {

    void callEditName();

    void callEditAbout();

    void callBack();

    void onProfilePickChooser();
}
