package com.app.trustChat.ui.groupProfileDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupDetailModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.chat.ChatActivity;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.groupChat.AddUsersToGroupActivity;
import com.app.trustChat.ui.main.ProgressDialog;
/*import com.app.trustChat.ui.singleChatProfileDetails.SingleChatProfileDeatilsActivity;*/
import com.app.trustChat.ui.singleChatProfileDetails.SingleProfileDetailActivity;
import com.app.trustChat.utils.HeaderView;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class GroupProfileDeatilsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, MessageCallback {

    private UserModel userModel;
    private HeaderView toolbarHeaderView;
    private HeaderView floatHeaderView;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private boolean isHideToolbarView = false;
    private RecyclerView rvAddParticipants;
    private String roomId;
    private Map<String, GroupUser> users = new HashMap<>();
    private RelativeLayout rlExit;
    private RelativeLayout rlAddParticipants;
    private boolean isGroupMember;
    private boolean isAdmin;
    private AddParticipantsAdapter adapter;
    private CardView cvDeleteGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_profile_deatils);
        findByViewIds();
        roomId = getIntent().getStringExtra("roomId");
        userModel = MyApplication.getInstance().getCurrentUser();
    }



    private void findByViewIds() {
        cvDeleteGroup = findViewById(R.id.cvDeleteGroup);
        rlExit = findViewById(R.id.rlExit);
        rlAddParticipants = findViewById(R.id.rlAddParticipants);
        toolbarHeaderView = findViewById(R.id.toolbar_header_view);
        floatHeaderView = findViewById(R.id.float_header_view);
        appBarLayout = findViewById(R.id.appbar);
        toolbar = findViewById(R.id.toolbar);
        rvAddParticipants = findViewById(R.id.rvAddParticipants);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvAddParticipants.setLayoutManager(layoutManager);
        adapter = new AddParticipantsAdapter(arrayList, this);
        rvAddParticipants.setAdapter(adapter);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //initUi();

        /*Replace with own custom image */
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.photo);
        int color = getDominantColor(icon);
        collapsingToolbarLayout.setContentScrimColor(color);
        collapsingToolbarLayout.setStatusBarScrimColor(color);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(darker(color, 0.8f));
        }
        /*Replace with own custom image */
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        rlAddParticipants.setOnClickListener(v -> {
            Intent intent = new Intent(GroupProfileDeatilsActivity.this, AddUsersToGroupActivity.class);
            intent.putExtra("roomId",roomId);
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static int darker(int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb(a,
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    private void initUi() {

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }

    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.group_menu_main, menu);
        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_person_add) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<GroupUser> arrayList = new ArrayList<>();

    private void setAddParticipants(Map<String, GroupUser> users, GroupDetailModel object) {
        arrayList.clear();
        adapter.notifyDataSetChanged();
        isGroupMember = false;
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
            if(groupUser.getId().equalsIgnoreCase(userModel.getUserId())){
                isGroupMember = true;
            }
            arrayList.add(groupUser);
        }
        if (!isGroupMember) {
            cvDeleteGroup.setVisibility(View.VISIBLE);
            rlExit.setVisibility(View.GONE);
            rlAddParticipants.setVisibility(View.GONE);
        } else {
            cvDeleteGroup.setVisibility(View.GONE);
            rlExit.setVisibility(View.VISIBLE);
            rlAddParticipants.setVisibility(View.VISIBLE);
        }
        for (GroupUser user : arrayList) {
            if (user.isAdmin() && user.getId().equalsIgnoreCase(userModel.getUserId())) {
                isAdmin = true;
                break;
            }
        }
        ImageView image = findViewById(R.id.image);

        adapter.notifyDataSetChanged();
        if (object.getPhoto() != null) {
            try {
                Glide.with(this).load(object.getPhoto())
                        .into(image);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        TextView tvTotalParticipants = findViewById(R.id.tvTotalParticipants);
        tvTotalParticipants.setText(arrayList.size() + " Participants");


        appBarLayout.addOnOffsetChangedListener(this);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        if (object.getCreatedAt() != null) {
            toolbarHeaderView.bindTo(object.getTitle(), object.getSubject() + " " + dateFormat.format(new Date(object.getCreatedAt())));
            floatHeaderView.bindTo(object.getTitle(), object.getSubject() + " " + dateFormat.format(new Date(object.getCreatedAt())));
        }
    }


    private void getRoomDetails() {
        FirebaseFirestore.getInstance().collection("rooms")
                .document(roomId).addSnapshotListener((documentSnapshot, e) -> {
                    GroupDetailModel object = documentSnapshot.toObject(GroupDetailModel.class);
                    users = (Map<String, GroupUser>) documentSnapshot.get("members");
                    object.setCreatedAt((Long) documentSnapshot.get("createdAt"));
                    setAddParticipants(users, object);
                });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getRoomDetails();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }


    public void exitFromGroup(View view) {
        ProgressDialog.show(getSupportFragmentManager());
        users.remove(userModel.getUserId());
        new LeftGroupAsync().execute();
    }

    public void deleteGroup(View view) {
        ProgressDialog.show(getSupportFragmentManager());
        new DeleteGroupAsync().execute();
    }

    public void makeAdmin(GroupUser groupUser) {
        ProgressDialog.show(getSupportFragmentManager());
        Map<String, GroupUser> newMap = new HashMap<>();
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
            if (gUser.getId().equalsIgnoreCase(groupUser.getId())) {
                gUser.setAdmin(true);
            }
            newMap.put(gUser.getId(), gUser);
        }

        users.clear();
        users.putAll(newMap);
        new LeftGroupAsync().execute();
    }


    public void removeUserFromRoom(GroupUser groupUser) {
        ProgressDialog.show(getSupportFragmentManager());
        Map<String, GroupUser> newMap = new HashMap<>();
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
            if (!gUser.getId().equalsIgnoreCase(groupUser.getId())) {
                newMap.put(gUser.getId(), gUser);
            }
        }

        users.clear();
        users.putAll(newMap);
        new LeftGroupAsync().execute();
    }


    public void showOption(GroupUser user) {
        Dialog dialog = new Dialog(GroupProfileDeatilsActivity.this, R.style.Theme_Dialog);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_room_user_options, null);
        TextView tvStartChat = v.findViewById(R.id.tvStartChat);
        TextView tvViewContact = v.findViewById(R.id.tvViewContact);
        TextView tvRemoveUser = v.findViewById(R.id.tvRemoveUser);
        TextView tvMakeAdmin = v.findViewById(R.id.tvMakeAdmin);

        if (!isGroupMember) {
            tvRemoveUser.setVisibility(View.GONE);
            tvMakeAdmin.setVisibility(View.GONE);
        }
        if (isAdmin) {
            tvRemoveUser.setVisibility(View.VISIBLE);
            tvMakeAdmin.setVisibility(View.VISIBLE);
        } else {
            tvRemoveUser.setVisibility(View.GONE);
            tvMakeAdmin.setVisibility(View.GONE);
        }
        if (user.isAdmin()) {
            tvMakeAdmin.setVisibility(View.GONE);
        }
        tvStartChat.setText(MessageFormat.format("Message {0}", user.getName()));
        tvViewContact.setText(MessageFormat.format("View {0}", user.getName()));
        tvRemoveUser.setText(MessageFormat.format("Remove {0}", user.getName()));
        dialog.setContentView(v);
        dialog.show();
        tvMakeAdmin.setOnClickListener(v13 -> {
            makeAdmin(user);
            dialog.dismiss();
        });
        tvRemoveUser.setOnClickListener(v12 -> {
            removeUserFromRoom(user);
            dialog.dismiss();
        });
        tvStartChat.setOnClickListener(v1 -> {
            Intent intent = new Intent(GroupProfileDeatilsActivity.this, ChatActivity.class);
            intent.putExtra("roomTitle", user.getName());
            intent.putExtra("roomId", getRoomId(user.getId()));
            intent.putExtra("roomImage", user.getPhoto());
            intent.putExtra("toUid", user.getId());
            intent.putExtra("subject", "");
            intent.putExtra("LAST_MSG", "");
            dialog.dismiss();
            startActivity(intent);
        });
        tvViewContact.setOnClickListener(v12 -> {
            Intent intent = new Intent(GroupProfileDeatilsActivity.this, SingleProfileDetailActivity.class);
            intent.putExtra("toUid", user.getId());
            intent.putExtra("isMe", false);
            dialog.dismiss();
            startActivity(intent);
        });
    }

    private String getRoomId(String userId) {
        Integer myId = Integer.valueOf(userModel.getUserId());
        Integer opponentId = Integer.valueOf(userId);
        return myId > opponentId ? myId + "_" + opponentId : opponentId + "_" + myId;
    }

    public class AddParticipantsAdapter extends RecyclerView.Adapter<AddParticipantsAdapter.ViewHolder> {
        private List<GroupUser> listCategoryList;
        Context context;


        public AddParticipantsAdapter(List<GroupUser> listCategoryList, Context context) {
            this.listCategoryList = listCategoryList;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_group_members, parent, false);
            AddParticipantsAdapter.ViewHolder viewHolder = new AddParticipantsAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull AddParticipantsAdapter.ViewHolder holder, int position) {
            GroupUser packageModel = listCategoryList.get(position);
            holder.setData(context, packageModel);
            holder.itemView.setOnClickListener(v -> {
                if (!listCategoryList.get(position).getId().equalsIgnoreCase(userModel.getUserId()))
                    showOption(listCategoryList.get(position));
            });

            holder.itemView.setOnLongClickListener(v -> {
                if (!listCategoryList.get(position).getId().equalsIgnoreCase(userModel.getUserId()))
                    showOption(listCategoryList.get(position));
                return true;
            });
        }

        @Override
        public int getItemCount() {

            return listCategoryList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView image;
            private TextView tvParticipantsName, tvAbout, tvGroupAdmin;


            ViewHolder(@NonNull View itemView) {
                super(itemView);
                tvGroupAdmin = itemView.findViewById(R.id.tvGroupAdmin);
                tvParticipantsName = itemView.findViewById(R.id.tvParticipantsName);
                tvAbout = itemView.findViewById(R.id.tvAbout);
                image = itemView.findViewById(R.id.ivProfile);
            }

            private void setData(Context context, GroupUser groupUser) {
                tvAbout.setText(groupUser.getMood());
                tvParticipantsName.setText(groupUser.getName());
                if (groupUser.getPhoto() != null) {
                    Glide.with(context).load(groupUser.getPhoto())
                            .into(image);
                }
                if (groupUser.isAdmin()) {
                    tvGroupAdmin.setVisibility(View.VISIBLE);
                }
            }

        }

    }

    class LeftGroupAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            chatRoom.setMembers(new Gson().toJson(users));
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel roomModel) {
            super.onPostExecute(roomModel);
            updateRoom(roomModel);
        }
    }


    private void updateRoom(ChatRoomModel roomModel) {
        ChatRoomService.updateGroupOnline(GroupProfileDeatilsActivity.this, roomModel, false, this);
        getRoomDetails();
    }

    @Override
    public void onSuccess(Message message) {
        ProgressDialog.hide(getSupportFragmentManager());
        getRoomDetails();
    }

    private class DeleteGroupAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            repository.deleteChatRoomById(roomId);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ProgressDialog.hide(getSupportFragmentManager());
            Intent intent = new Intent(GroupProfileDeatilsActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

}
