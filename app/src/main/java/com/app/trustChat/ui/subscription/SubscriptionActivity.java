package com.app.trustChat.ui.subscription;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.model.WrapperModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.main.ProgressDialog;
import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SubscriptionActivity extends AppCompatActivity {

    UserModel currentUser;
    String mobile;
    ImageView tvBack;
    LinearLayout llSubscriptionDetails;
    private TextView tvPackageMobile, tvPackageName, tvPackageValidity, tvPackageAmount, tvTrans_id,
            tvTrans_date, tvStart_date, tvRenew_date, tvAcc_no, tvIfsc, tvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(SubscriptionActivity.this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        mobile = currentUser.getMobile();
        Log.e("Premium", "mobile----" + mobile);
        findByViewIds();
        postSubscription(mobile);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void findByViewIds() {
        tvPackageMobile = findViewById(R.id.tvPackageMobile);
        tvPackageName = findViewById(R.id.tvPackageName);
        tvPackageValidity = findViewById(R.id.tvPackageValidity);
        tvPackageAmount = findViewById(R.id.tvPackageAmount);
        tvTrans_id = findViewById(R.id.tvTrans_id);
        tvTrans_date = findViewById(R.id.tvTrans_date);
        tvBack = findViewById(R.id.tvBack);
        tvStart_date = findViewById(R.id.tvStart_date);
        tvRenew_date = findViewById(R.id.tvRenew_date);
        tvAcc_no = findViewById(R.id.tvAcc_no);
        tvIfsc = findViewById(R.id.tvIfsc);
        tvNoData = findViewById(R.id.tvNoData);
        llSubscriptionDetails = findViewById(R.id.llSubscriptionDetails);
    }

    private void postSubscription(String mobile) {
        ProgressDialog.show(getSupportFragmentManager());
        Single<WrapperModel> caller = RetrofitBuilder.getService(SubscriptionActivity.this)
                .getSubscriptionDetails(mobile);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<WrapperModel>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(WrapperModel response) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        Log.e("Response", "checkk----" + response.getDetails().getStatus());
                        Log.e("Response", "checkk----" + response.getDetails().getMsg());

                        if (response.getIs_premium().equalsIgnoreCase("1")) {
                            tvNoData.setVisibility(View.GONE);
                            llSubscriptionDetails.setVisibility(View.VISIBLE);
                            tvPackageMobile.setText("Mobile : " + response.getDetails().getMobile());
                            tvPackageName.setText("Package Name : " + response.getDetails().getPkg_name());
                            tvPackageValidity.setText("Validity : " + response.getDetails().getPkg_validity());
                            tvPackageAmount.setText("Amount : " + response.getDetails().getPkg_amount());
                            tvTrans_id.setText("Transaction id : " + response.getDetails().getTranx_id());
                            tvTrans_date.setText("Transaction Date : " + response.getDetails().getTranx_date());
                            tvStart_date.setText("Start Date : " + response.getDetails().getStart_date());
                            tvRenew_date.setText("Renew Date : " + response.getDetails().getRenew_date());
                            tvAcc_no.setText("Account Number : " + response.getDetails().getAc_no());
                            tvIfsc.setText("IFSC : " + response.getDetails().getIfsc());
                        } else {
                            tvNoData.setVisibility(View.VISIBLE);
                            llSubscriptionDetails.setVisibility(View.GONE);
                        }


                    }


                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getSupportFragmentManager());

                    }
                }));
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }
}