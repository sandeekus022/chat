package com.app.trustChat.ui.contact;

import android.app.Application;

import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.UserRepository;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

class UserListViewModel extends AndroidViewModel {
    MutableLiveData<List<UserModel>> contactList;
    UserRepository repository;

    public UserListViewModel(Application application) {
        super(application);
        repository = new UserRepository(application);
        repository.getUsers();
    }

    public MutableLiveData<List<UserModel>> getContactList() {
        return contactList;
    }

    public void insertAllUsers(){

    }
}
