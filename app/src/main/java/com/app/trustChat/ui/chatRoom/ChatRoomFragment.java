package com.app.trustChat.ui.chatRoom;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.ui.chat.ChatActivity;
import com.app.trustChat.ui.chat.HomeActivity;
import com.bumptech.glide.Glide;
import com.google.common.reflect.TypeToken;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatRoomFragment extends Fragment {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
    private static RecyclerViewAdapter mAdapter;
    private static List<ChatRoomModel> tempList = new ArrayList<>();
    private Context context;
    private static List<ChatRoomModel> roomList = new ArrayList<>();
    private UserModel currentUser;
    String myUid;
    private FirebaseFirestore fireStore;
    private ListenerRegistration chatRoomListener;
    private static boolean isLongClickEnabled;
    private static List<ChatRoomModel> selectedRoomList = new ArrayList<>();
    private List<UserModel> appUsers = new ArrayList<>();

    public static void disableLongClick() {
        for (int i = 0; i < roomList.size(); i++) {
            roomList.get(i).setRoomSelected(false);
        }
        selectedRoomList.clear();
        mAdapter.notifyDataSetChanged();
        isLongClickEnabled = false;
    }

    public static void deleteRooms() {
        new DeleteChatRoomsAsync(selectedRoomList).execute();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_room_fragment, container, false);
        fireStore = FirebaseFirestore.getInstance();
        tempList.clear();
        currentUser = MyApplication.getInstance().getCurrentUser();
        myUid = currentUser.getUserId();
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.getItemAnimator().setChangeDuration(0);
        mAdapter = new RecyclerViewAdapter(roomList);
        recyclerView.setAdapter(mAdapter);
        recyclerView.requestFocus();
        new GetAllRoomsAsync().execute();
        getAppUsers();
        return view;
    }


    void getAppUsers() {
        String listGson = SharedPrefManager.getInstance(getContext())
                .get(SharedPrefManager.Key.USERS_LIST, null);
        List<UserModel> list = new Gson().fromJson(listGson, new TypeToken<ArrayList<UserModel>>() {
        }.getType());
        for (UserModel user : list) {
            if (user.getEmail() != null)
                appUsers.add(user);
        }

    }


    public void filterList(String query) {
        mAdapter.filterList(query);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ChatRoomModel> roomList;

        RecyclerViewAdapter(List<ChatRoomModel> roomList) {
            this.roomList = roomList;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.list_item_dialog, parent, false);
            return new RoomViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            RoomViewHolder roomViewHolder = (RoomViewHolder) holder;

            final ChatRoomModel chatRoomModel = roomList.get(position);

            if (!chatRoomModel.isGroup()) {
                String opponentId = chatRoomModel.getOpponentId();
                if (opponentId != null) {
                    fireStore.collection("users").document(opponentId)
                            .addSnapshotListener((documentSnapshot, e) -> {
                                if (e != null)
                                    return;
                                UserModel userModel = documentSnapshot.toObject(UserModel.class);
                                if (userModel != null)
                                    chatRoomModel.setPhoto(userModel.getUserphoto());
                                new CreateRoomAsync(chatRoomModel);
                            });
                }
            }

            if (!chatRoomModel.isGroup()) {
                for (UserModel userModel : appUsers) {
                    if (userModel.getUserId().equalsIgnoreCase(chatRoomModel.getOpponentId())) {
                        chatRoomModel.setTitle(userModel.getUsernm());
                        chatRoomModel.setPhoto(userModel.getUserphoto());
                        new CreateRoomAsync(chatRoomModel);
                        break;
                    }
                }
            }
            roomViewHolder.room_title.setText(chatRoomModel.getTitle());
            if (chatRoomModel.getLastMsg() == null || chatRoomModel.getLastMsg().isEmpty())
                roomViewHolder.last_msg.setText(chatRoomModel.getSubject());
            else
                roomViewHolder.last_msg.setText(chatRoomModel.getLastMsg());
            if (chatRoomModel.getUpdatedAt() != null)
                roomViewHolder.last_time.setText(simpleDateFormat.format(new Date(chatRoomModel.getUpdatedAt())));
            if (chatRoomModel.getPhoto() == null || chatRoomModel.getPhoto().isEmpty()) {
                ((RoomViewHolder) holder).tvImageAvatar.setVisibility(View.VISIBLE);
                ((RoomViewHolder) holder).room_image.setVisibility(View.GONE);
                if (chatRoomModel.getTitle() != null)
                    ((RoomViewHolder) holder).tvImageAvatar.setText(String.valueOf(chatRoomModel.getTitle().charAt(0)));
            } else {
                Glide.with(context).load(chatRoomModel.getPhoto())
                        .placeholder(R.drawable.ic_user)
                        .into(((RoomViewHolder) holder).room_image);
                ((RoomViewHolder) holder).room_image.setVisibility(View.VISIBLE);
                ((RoomViewHolder) holder).tvImageAvatar.setVisibility(View.GONE);

            }

            if (chatRoomModel.getUnreadCount() > 0) {
                roomViewHolder.unread_count.setText(chatRoomModel.getUnreadCount().toString());
                roomViewHolder.unread_count.setVisibility(View.VISIBLE);
            } else {
                roomViewHolder.unread_count.setVisibility(View.INVISIBLE);
            }

            roomViewHolder.itemView.setOnClickListener(v -> {
                if (HomeActivity.forwardedMessageList.size() != 0) {
                    showConfirmationDialog(chatRoomModel);
                } else {
                    startChat(chatRoomModel);
                }
            });


            roomViewHolder.itemView.setLongClickable(true);

            if (roomList.get(position).isRoomSelected()) {
                ((RoomViewHolder) holder).viewSelect.setVisibility(View.VISIBLE);
            } else {
                ((RoomViewHolder) holder).viewSelect.setVisibility(View.GONE);
            }

        }


        @Override
        public int getItemCount() {
            //updateEmptyView(roomList.size() == 0);
            return roomList.size();
        }

        private void filterList(String query) {
            if (query.length() > 0) {
                List<ChatRoomModel> queryList = new ArrayList<>();
                for (ChatRoomModel room : tempList) {
                    if (room.getTitle() != null)
                        if (room.getTitle().toLowerCase().contains(query.toLowerCase()))
                            queryList.add(room);
                }
                roomList.clear();
                roomList.addAll(queryList);
            } else {
                roomList.clear();
                roomList.addAll(tempList);
            }

            notifyDataSetChanged();
        }

        private class RoomViewHolder extends RecyclerView.ViewHolder {
            private CircleImageView room_image;
            private TextView room_title;
            private TextView last_msg;
            private TextView last_time;
            private TextView tvImageAvatar;
            private TextView unread_count;
            private TextView tvTyping;
            private View viewSelect;

            RoomViewHolder(View view) {
                super(view);
                viewSelect = view.findViewById(R.id.viewSelect);
                room_image = view.findViewById(R.id.room_image);
                room_title = view.findViewById(R.id.room_title);
                tvTyping = view.findViewById(R.id.tvTyping);
                last_msg = view.findViewById(R.id.last_msg);
                last_time = view.findViewById(R.id.last_time);
                tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
                unread_count = view.findViewById(R.id.unread_count);
            }
        }
    }

    private void showConfirmationDialog(ChatRoomModel chatRoomModel) {
        Dialog dialog = new Dialog(getContext(), R.style.Theme_Dialog);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_forward_confirmation, null);
        TextView tvUserMsg = v.findViewById(R.id.tvUserMsg);
        TextView tvOk = v.findViewById(R.id.tvOk);
        tvUserMsg.setText("Forward message to " + chatRoomModel.getTitle() + "?");

        tvOk.setOnClickListener(v1 -> {
            dialog.dismiss();
            startChat(chatRoomModel);
        });
        dialog.setContentView(v);
        dialog.show();
    }

    private void startChat(ChatRoomModel chatRoomModel) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("roomTitle", chatRoomModel.getTitle());
        intent.putExtra("roomId", chatRoomModel.getRoomId());
        intent.putExtra("roomImage", chatRoomModel.getPhoto());
        intent.putExtra("isGroup", chatRoomModel.isGroup());
        intent.putExtra("toUid", chatRoomModel.getOpponentId());
        intent.putExtra("subject", chatRoomModel.getSubject() == null ? "" : chatRoomModel.getSubject());
        intent.putExtra("LAST_MSG", chatRoomModel.getLastMsg());
        startActivity(intent);
    }

    private void updateSelected(ChatRoomModel roomModel, boolean roomSelected) {
        if (roomSelected)
            selectedRoomList.add(roomModel);
        else
            selectedRoomList.remove(roomModel);

        if (selectedRoomList.size() > 0) {
            isLongClickEnabled = true;
            HomeActivity.setLongClickEnabled(true, selectedRoomList.size());
        } else {
            isLongClickEnabled = false;
            HomeActivity.setLongClickEnabled(true, selectedRoomList.size());
        }
    }


    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    private static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                return resolveInfo.activityInfo.name;
            }
        }
        return null;
    }


    private static class GetAllRoomsAsync extends AsyncTask<Void, Void, List<ChatRoomModel>> {

        @Override
        protected List<ChatRoomModel> doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            return repository.getAllChatRooms();
        }

        @Override
        protected void onPostExecute(List<ChatRoomModel> rooms) {
            super.onPostExecute(rooms);
            roomList.clear();
            tempList.clear();
            roomList.addAll(rooms);
            tempList.addAll(rooms);
            try {
                Collections.sort(roomList, (o1, o2) -> o2.getUpdatedAt().compareTo(o1.getUpdatedAt()));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startListening();
    }


    @Override
    public void onPause() {
        super.onPause();
        chatRoomListener.remove();
        chatRoomListener = null;
    }


    private void startListening() {
        chatRoomListener = fireStore.collection("rooms").whereGreaterThanOrEqualTo("members." + currentUser.getUserId()
                + ".messageCount", 0).addSnapshotListener((documentSnapshots, e) -> {
            int unreadTotal = 0;
            for (DocumentChange change : documentSnapshots.getDocumentChanges()) {
                ChatRoomModel chatRoomModel = new ChatRoomModel();
                Map<String, GroupUser> users = (Map<String, GroupUser>) change.getDocument().get("members");
                chatRoomModel.setUnreadCount(users.size());
                String oneToOneTitle = null;
                String opponentId = null;
                for (Object groupUser : users.values()) {
                    Gson gson = new Gson();
                    JsonElement jsonElement = gson.toJsonTree(groupUser);
                    GroupUser user = gson.fromJson(jsonElement, GroupUser.class);
                    if (myUid.equalsIgnoreCase(user.getId())) {
                        Integer unread = user.getMessageCount();
                        unreadTotal += unread;
                        chatRoomModel.setUnreadCount(unread);
                    } else {
                        oneToOneTitle = user.getMobile();
                        opponentId = user.getId();
                        chatRoomModel.setTitle(oneToOneTitle);
                        chatRoomModel.setOpponentId(user.getId());
                    }
                }
                boolean isGroup = (change.getDocument().getBoolean("isGroup"));
                if (isGroup) {
                    chatRoomModel.setTitle(change.getDocument().getString("title"));
                } else {
                    for (UserModel user : appUsers) {
                        if (user.getUserId().equalsIgnoreCase(opponentId)) {
                            chatRoomModel.setOpponentId(user.getUserId());
                        }
                    }
                    chatRoomModel.setTitle(oneToOneTitle);
                }
                chatRoomModel.setGroup(change.getDocument().getBoolean("isGroup"));
                chatRoomModel.setRoomId(change.getDocument().getId());
                chatRoomModel.setUserId(change.getDocument().getString("userId"));
                chatRoomModel.setSubject(change.getDocument().getString("subject"));
                chatRoomModel.setPhoto(change.getDocument().getString("photo"));
                chatRoomModel.setLastMsg(change.getDocument().getString("lastMsg"));
                chatRoomModel.setStatus(change.getDocument().getString("status"));
                chatRoomModel.setUpdatedAt(change.getDocument().getLong("updatedAt"));
                chatRoomModel.setCreatedAt(change.getDocument().getLong("createdAt"));
                chatRoomModel.setUnreadCount(unreadTotal);
                chatRoomModel.setMembers(new Gson().toJson(users));
                switch (change.getType()) {
                    case ADDED:
                    case MODIFIED:
                        new CreateRoomAsync(chatRoomModel).execute();
                        break;
                    case REMOVED:
                }

            }
        });
    }

    class CreateRoomAsync extends AsyncTask<Void, Void, Long> {
        ChatRoomModel chatRoomModel;

        public CreateRoomAsync(ChatRoomModel chatRoomModel) {
            this.chatRoomModel = chatRoomModel;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(chatRoomModel.getRoomId());
            if (chatRoom == null) {
                createChatRoom(chatRoomModel);
                return repository.createChatRoom(chatRoomModel);
            } else {
                return Long.valueOf(repository.update(chatRoomModel));
            }
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            updateRoom(chatRoomModel);
        }
    }

    private void createChatRoom(ChatRoomModel chatRoomModel) {
        String members = chatRoomModel.getMembers();
        Map<String, GroupUser> users = new Gson().fromJson(
                members, new com.google.gson.reflect.TypeToken<HashMap<String, Object>>() {
                }.getType());
        Gson gson = new Gson();
        for (Object value : users.values()) {
            JsonElement jsonElement = gson.toJsonTree(value);
            GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
            for (UserModel userModel : appUsers) {
                if (groupUser.getId().equalsIgnoreCase(userModel.getUserId())) {
                    groupUser.setName(userModel.getUsernm());
                    users.put(groupUser.getId(), groupUser);
                    break;
                }
            }
        }
        chatRoomModel.setMembers(new Gson().toJson(users));
        ChatRoomService.updateGroupOnline(context, chatRoomModel, false, null);

    }

    private void updateRoom(ChatRoomModel roomModel) {
        roomModel.setUpdatedAt(System.currentTimeMillis());
        if (roomList.contains(roomModel)) {
            for (int i = 0; i < roomList.size(); i++) {
                if (roomList.get(i).getRoomId().equalsIgnoreCase(roomModel.getRoomId())) {
                    roomList.get(i).setLastMsg(roomModel.getLastMsg());
                    roomList.get(i).setUnreadCount(roomModel.getUnreadCount());
                    break;
                }
            }
        } else {
            roomList.add(roomModel);
            tempList.add(roomModel);
        }
        Collections.sort(roomList, (o1, o2) -> o2.getUpdatedAt().compareTo(o1.getUpdatedAt()));
        mAdapter.notifyDataSetChanged();
    }

    private static class DeleteChatRoomsAsync extends AsyncTask<Void, Void, Void> {
        List<ChatRoomModel> chatRoomList;

        public DeleteChatRoomsAsync(List<ChatRoomModel> chatRoomList) {
            this.chatRoomList = chatRoomList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            for (ChatRoomModel chatRoomModel : chatRoomList) {
                repository.deleteChatRoomById(chatRoomModel.getRoomId());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new GetAllRoomsAsync().execute();
        }
    }
}
