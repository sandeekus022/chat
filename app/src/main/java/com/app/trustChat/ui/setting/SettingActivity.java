package com.app.trustChat.ui.setting;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.userProfile.UserProfileActivity;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;

public class SettingActivity extends AppCompatActivity {
    private ImageView ivBack, ivUserImage;
    private TextView tvUserName, tvStatus;
    private RelativeLayout rlUSerProfile, llContactUS, llTermAndConditions, llPrivacyPolicy, llAppInfo, llInviteFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        findViewById();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        rlUSerProfile.setOnClickListener(v -> {
            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra("isMe",true);
            startActivity(intent);
        });

        ivBack.setOnClickListener(v -> {
            onBackPressed();
        });


        llContactUS.setOnClickListener(v -> {
            Toast.makeText(this, "Contact Us!!!", Toast.LENGTH_SHORT).show();
        });


        llTermAndConditions.setOnClickListener(v -> {
            Toast.makeText(this, "Terms & Condition!!!", Toast.LENGTH_SHORT).show();
        });
        llPrivacyPolicy.setOnClickListener(v -> {
            Toast.makeText(this, "Privacy Policy!!!", Toast.LENGTH_SHORT).show();
        });


        llAppInfo.setOnClickListener(v -> {
            Toast.makeText(this, "App Info!!!", Toast.LENGTH_SHORT).show();
        });


        llInviteFriends.setOnClickListener(v -> {
            shareApp();
        });
    }

    private void findViewById() {
        ivBack = findViewById(R.id.ivBack);
        ivUserImage = findViewById(R.id.ivUserImage);
        tvUserName = findViewById(R.id.tvUserName);
        tvStatus = findViewById(R.id.tvStatus);
        llContactUS = findViewById(R.id.llContactUS);
        llTermAndConditions = findViewById(R.id.llTermAndConditions);
        llPrivacyPolicy = findViewById(R.id.llPrivacyPolicy);
        llAppInfo = findViewById(R.id.llAppInfo);
        llInviteFriends = findViewById(R.id.llInviteFriends);
        rlUSerProfile = findViewById(R.id.rlUSerProfile);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
        UserModel currentUser = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);

        Glide.with(SettingActivity.this).load(currentUser.getUserphoto())
                .placeholder(R.drawable.ic_user)
                .into(ivUserImage);
        tvUserName.setText(currentUser.getUsernm());
        tvStatus.setText(currentUser.getMood());
    }

    /* access modifiers changed from: private */
    public void shareApp() {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.app_name));
            String sb = "\nLet's chat on Trust App! it's fast, simple and secure app we can use to message and call each other for free. Get it at\n\n" +
                    "https://play.google.com/store/apps/details?id=" +
                    getPackageName() +
                    "\n\nIf you like this application then share with your buddies and support Made in India motive and don’t forget to rate us on play store!";
            intent.putExtra("android.intent.extra.TEXT", sb);
            startActivity(Intent.createChooser(intent, "Invite a friend via..."));
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}