package com.app.trustChat.ui.userProfile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.app.trustChat.BuildConfig;
import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.MediaUploadModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.networking.APICallbackListener;
import com.app.trustChat.networking.ApiError;
import com.app.trustChat.networking.GenericApiCaller;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.utils.BitmapUtils;
import com.app.trustChat.utils.FileUtil;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class UserProfileActivity extends AppCompatActivity {

    private UserProfileViewModel mViewModel;
    private Context context;
    private View view;
    private FrameLayout Frame_Layout;
    private ImageView ivEditName, ivBack, ivEditAbout;
    public static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private String currentPhotoPath;
    public static final int GALLERY_PICK_CODE = 10;
    private UserModel userModel;
    private boolean isMe;
    private UserModel currentUser;
    private DocumentReference firestore;
    private String mTempPhotoPath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        findViewById();
        setOnClickListeners();
        updateUser();
        firestore = FirebaseFirestore.getInstance().collection("users").document(String.valueOf(currentUser.getUserId()));

    }

    TextView tvUserName, tvAboutName, tvMobileNumber;
    ImageView ivUserImage;

    private void findViewById() {
        Frame_Layout = findViewById(R.id.Frame_Layout);
        ivEditName = findViewById(R.id.ivEditName);
        ivEditAbout = findViewById(R.id.ivEditAbout);
        ivBack = findViewById(R.id.ivBack);
        tvUserName = findViewById(R.id.tvUserName);
        tvAboutName = findViewById(R.id.tvAboutName);
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        ivUserImage = findViewById(R.id.profile_image);
    }

    private void updateUser() {
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);

        Glide.with(UserProfileActivity.this).load(currentUser.getUserphoto())
                .placeholder(R.drawable.ic_user)
                .into(ivUserImage);
        tvUserName.setText(currentUser.getUsernm());
        tvAboutName.setText(currentUser.getMood());
        tvMobileNumber.setText(currentUser.getMobile());
    }

    private void setOnClickListeners() {
        Frame_Layout.setOnClickListener(v -> {
            if (checkAndRequestPermission()) {
                openDialogChooser();

            }
        });
        ivEditName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog("editName");
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ivEditAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog("editAbout");
            }
        });

    }

    public void editDialog(String check) {
        final Dialog registerUser = new Dialog(this);
        registerUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerUser.setContentView(R.layout.edit_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(registerUser.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        registerUser.getWindow().setAttributes(lp);

        registerUser.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        registerUser.setCancelable(false);
        registerUser.show();
        RelativeLayout rlNameDialog = registerUser.findViewById(R.id.rlNameDialog);
        RelativeLayout rlAboutDialog = registerUser.findViewById(R.id.rlAboutDialog);
        RelativeLayout relativeLayout = registerUser.findViewById(R.id.rlDailog);

        TextView tvSave = registerUser.findViewById(R.id.tvSave);
        TextView tvAboutSave = registerUser.findViewById(R.id.tvAboutSave);
        TextView tvCancel = registerUser.findViewById(R.id.tvCancel);
        TextView tvAboutCancel = registerUser.findViewById(R.id.tvAboutCancel);
        EditText etEnterName = registerUser.findViewById(R.id.etEnterName);
        EditText etAddAbout = registerUser.findViewById(R.id.etAddAbout);

        if (check.equalsIgnoreCase("editName")) {
            rlNameDialog.setVisibility(View.VISIBLE);
            rlAboutDialog.setVisibility(View.GONE);
        } else {
            rlNameDialog.setVisibility(View.GONE);
            rlAboutDialog.setVisibility(View.VISIBLE);
        }

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etEnterName.getText().toString().length() <= 0) {
                    etEnterName.setError("Please Enter Name");
                } else {
                    currentUser.setUsernm(etEnterName.getText().toString().trim());
                    currentUser.setLastupdate(System.currentTimeMillis());
                    firestore.set(currentUser);
                    MyApplication.saveCurrentUser(currentUser);
                    updateUser();
                    registerUser.dismiss();
                }
            }
        });

        tvAboutSave.setOnClickListener(v -> {
            if (etAddAbout.getText().toString().length() <= 0) {
                etAddAbout.setError("Please write something");
            } else {
                currentUser.setMood(etAddAbout.getText().toString().trim());
                currentUser.setLastupdate(System.currentTimeMillis());
                firestore.set(currentUser);
                MyApplication.saveCurrentUser(currentUser);
                updateUser();
                registerUser.dismiss();
            }
        });


        tvCancel.setOnClickListener(v -> registerUser.dismiss());
        tvAboutCancel.setOnClickListener(v -> registerUser.dismiss());


    }


    public void openDialogChooser() {
        final Dialog registerUser = new Dialog(this);
        registerUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerUser.setContentView(R.layout.chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(registerUser.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        registerUser.getWindow().setAttributes(lp);
        registerUser.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        registerUser.setCancelable(false);
        registerUser.show();

        LinearLayout galleryId = registerUser.findViewById(R.id.llGallery);
        LinearLayout cameraId = registerUser.findViewById(R.id.llCamera);
        RelativeLayout relativeLayout = registerUser.findViewById(R.id.rlDailog);

        cameraId.setOnClickListener(v -> {
            registerUser.dismiss();
            launchCamera();
        });

        galleryId.setOnClickListener(v -> {
            registerUser.dismiss();
            startActivityForResult(Intent.createChooser(getGalleryPickerIntent(), "Select Profile Picture"), GALLERY_PICK_CODE);

        });


        relativeLayout.setOnClickListener(v -> registerUser.dismiss());

    }

    private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists()) {
            System.out.println("File exists");
        } else {
            System.out.println("File not exists");
        }
        File file = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String str = "ImageView";
        if (requestCode == GALLERY_PICK_CODE && resultCode == Activity.RESULT_OK && data != null) {
            try {
                Log.e("UCrop", "data---" + data.getData());
                Uri destinationUri = Uri.fromFile(getImageFile());
                openCropActivity(data.getData(), destinationUri);
            } catch (Exception ex) {
                Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_ACTION_PICK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap mResultsBitmap = BitmapUtils.resamplePic(this, mTempPhotoPath);
            Uri resultUri = Uri.fromFile(new File(BitmapUtils.saveImage(this, mResultsBitmap)));

            try {
                File file = FileUtil.from(this, resultUri);
                uploadProfileImage(this, file);
                ProgressDialog.show(getSupportFragmentManager());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            Uri resultUri = UCrop.getOutput(data);

            File file = null;
            try {
                file = FileUtil.from(getApplicationContext(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ProgressDialog.show(getSupportFragmentManager());
            uploadProfileImage(this, file);
            Log.e("UCrop", "resultUri---" + resultUri);

        } else if (resultCode == UCrop.RESULT_ERROR) {
            Throwable cropError = UCrop.getError(data);
            Log.e("UCrop", "cropError---" + cropError);
        }

    }

    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(this, R.color.colorOrange));
        UCrop.of(sourceUri, destinationUri)
                //.withAspectRatio(5f, 5f)
                .start(this);
    }


    public void uploadProfileImage(Context context, File file) {
        MultipartBody.Part requestImage = null;
        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), currentUser.getMobile());
        RequestBody image = RequestBody.create(MediaType.parse("image/*"), file);
        requestImage = MultipartBody.Part.createFormData("image", file.getName(), image);

        new GenericApiCaller<>(context, RetrofitBuilder.getService(context)
                .uploadProfileImage(requestImage, mobile))
                .doApiCall(new APICallbackListener<MediaUploadModel>() {
                    @Override
                    public void onSuccess(Response<MediaUploadModel> response) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        if (response.body().getStatus() == 1) {
                            Toast.makeText(context, "Profile Picture Upload Successfully\nIt will reflect soon on your profile",
                                    Toast.LENGTH_SHORT).show();
                            SharedPrefManager.getInstance(UserProfileActivity.this)
                                    .set(SharedPrefManager.Key.PROFILE_PIC, response.body().getImage());
                            currentUser.setUserphoto(response.body().getImage());
                            Glide.with(UserProfileActivity.this).load(response.body().getImage())
                                    .placeholder(R.drawable.ic_user)
                                    .into(ivUserImage);
                            firestore.set(currentUser);
                            MyApplication.saveCurrentUser(currentUser);
                        } else {
                            Toast.makeText(context, "Profile Picture Upload Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(ApiError object) {
                        ProgressDialog.hide(getSupportFragmentManager());
                        Toast.makeText(context, object.getResponseMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setUserOnline(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
    }

    public Intent getGalleryPickerIntent() {

        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        return pictureIntent;
    }

    public void takeCameraIntent() {
        Uri uriImage;
        Intent pictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        try {
            File file = getImageFile();
            if (Build.VERSION.SDK_INT >= 24) {
                uriImage = FileProvider.getUriForFile(UserProfileActivity.this, BuildConfig.APPLICATION_ID.concat(".provider"), file);
            } else {
                uriImage = Uri.fromFile(file);
            }
            pictureIntent.putExtra("output", uriImage);
            startActivityForResult(pictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(UserProfileActivity.this, "Please select another image", Toast.LENGTH_SHORT).show();
        }
    }

    private void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = BitmapUtils.createTempImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mTempPhotoPath = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(this, BitmapUtils.FILE_PROVIDER_AUTHORITY, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
            }
        }
    }


    private String[] appPermissions = {
            WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE,
            CAMERA
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    private boolean checkAndRequestPermission() {
        List<String> listPermissionNeeded = new ArrayList<>();
        for (String perm : appPermissions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionNeeded.add(perm);
            }
        }

        //Ask for non-granted permissions
        if (!listPermissionNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(UserProfileActivity.this,
                    listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]),
                    PERMISSIONS_REQUEST_CODE);
            return false;
        }

        //App has all permissions. Proceed ahead
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            //Gather permission grant results
            for (int i = 0; i < grantResults.length; i++) {
                //Add only permission which are denied
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            //Check if all permissions are granted
            if (deniedCount == 0) {
                //Proceed ahead with the app
                openDialogChooser();
            }
            //Atleast one or all permissions are denied
            else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();

                    //permission is denied(this is the first time, when "never ask again" is not checked)
                    //so ask again explaining the usage of permission
                    //shouldShowRequestPermissionRationale will return true
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        //Show dialog of explanation
                        showDialog("", "This app needs Camera and Storage permission to work without and problems.",
                                "Yes, Grant Permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        if (checkAndRequestPermission()) {
                                            openDialogChooser();

                                        }
                                    }
                                },
                                "No, Exit app", (dialogInterface, i) -> {
                                    dialogInterface.dismiss();
                                    finish();
                                }, false);
                        break;
                    } else {
                        //permission is denied( and never ask again is checked)
                        //shouldShowRequestPermissionRationale will return false
                        //Ask user to go to settings and manually allow permissions
                        showDialog("", "You have denied some permissions. Allow all permissions at [Setting] > [Permissions]",
                                "Go to Settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        //Go to settings
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);

                                    }
                                }, "No, Exit app", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        finish();
                                    }
                                }, false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showDialog(String title, String msg, String positiveLabel,
                                  DialogInterface.OnClickListener positiveOnClick,
                                  String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
                                  boolean isCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);

        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }

}