package com.app.trustChat.ui.chat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Person;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.constants.ChatRoomStatus;
import com.app.trustChat.constants.MediaMessageStatus;
import com.app.trustChat.constants.MessageStatus;
import com.app.trustChat.constants.MessageType;
import com.app.trustChat.constants.UserStatus;
import com.app.trustChat.customViews.AttachmentOption;
import com.app.trustChat.customViews.AttachmentOptionsListener;
import com.app.trustChat.customViews.AudioRecordView;
import com.app.trustChat.model.ChatModel;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.Contacts;
import com.app.trustChat.model.GroupDetailModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.model.Locations;
import com.app.trustChat.model.Message;
import com.app.trustChat.model.NotificationModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.repository.MessageRepository;
import com.app.trustChat.services.ChatRoomService;
import com.app.trustChat.services.FileUploaderService;
import com.app.trustChat.services.MessagingService;
import com.app.trustChat.services.SinchService;
import com.app.trustChat.ui.AttachContactActivity;
import com.app.trustChat.ui.MediaPlayerActivity;
import com.app.trustChat.ui.groupProfileDetails.GroupDetailsActivity;
import com.app.trustChat.ui.singleChatProfileDetails.SingleProfileDetailActivity;
import com.app.trustChat.ui.viewImage.ViewImageActivity;
import com.app.trustChat.ui.voiceCall.BaseActivity;
import com.app.trustChat.ui.voiceCall.CallScreenAudioVideoActivity;
import com.app.trustChat.ui.voiceCall.OnClientStartListener;
import com.app.trustChat.utils.BitmapUtils;
import com.app.trustChat.utils.FileUtil;
import com.app.trustChat.utils.Util9;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.vanniktech.emoji.EmojiPopup;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class ChatActivity extends BaseActivity implements MessageCallback, AudioRecordView.RecordingListener, AttachmentOptionsListener,
        SinchService.StartFailedListener, OnClientStartListener, PopupMenu.OnMenuItemClickListener {
    private MediaRecorder mRecorder;
    private String roomTitle;
    private static final String TAG = "ChatActivity";
    private static final int PICK_FROM_ALBUM = 1;
    private static final int PICK_FROM_CAMERA = 1011;
    private static final int PICK_FROM_FILE = 2;
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1111;
    private static final int REQUEST_SEND_LOCATION = 5555;
    private static final int REQUEST_SEND_CONTACT = 6666;
    private static final int REQUEST_TAKE_AUDIO = 4444;
    private static final String FILE_BASE_URL = "http://trustapp.co.in/assets/storage/";
    private RecyclerViewAdapter mAdapter;
    private SimpleDateFormat dateFormatDay = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatHour = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
    private String roomID;
    private String myUid;
    private String userId;
    private UserModel currentUser;
    private List<Message> messageList = new ArrayList<>();
    private boolean isChatRoomExist = true;
    private UserModel opponentUser;
    private boolean isGroup;
    private ChatRoomModel chatRoomModel = new ChatRoomModel();
    private Context context;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseFirestore fireStore = null;
    private ListenerRegistration messageListener;
    private String lastMsg;
    private boolean isInChat;
    private boolean isOnline;
    private String toUid;
    private boolean isLongClickEnabled;
    private List<Message> selectedMessageList = new ArrayList<>();
    private boolean isPartOfGroup;
    private EmojiPopup emojIcon;
    // private boolean isAttachmentDialogVisible;
    private AudioRecordView audioRecordView;
    private long time;
    private View binding;
    private RecyclerView recyclerViewMessages;
    private ImageView ivUserStatus;
    private TextView tvLastSeen;
    private TextView tvTyping;
    private LinearLayout rlHeader;
    private RelativeLayout rlRoomSelection;
    private RelativeLayout rlNotInGroup;
    private Map<String, GroupUser> users = new HashMap<>();
    private String mTempPhotoPath;
    private Bitmap mResultsBitmap;
    private String notificationTitle;
    private String recordingFileUri;
    private File recordingFile;
    private boolean isRecordingCancelled;
    // private boolean recordingStarted;
    ImageView btnVoiceCall;
    ImageView btnCallVideo, btnCallMenu;
    private List<UserModel> appUsers = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //binding = DataBindingUtil.setContentView(this,R.layout.activity_chat);
        // this is to make your layout the root of audio record view, root layout supposed to be empty..
        setContentView(R.layout.activity_chatting);

        roomID = getIntent().getStringExtra("roomId");
        if (roomID == null) {
            onBackPressed();
        }
        isGroup = getIntent().getBooleanExtra("isGroup", false);
        fireStore = FirebaseFirestore.getInstance();
        if (roomID != null)
            getRoomDetailsFromFirebase();
        context = ChatActivity.this;

        audioRecordView = new AudioRecordView();
        audioRecordView.initView((FrameLayout) findViewById(R.id.layoutMain));
        // this is to provide the container layout to the audio record view..
        binding = audioRecordView.setContainerView(R.layout.activity_chat);
        audioRecordView.setRecordingListener(this);


        audioRecordView.setAttachmentOptions(AttachmentOption.getDefaultList(), this);

        audioRecordView.removeAttachmentOptionAnimation(false);

        recyclerViewMessages = binding.findViewById(R.id.recyclerViewMessages);
        linearLayoutManager = new LinearLayoutManager(this);
        mAdapter = new RecyclerViewAdapter(messageList);

        recyclerViewMessages.setLayoutManager(linearLayoutManager);
        recyclerViewMessages.setAdapter(mAdapter);
        recyclerViewMessages.getItemAnimator().setChangeDuration(0);

        setListener();

        audioRecordView.getEmojiView().setOnClickListener(v -> {
            audioRecordView.hideAttachmentOptionView();
            emojIcon.toggle();
        });

        audioRecordView.getCameraView().setOnClickListener(v -> {
            audioRecordView.hideAttachmentOptionView();
            launchCamera();
        });

        audioRecordView.getSendView().setOnClickListener(v -> {
            String msg = audioRecordView.getMessageView().getText().toString().trim();
            audioRecordView.getMessageView().setText("");
            sendGCM(msg.trim());
            sendMessage(msg.trim(), MessageType.MESSAGE_TYPE_TEXT,
                    null, "", "", "", "", null, "");
        });

        audioRecordView.getMessageView().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    setTypingStatus(UserStatus.TYPING_TO_NO_ONE);
                } else {
                    setTypingStatus(chatRoomModel.getRoomId());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        emojIcon = EmojiPopup.Builder.fromRootView(audioRecordView.getRootView()).setOnEmojiPopupShownListener(() -> {
            if (audioRecordView.getAttachmentView().getVisibility() == View.VISIBLE) {

            }
        }).build(audioRecordView.getMessageView());
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(ChatActivity.this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        userId = currentUser.getUserId();
        lastMsg = getIntent().getStringExtra("LAST_MSG");

        toUid = getIntent().getStringExtra("toUid");

        opponentUser = getIntent().getExtras().getParcelable("opponentUser");

        myUid = SharedPrefManager.getInstance(ChatActivity.this)
                .get(SharedPrefManager.Key.USER_ID, "");
        setView();
        if (!isGroup) {
            getCurrentUserNameForChatRoomFromFirebase();
            getAppUsers();
        }
    }

    private void getCurrentUserNameForChatRoomFromFirebase() {
        fireStore.collection("rooms").document(roomID).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Map<String, GroupUser> users = (Map<String, GroupUser>) task.getResult().get("members");
                if (users != null) {
                    for (Object groupUser : users.values()) {
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(groupUser);
                        GroupUser user = gson.fromJson(jsonElement, GroupUser.class);
                        if (myUid.equalsIgnoreCase(user.getId())) {
                            notificationTitle = user.getName();
                        }
                    }
                }
            }
        });
    }

    void getAppUsers() {
        String listGson = SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USERS_LIST, null);
        List<UserModel> list = new Gson().fromJson(listGson, new com.google.common.reflect.TypeToken<ArrayList<UserModel>>() {
        }.getType());
        for (UserModel user : list) {
            if (user.getEmail() != null)
                appUsers.add(user);
        }

    }

    public void setView() {
        rlHeader = binding.findViewById(R.id.rlHeader);
        btnVoiceCall = binding.findViewById(R.id.callAudio);
        btnCallVideo = binding.findViewById(R.id.callVideo);
        btnCallMenu = binding.findViewById(R.id.callMenu);
        rlRoomSelection = binding.findViewById(R.id.rlRoomSelection);
        rlNotInGroup = binding.findViewById(R.id.rlNotInGroup);
        ivUserStatus = binding.findViewById(R.id.ivUserStatus);
        ImageView ivBack = binding.findViewById(R.id.ivBack);
        ImageView roomImage = binding.findViewById(R.id.room_image);
        ImageView ivDeleteMessages = binding.findViewById(R.id.ivDeleteMessages);
        ImageView ivClearSelection = binding.findViewById(R.id.ivClearSelection);
        ImageView ivForward = binding.findViewById(R.id.ivForward);
        TextView tvContactName = binding.findViewById(R.id.tvContactName);
        tvLastSeen = binding.findViewById(R.id.tvLastSeen);
        tvTyping = binding.findViewById(R.id.tvTyping);
        RelativeLayout containerAvatar = binding.findViewById(R.id.container_avatar);
        TextView tvImageAvatar = binding.findViewById(R.id.tvImageAvatar);
        ivBack.setOnClickListener(v -> onBackPressed());

        if (isGroup) {
            btnVoiceCall.setVisibility(View.GONE);
            btnCallVideo.setVisibility(View.GONE);
        } else {
            btnVoiceCall.setVisibility(View.VISIBLE);
            btnCallVideo.setVisibility(View.VISIBLE);
        }

        roomTitle = getIntent().getStringExtra("roomTitle");
        String roomPhoto = getIntent().getStringExtra("roomImage");

        tvContactName.setText(roomTitle);

        if (roomPhoto == null || roomPhoto.isEmpty()) {
            roomImage.setVisibility(View.GONE);
            if (roomTitle != null)
                tvImageAvatar.setText(String.valueOf(roomTitle.charAt(0)));
            tvImageAvatar.setVisibility(View.VISIBLE);
        } else {
            Glide.with(ChatActivity.this).load(roomPhoto)
                    .placeholder(R.drawable.ic_user)
                    .into(roomImage);
            roomImage.setVisibility(View.VISIBLE);
            tvImageAvatar.setVisibility(View.GONE);
        }

        String subject = getIntent().getStringExtra("subject");


        if (!isGroup) {
            getUserRealtimeUpdates(toUid);
            getTypingStatus();
        } else {
            tvLastSeen.setText(subject);
        }

        findViewById(R.id.rlProfile).setOnClickListener(v -> {
            if (!isGroup) {
                Intent intent = new Intent(ChatActivity.this, SingleProfileDetailActivity.class);
                intent.putExtra("title", roomTitle);
                intent.putExtra("roomId", roomID);
                intent.putExtra("toUid", toUid);
                startActivity(intent);
            } else {
                Intent intent = new Intent(ChatActivity.this, GroupDetailsActivity.class);
                intent.putExtra("roomId", roomID);
                intent.putExtra("IS_MEMBER", isPartOfGroup);
                startActivity(intent);
            }
        });
        ivClearSelection.setOnClickListener(v -> clearMessageSelection());

        ivDeleteMessages.setOnClickListener(v -> new DeleteMessageAsync(selectedMessageList).execute());
        ivForward.setOnClickListener(v -> forwardMessages());
        btnVoiceCall.setOnClickListener(v -> createCall("audio"));
        btnCallVideo.setOnClickListener(v -> createCall("video"));
        if (isGroup) {
            btnVoiceCall.setVisibility(View.GONE);
            btnCallVideo.setVisibility(View.GONE);
        }

        btnCallMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isGroup) {
                    PopupMenu popup = new PopupMenu(ChatActivity.this, view);
                    popup.setOnMenuItemClickListener(ChatActivity.this);
                    popup.inflate(R.menu.popup_menu_group);
                    popup.show();
                } else {
                    PopupMenu popup = new PopupMenu(ChatActivity.this, view);
                    popup.setOnMenuItemClickListener(ChatActivity.this);
                    popup.inflate(R.menu.popup_menu);
                    popup.show();
                }


            }
        });
    }

    private void setListener() {
        audioRecordView.getEmojiView().setOnClickListener(v -> {
            audioRecordView.hideAttachmentOptionView();
            emojIcon.toggle();
        });

        audioRecordView.getCameraView().setOnClickListener(v -> audioRecordView.hideAttachmentOptionView());
    }

    @Override
    protected void onStart() {
        super.onStart();
        time = System.currentTimeMillis() / (1000);
    }

    private void attachContacts() {
        Intent intent = new Intent(this, AttachContactActivity.class);
        startActivityForResult(intent, REQUEST_SEND_CONTACT);
    }


    private void getUserRealtimeUpdates(String id) {
        fireStore.collection("users")
                .document(id).addSnapshotListener((documentSnapshot, e) -> {
            UserModel user = documentSnapshot.toObject(UserModel.class);

            if (user.getUserStatus().equalsIgnoreCase(UserStatus.ONLINE)) {
                ivUserStatus.setVisibility(View.VISIBLE);
                tvLastSeen.setText(UserStatus.ONLINE);
                isOnline = true;
            } else {
                isOnline = false;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
                Date today = new Date();
                String todayDate = dateFormat.format(today);
                Date d = new Date(user.getLastSeen());
                Date time = new Date(user.getLastSeen());
                String lastSeenDate = todayDate.equalsIgnoreCase(dateFormat.format(d)) ? "today" : dateFormat.format(d);
                tvLastSeen.setText("Last Seen: " + lastSeenDate + " at " + timeFormat.format(time));
                ivUserStatus.setVisibility(View.GONE);
            }
        });
    }

    private void getRoomDetails() {
        new GetRoomDetailsAsync().execute();
    }

    private void setTypingStatus(String typing) {
        String currentUserId = currentUser.getUserId();
        currentUser.setTypingTo(typing);
        currentUser.setUserStatus(UserStatus.ONLINE);
        FirebaseFirestore fireStore = FirebaseFirestore.getInstance();
        DocumentReference documentReference = fireStore.getInstance().collection("users")
                .document(currentUserId);
        documentReference.set(currentUser);
        MyApplication.saveCurrentUser(currentUser);

    }

    private void getTypingStatus() {
        FirebaseFirestore fireStore = FirebaseFirestore.getInstance();
        DocumentReference documentReference = fireStore.getInstance().collection("users")
                .document(toUid);
        documentReference.addSnapshotListener((documentSnapshot, e) -> {
            UserModel user = documentSnapshot.toObject(UserModel.class);
            if (user.getTypingTo() != null) {
                if (user.getTypingTo().equalsIgnoreCase(chatRoomModel.getRoomId())) {
                    tvTyping.setVisibility(View.VISIBLE);
                    tvLastSeen.setVisibility(View.GONE);
                } else {
                    tvTyping.setVisibility(View.GONE);
                    tvLastSeen.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApplication.getInstance().setUserOffline();
        stopListening();
    }

    void stopListening() {
        if (messageListener != null) {
            messageListener.remove();
            messageListener = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRoomDetails();
        new GetAllMessagesAsync().execute(roomID);
        new SendUnsentAsync().execute();
        startListening();
        MyApplication.getInstance().setUserOnline(true);
    }

    private Button.OnClickListener sendBtnClickListener = view -> {
        String msg = audioRecordView.getMessageView().getText().toString().trim();
        if (!msg.isEmpty()) {
            audioRecordView.getMessageView().setText("");
            sendMessage(msg.trim(), MessageType.MESSAGE_TYPE_TEXT,
                    null, "", "", "", "", null, "");
        }
    };


    private void sendMessage(final String msg, String msgType,
                             final ChatModel.FileInfo fileInfo, String mediaType, String mimeType,
                             String contact, String location, File file, String localUrl) {
        if (!isChatRoomExist) {
            createRoom(msg);
        } else {
            updateRoom(msg);
        }


        Message message = new Message();
        message.setMsg(msg);
        message.setMsgId(currentUser.getUserId() + "_" + System.currentTimeMillis());
        message.setRoomId(roomID);
        message.setUId(myUid);
        message.setName(currentUser.getUsernm());
        message.setCreatedAt(System.currentTimeMillis());
        message.setIsSeen(MessageStatus.UNSENT);
        message.setMsgType(msgType);
        message.setReadUsers("");
        if (!location.isEmpty())
            message.setLocation(location);
        if (!contact.isEmpty())
            message.setContacts(contact);
        if (fileInfo != null) {
            message.setFileName(fileInfo.fileName);
            message.setMediaUrl(fileInfo.fileUrl);
            message.setFileSize(fileInfo.fileSize);
            message.setMediaType(mediaType);
            message.setMimeType(mimeType);
            message.setMediaMessageStatus(MediaMessageStatus.IN_PROGRESS);
            FileUploaderService.uploadFile(ChatActivity.this, file, message.getMsgId(), roomID, currentUser.getMobile(), fileInfo.fileName);
        }
        new SaveMessageAsync().execute(message);
        messageList.add(message);
        new MessagingService().sendMessageToFirebase(ChatActivity.this, message, this);
        notifyItemInserted();
    }


    private void sendForwardedMessage(Message message) {
        updateRoom(message.getMsg());
        message.setMsgId(currentUser.getUserId() + "_" + System.currentTimeMillis());
        message.setRoomId(roomID);
        message.setUId(myUid);
        message.setName(currentUser.getUsernm());
        message.setMediaMessageStatus(MediaMessageStatus.UPLOADED);
        message.setCreatedAt(System.currentTimeMillis());
        message.setIsSeen(MessageStatus.UNSENT);
        new SaveMessageAsync().execute(message);
        messageList.add(message);
        mAdapter.notifyDataSetChanged();
        new MessagingService().sendMessageToFirebase(ChatActivity.this, message, this);
    }

    void notifyItemInserted() {
        mAdapter.notifyItemInserted(mAdapter.getItemCount());
        recyclerViewMessages.scrollToPosition(messageList.size() - 1);
    }

    void notifyItemUpdated(Message msg) {
        int pos = 0;
        for (int i = 0; i < messageList.size(); i++) {
            if (messageList.get(i).getMsgId().equalsIgnoreCase(msg.getMsgId())) {
                messageList.get(i).setMediaMessageStatus(MediaMessageStatus.UPLOADED);
                new UpdateMessageAsync(msg).execute();
                pos = i;
                break;
            }
        }
        try {
            mAdapter.notifyItemChanged(pos + 1);
        } catch (Exception e) {
            mAdapter.notifyItemChanged(pos);
        }
    }

    private void createRoom(String msg) {
        chatRoomModel.setRoomId(roomID);
        chatRoomModel.setGroup(isGroup);
        chatRoomModel.setUserId(currentUser.getUserId());
        chatRoomModel.setTitle(opponentUser.getUsernm());
        chatRoomModel.setOpponentId(opponentUser.getUserId());
        chatRoomModel.setLastMsg(msg);
        chatRoomModel.setSubject(currentUser.getUsernm() + " created this group");
        chatRoomModel.setUnreadCount(0);
        chatRoomModel.setUpdatedAt(System.currentTimeMillis());
        chatRoomModel.setCreatedAt(System.currentTimeMillis());
        chatRoomModel.setStatus(ChatRoomStatus.OFFLINE);
        GroupUser currUser = new GroupUser(currentUser.getUsernm(), currentUser.getMobile(), true,
                currentUser.getMood(), 0, currentUser.getUserphoto(),
                currentUser.getUserId(), currentUser.getToken(), false);
        GroupUser oppUser = new GroupUser(opponentUser.getUsernm(), opponentUser.getMobile(), false, opponentUser.getMood(),
                1, opponentUser.getUserphoto(), opponentUser.getUserId(), opponentUser.getToken(), false);
        Map<String, GroupUser> users = new HashMap<>();
        users.put(currUser.getId(), currUser);
        users.put(oppUser.getId(), oppUser);
        chatRoomModel.setMembers(new Gson().toJson(users));
        new CreateRoomAsync(chatRoomModel).execute();
        ChatRoomService.createGroupOnline(ChatActivity.this, chatRoomModel);
    }

    private void updateRoom(String msg) {
        if (isChatRoomExist) {
            chatRoomModel.setUpdatedAt(System.currentTimeMillis());
            chatRoomModel.setLastMsg(msg);
            String members = chatRoomModel.getMembers();
            Map<String, GroupUser> users = new Gson().fromJson(
                    members, new TypeToken<HashMap<String, Object>>() {
                    }.getType());
            Map<String, GroupUser> groupMembers = new HashMap<>();
            for (Object value : users.values()) {
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(value);
                GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
                if (myUid.equals(groupUser.getId())) {
                    groupUser.setMessageCount(0);
                } else {
                    groupUser.setMessageCount(groupUser.getMessageCount() + 1);
                }
                groupMembers.put(groupUser.getId(), groupUser);
            }
            chatRoomModel.setMembers(new Gson().toJson(groupMembers));
            new UpdateRoomAsync(chatRoomModel).execute();
            ChatRoomService.updateGroupOnline(ChatActivity.this, chatRoomModel, true, null);
            recyclerViewMessages.scrollToPosition(mAdapter.getItemCount());
        } else {
            createRoom(msg);
        }
    }

    private void setUnreadCountToZero() {
        String members = chatRoomModel.getMembers();
        Map<String, GroupUser> users = new Gson().fromJson(
                members, new TypeToken<HashMap<String, Object>>() {
                }.getType());
        Map<String, GroupUser> groupMembers = new HashMap<>();
        for (Object value : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(value);
            GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
            if (myUid.equals(groupUser.getId()))
                groupUser.setMessageCount(0);
            groupMembers.put(groupUser.getId(), groupUser);
        }
        chatRoomModel.setMembers(new Gson().toJson(groupMembers));
        new UpdateRoomAsync(chatRoomModel).execute();
        ChatRoomService.updateGroupOnline(ChatActivity.this, chatRoomModel, false, null);
        recyclerViewMessages.scrollToPosition(mAdapter.getItemCount());
    }


    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_SEND_CONTACT) {
            List<UserModel> contactList = new ArrayList<>();
            contactList.addAll(Objects.requireNonNull(Objects.requireNonNull(data.getExtras()).getParcelableArrayList("CONTACT_LIST")));
            for (UserModel userModel : contactList) {
                Contacts contacts = new Contacts(userModel.getUsernm(), userModel.getMobile());
                String contact = new Gson().toJson(contacts);
                sendMessage("Contact", MessageType.MESSAGE_TYPE_CONTACT, null, "", "", contact, "", null, "");
            }
            sendGCM("Contact");
            return;
        }
        if ((requestCode == REQUEST_SEND_LOCATION) && (resultCode == RESULT_OK)) {
          /*  Place place = PingPlacePicker.getPlace(data);
            if (place != null) {
                String location = new Gson().toJson(new Locations(place.getLatLng().latitude, place.getLatLng().longitude, place.getName()));
                ChatModel.FileInfo fileInfo = new ChatModel.FileInfo();
                fileInfo.fileUrl = getStaticMapLink(place.getLatLng().latitude, place.getLatLng().longitude);
                sendMessage("Location", MessageType.MESSAGE_TYPE_LOCATION, fileInfo, "location", "", "", location, null, "");
                sendGCM("Location");
            }
            return;*/
        }


        File file = null;
        ChatModel.FileInfo fileInfo = null;
        String filename = Util9.getUniqueValue(userId);
        String mimeType = null;
        Uri fileUri = null;

        if (requestCode == PICK_FROM_CAMERA) {
            mResultsBitmap = BitmapUtils.resamplePic(this, mTempPhotoPath);
            fileUri = Uri.fromFile(new File(BitmapUtils.saveImage(this, mResultsBitmap)));

            fileInfo = getFileDetailFromUri(this, fileUri);
            try {
                file = FileUtil.from(this, fileUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (fileUri != null)
                mimeType = FileUtil.getMimeType(fileUri);
        }
        if (requestCode == PICK_FROM_ALBUM || requestCode == PICK_FROM_FILE ||
                requestCode == REQUEST_TAKE_GALLERY_VIDEO || requestCode == REQUEST_TAKE_AUDIO) {
            fileUri = data.getData();
            fileInfo = getFileDetailFromUri(this, fileUri);
            try {
                file = FileUtil.from(this, fileUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (fileUri != null)
                mimeType = FileUtil.getMimeType(fileUri);
        }

        String getSubFolder = FileUtil.getSubFolder(mimeType);
        assert file != null;
        String fileExt = file.getPath().substring(file.getPath().lastIndexOf("."));

        assert fileInfo != null;
        fileInfo.fileUrl = FILE_BASE_URL + getSubFolder + filename + fileExt;
        fileInfo.fileName = filename;
        if (requestCode == PICK_FROM_ALBUM || requestCode == PICK_FROM_CAMERA) {
            sendMessage("Image", MessageType.MESSAGE_TYPE_IMAGE, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
            sendGCM("Image");
        } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
            sendMessage("Video", MessageType.MESSAGE_TYPE_VIDEO, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
            sendGCM("Video");
        } else if (requestCode == REQUEST_TAKE_AUDIO) {
            sendMessage("Audio", MessageType.MESSAGE_TYPE_AUDIO, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
            sendGCM("Audio");
        } else {
            sendMessage("Attachment", MessageType.MESSAGE_TYPE_FILE, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
            sendGCM("Attachment");
        }
    }

    // get file name and size from Uri
    private static ChatModel.FileInfo getFileDetailFromUri(final Context context, final Uri uri) {
        if (uri == null) {
            return null;
        }

        ChatModel.FileInfo fileDetail = new ChatModel.FileInfo();
        // File Scheme.
        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            File file = new File(String.valueOf(uri));
            fileDetail.fileName = file.getName();
            fileDetail.fileSize = Util9.size2String(file.length());
        } else if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            Cursor returnCursor =
                    context.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null && returnCursor.moveToFirst()) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                fileDetail.fileName = returnCursor.getString(nameIndex);
                fileDetail.fileSize = Util9.size2String(returnCursor.getLong(sizeIndex));
                returnCursor.close();
            }
        }

        return fileDetail;
    }

    @Override
    public void onSuccess(Message message) {
        int pos = 0;
        if (message != null) {
            for (int i = 0; i < messageList.size(); i++) {
                if (messageList.get(i).getMsgId().equalsIgnoreCase(message.getMsgId())) {
                    messageList.get(i).setIsSeen(message.getIsSeen());
                    pos = i;
                    break;
                }
            }
            mAdapter.notifyItemChanged(pos);
        }
    }


    @Override
    public void onClick(AttachmentOption attachmentOption) {
        audioRecordView.hideAttachmentOptionView();
        Intent intent = new Intent();
        switch (attachmentOption.getId()) {
            case AttachmentOption.DOCUMENT_ID:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("*/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_FILE);
                break;
            case AttachmentOption.CAMERA_ID:
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);
                break;
            case AttachmentOption.GALLERY_ID:
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_FROM_ALBUM);
                break;
            case AttachmentOption.AUDIO_ID:
                Intent intent_upload = new Intent();
                intent_upload.setType("audio/*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload, REQUEST_TAKE_AUDIO);
                break;
            case AttachmentOption.LOCATION_ID:
                openPlacePicker();
                break;
            case AttachmentOption.CONTACT_ID:
                attachContacts();
                break;
        }
    }

    @Override
    public void onRecordingStarted() {
        startRecording();
    }

    @Override
    public void onRecordingLocked() {
    }

    @Override
    public void onRecordingCompleted() {
        int recordTime = (int) ((System.currentTimeMillis() / (1000)) - time);
        if (recordTime > 5)
            stopRecording();
        else {
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            } catch (Exception e) {
                Log.e("TAG", e.getLocalizedMessage());
            }
        }

    }

    @Override
    public void onRecordingCanceled() {
        isRecordingCancelled = true;
    }


    class RecyclerViewAdapter extends RecyclerView.Adapter<MessageViewHolder> {
        private List<Message> messageList;

        RecyclerViewAdapter(List<Message> messageList) {
            this.messageList = messageList;
        }

        @Override
        public int getItemViewType(int position) {
            Message message = new Message();
            if (messageList != null)
                message = messageList.get(position);
            if (myUid.equals(message.getUId())) {
                switch (message.getMsgType()) {
                    case "1":
                        return R.layout.item_chatimage_right;
                    case "2":
                        return R.layout.item_chatfile_right;
                    case "3":
                        return R.layout.item_chatvideo_right;
                    case "4":
                        return R.layout.item_chatlocation_right;
                    case "5":
                        return R.layout.item_chat_contact_right;
                    case "6":
                        return R.layout.item_audio_right;
                    default:
                        return R.layout.item_chatmsg_right;
                }
            } else {
                switch (message.getMsgType()) {
                    case "1":
                        return R.layout.item_chatimage_left;
                    case "2":
                        return R.layout.item_chatfile_left;
                    case "3":
                        return R.layout.item_chatvideo_left;
                    case "4":
                        return R.layout.item_chatlocation_left;
                    case "5":
                        return R.layout.item_chat_contact_left;
                    case "6":
                        return R.layout.item_audio_left;
                    default:
                        return R.layout.item_chatmsg_left;
                }
            }
        }

        @NonNull
        @Override
        public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
            return new MessageViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int position) {
            messageViewHolder.itemView.setLongClickable(true);
            final Message message = messageList.get(position);
            if (!message.getUId().equalsIgnoreCase(myUid)) {
                if (chatRoomModel.isGroup()) {
                    messageViewHolder.msg_name.setText(message.getName());
                    messageViewHolder.msg_name.setVisibility(View.VISIBLE);
                } else {
                    messageViewHolder.msg_name.setVisibility(View.GONE);
                }
            }

            if ("1".equalsIgnoreCase(message.getMsgType()) || "2".equalsIgnoreCase(message.getMsgType())
                    || "3".equalsIgnoreCase(message.getMsgType())
                    || "6".equalsIgnoreCase(message.getMsgType())) {
                if (message.getMediaMessageStatus() != null) {
                    switch (message.getMediaMessageStatus()) {
                        case MediaMessageStatus.FAILED:
                        case MediaMessageStatus.IN_PROGRESS:
                            messageViewHolder.mediaProgress.setVisibility(View.VISIBLE);
                            break;
                        case MediaMessageStatus.UPLOADED:
                            messageViewHolder.mediaProgress.setVisibility(View.GONE);
                            break;
                    }
                }
            }

            if ("0".equals(message.getMsgType())) {                                      // text message
                messageViewHolder.msg_item.setText(message.getMsg());
            } else if ("1".equalsIgnoreCase(message.getMsgType())) {                                                                // image transfer
                messageViewHolder.realName = message.getMsg();
                loadThumb(message.getMediaUrl(), messageViewHolder.img_item);
                messageViewHolder.img_item.setOnClickListener(v -> {
                    Intent imageIntent = new Intent(ChatActivity.this, ViewImageActivity.class);
                    imageIntent.putExtra("imageUrl", message.getMediaUrl());
                    startActivity(imageIntent);
                });
            } else if ("2".equals(message.getMsgType())) {                                      // file transfer
                messageViewHolder.filename = message.getFileName();
                messageViewHolder.realName = message.getMsg();
                messageViewHolder.img_item
                        .setImageDrawable(FileUtil.getFileDrawable(ChatActivity.this, message.getMediaType()));
                messageViewHolder.tvFileName.setText(message.getFileName());
                messageViewHolder.msgBox.setOnClickListener(v -> {
                    Intent doc = new Intent(Intent.ACTION_VIEW);
                    doc.setData(Uri.parse(message.getMediaUrl()));
                    startActivity(doc);
                });
            } else if ("3".equalsIgnoreCase(message.getMsgType())) {
                loadThumb(message.getMediaUrl(), messageViewHolder.img_item);
            } else if ("4".equalsIgnoreCase(message.getMsgType())) {
                loadThumb(message.getMediaUrl(), messageViewHolder.img_item);
                Locations locations = new Gson().fromJson(message.getLocation(), Locations.class);
                messageViewHolder.img_item.setOnClickListener(v -> {
                    if (locations != null)
                        showDirections(locations.getLat(), locations.getLng());
                });
            } else if ("5".equals(message.getMsgType())) {
                Contacts contacts = new Gson().fromJson(message.getContacts(), Contacts.class);
                messageViewHolder.tvUserName.setText(contacts.getName());
                messageViewHolder.tvMobile.setText(contacts.getMobile());
                messageViewHolder.tvImageAvatar.setText(String.valueOf(contacts.getName().charAt(0)));
                messageViewHolder.tvSave.setOnClickListener(v -> addAsContactConfirmed(contacts));
            } else if ("6".equalsIgnoreCase(message.getMsgType())) {
                messageViewHolder.seekbar.setEnabled(false);
                messageViewHolder.ivPlay.setOnClickListener(v -> {
                    /*Intent playVideo = new Intent(Intent.ACTION_VIEW);
                    playVideo.setDataAndType(Uri.parse(message.getMediaUrl()), "audio/*");
                    startActivity(playVideo);*/
                    Intent intent = new Intent(ChatActivity.this, MediaPlayerActivity.class);
                    intent.putExtra("MEDIA_URL", message.getMediaUrl());
                    startActivity(intent);
                });

            }


            if ("3".equalsIgnoreCase(message.getMsgType())) {
                messageViewHolder.img_item.setOnClickListener(v -> {
                    Intent playVideo = new Intent(Intent.ACTION_VIEW);
                    playVideo.setDataAndType(Uri.parse(message.getMediaUrl()), "video/mp4");
                    startActivity(playVideo);
                });
            }


            if (message.getIsSeen().equalsIgnoreCase(MessageStatus.UNSENT)) {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_unsent));
            } else if (message.getIsSeen().equalsIgnoreCase(MessageStatus.SENT)) {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_delivered));
            } else if (message.getIsSeen().equalsIgnoreCase(MessageStatus.DELIVERED)) {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_delivered));
            } else {
                messageViewHolder.ivMsgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_read));
            }

            messageViewHolder.divider.setVisibility(View.INVISIBLE);
            messageViewHolder.divider.getLayoutParams().height = 0;
            messageViewHolder.timestamp.setText("");
            if (message.getCreatedAt() == null) {
                return;
            }

            String day = dateFormatDay.format(message.getCreatedAt());
            String timestamp = dateFormatHour.format(message.getCreatedAt());
            messageViewHolder.timestamp.setText(timestamp);

            if (position == 0) {
                messageViewHolder.divider_date.setText(day);
                messageViewHolder.divider.setVisibility(View.VISIBLE);
                messageViewHolder.divider.getLayoutParams().height = 60;
            } else {
                Message beforeMsg = messageList.get(position - 1);
                String beforeDay = dateFormatDay.format(beforeMsg.getCreatedAt());

                if (!day.equals(beforeDay)) {
                    messageViewHolder.divider_date.setText(day);
                    messageViewHolder.divider.setVisibility(View.VISIBLE);
                    messageViewHolder.divider.getLayoutParams().height = 60;
                }
            }

            if (message.isSelected()) {
                messageViewHolder.itemView.setBackgroundColor(Color.parseColor("#2CFF5722"));
            } else {
                messageViewHolder.itemView.setBackgroundColor(Color.parseColor("#00FFFFFF"));

            }
            messageViewHolder.itemView.setOnClickListener(v -> {
                if (isLongClickEnabled) {
                    messageList.get(position).setSelected(!messageList.get(position).isSelected());
                    updateSelectedMessageList(messageList.get(position));
                    notifyItemChanged(position);
                }
            });
            if (messageViewHolder.ivForward != null) {
                messageViewHolder.ivForward.setOnClickListener(v -> {
                    isLongClickEnabled = true;
                    messageList.get(position).setSelected(!messageList.get(position).isSelected());
                    updateSelectedMessageList(messageList.get(position));
                    notifyItemChanged(position);
                });

            }
            messageViewHolder.itemView.setOnLongClickListener(v -> {
                isLongClickEnabled = true;
                messageList.get(position).setSelected(!messageList.get(position).isSelected());
                updateSelectedMessageList(messageList.get(position));
                notifyItemChanged(position);
                return true;
            });

        }

        @Override
        public int getItemCount() {
            return messageList != null ? messageList.size() : 0;
        }

        public void clearChat() {
            messageList.clear();
            notifyDataSetChanged();
        }
    }

    private static class MessageViewHolder extends RecyclerView.ViewHolder {
        ProgressBar mediaProgress;
        ImageView user_photo;
        RelativeLayout msgBox;
        TextView msg_item;
        TextView tvFileName;
        VideoView videoView;
        ImageView img_item;
        ImageView ivMsgSeen;
        TextView msg_name;
        TextView timestamp;
        LinearLayout divider;
        TextView divider_date;
        ImageView ivPlay;
        TextView tvImageAvatar;
        TextView tvUserName;
        TextView tvMobile;
        TextView tvSave;
        String filename;
        String realName;
        SeekBar seekbar;
        ImageView ivForward;

        MessageViewHolder(View view) {
            super(view);
            mediaProgress = view.findViewById(R.id.mediaProgress);
            ivForward = view.findViewById(R.id.ivForward);
            msgBox = view.findViewById(R.id.msgBox);
            ivPlay = view.findViewById(R.id.ivPlay);
            user_photo = view.findViewById(R.id.user_photo);
            seekbar = view.findViewById(R.id.seekbar);
            msg_item = view.findViewById(R.id.msg_item);
            videoView = view.findViewById(R.id.videoView);
            img_item = view.findViewById(R.id.img_item);
            ivMsgSeen = view.findViewById(R.id.ivMsgSeen);
            timestamp = view.findViewById(R.id.timestamp);
            tvFileName = view.findViewById(R.id.tvFileName);
            msg_name = view.findViewById(R.id.msg_name);
            divider = view.findViewById(R.id.divider);
            divider_date = view.findViewById(R.id.divider_date);
            tvImageAvatar = view.findViewById(R.id.tvImageAvatar);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvMobile = view.findViewById(R.id.tvMobile);
            tvSave = view.findViewById(R.id.tvSave);
        }
    }

    static class SaveMessageAsync extends AsyncTask<Message, Void, Message> {

        @Override
        protected Message doInBackground(Message... messages) {
            MessageRepository repository = new MessageRepository(MyApplication.getInstance());
            repository.insertMessage(messages[0]);
            return messages[0];
        }


        @Override
        protected void onPostExecute(Message message) {
            super.onPostExecute(message);
        }

    }

    @SuppressLint("StaticFieldLeak")
    class UpdateMessageAsync extends AsyncTask<Void, Void, Integer> {
        Message message;

        public UpdateMessageAsync(Message message) {
            this.message = message;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            MessageRepository repository = new MessageRepository(MyApplication.getInstance());
            return repository.updateMessage(message);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            onSuccess(message);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class GetAllMessagesAsync extends AsyncTask<String, Void, List<Message>> {
        @Override
        protected List<Message> doInBackground(String... strings) {
            String roomId = strings[0];
            MessageRepository messageRepository = new MessageRepository(MyApplication.getInstance());
            return messageRepository.getMessages(roomId);
        }

        @Override
        protected void onPostExecute(List<Message> messages) {
            super.onPostExecute(messages);
            messageList.clear();
            messageList.addAll(messages);
            mAdapter.notifyDataSetChanged();
            recyclerViewMessages.scrollToPosition(messageList.size() - 1);
            if (HomeActivity.forwardedMessageList.size() > 0) {
                List<Message> list = new ArrayList<>();
                list.addAll(HomeActivity.forwardedMessageList);
                HomeActivity.forwardedMessageList.clear();
                for (Message m : list) {
                    m.setMediaMessageStatus(MediaMessageStatus.UPLOADED);
                    sendForwardedMessage(m);
                }
            }
        }
    }

    class GetRoomDetailsAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomID);
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel chatRoom) {
            if (chatRoom != null) {
                chatRoomModel = chatRoom;
                isPartOfGroup = false;
                String members = chatRoomModel.getMembers();
                Map<String, GroupUser> users = new Gson().fromJson(
                        members, new TypeToken<HashMap<String, Object>>() {
                        }.getType());
                for (Object value : users.values()) {
                    Gson gson = new Gson();
                    JsonElement jsonElement = gson.toJsonTree(value);
                    GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
                    if (myUid.equals(groupUser.getId()) && !groupUser.isBlocked()) {
                        groupUser.setMessageCount(0);
                        isPartOfGroup = true;
                        break;
                    }
                    if (groupUser.getId().equalsIgnoreCase(myUid) && groupUser.isBlocked()) {
                        isPartOfGroup = false;
                        break;
                    }
                }
                if (!isPartOfGroup) {
                    updateUi(true);
                } else {
                    updateUi(false);
                }
                setUnreadCountToZero();

            } else {
                isChatRoomExist = false;
            }

            if (isChatRoomExist && !chatRoomModel.isGroup()) {
                updateOpponent(chatRoomModel);
            }
        }
    }

    private void updateUi(boolean isBlock) {
        if (isBlock) {
            ((TextView) findViewById(R.id.tvBlock))
                    .setText(String.format(getString(R.string.blocked_message), roomTitle, roomTitle));
            rlNotInGroup.setVisibility(View.VISIBLE);
            audioRecordView.getMessageView().setEnabled(false);
            audioRecordView.getAttachmentView().setClickable(false);
            audioRecordView.getCameraView().setClickable(false);
            audioRecordView.getEmojiView().setClickable(false);
            audioRecordView.setRecordingListener(null);
            audioRecordView.getImageViewMic().setClickable(false);
            btnCallVideo.setVisibility(View.GONE);
            btnVoiceCall.setVisibility(View.GONE);

        } else {
            rlNotInGroup.setVisibility(View.GONE);
            audioRecordView.getMessageView().setEnabled(true);
            audioRecordView.getAttachmentView().setClickable(true);
            audioRecordView.getCameraView().setClickable(true);
            audioRecordView.getEmojiView().setClickable(true);
            audioRecordView.setRecordingListener(this);
            audioRecordView.getImageViewMic().setClickable(true);
            if (!isGroup) {
                btnCallVideo.setVisibility(View.VISIBLE);
                btnVoiceCall.setVisibility(View.VISIBLE);
            }
        }
    }

    private void getRoomDetailsFromFirebase() {
        fireStore.collection("rooms")
                .document(roomID).addSnapshotListener((documentSnapshot, e) -> {
            GroupDetailModel object = documentSnapshot.toObject(GroupDetailModel.class);
            users = (Map<String, GroupUser>) documentSnapshot.get("members");
            if (users != null)
                updateBlock();
        });
    }

    private void updateBlock() {
        for (Object user : users.values()) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(user);
            GroupUser gUser = gson.fromJson(jsonElement, GroupUser.class);
            if (gUser.getId().equalsIgnoreCase(currentUser.getUserId()) && gUser.isBlocked()) {
                isPartOfGroup = false;
            } else {
                isPartOfGroup = true;
            }
        }
        if (isPartOfGroup)
            enableUi();
        else
            updateUi(true);

    }

    private void enableUi() {
        rlNotInGroup.setVisibility(View.GONE);
        audioRecordView.getMessageView().setEnabled(true);
        audioRecordView.getAttachmentView().setClickable(true);
        audioRecordView.getCameraView().setClickable(true);
        audioRecordView.getEmojiView().setClickable(true);
        audioRecordView.setRecordingListener(this);
        audioRecordView.getImageViewMic().setClickable(true);
    }

    @SuppressLint("StaticFieldLeak")
    class CreateRoomAsync extends AsyncTask<Void, Void, Long> {

        private ChatRoomModel chatRoomModel;

        public CreateRoomAsync(ChatRoomModel chatRoomModel) {
            this.chatRoomModel = chatRoomModel;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            return repository.createChatRoom(chatRoomModel);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            isChatRoomExist = true;
        }
    }

    static class UpdateRoomAsync extends AsyncTask<Void, Void, Integer> {
        ChatRoomModel chatRoomModel;

        public UpdateRoomAsync(ChatRoomModel chatRoomModel) {
            this.chatRoomModel = chatRoomModel;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            return repository.update(chatRoomModel);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SendUnsentAsync extends AsyncTask<Void, Void, List<Message>> {
        @Override
        protected List<Message> doInBackground(Void... voids) {
            MessageRepository messageRepository = new MessageRepository(MyApplication.getInstance());
            return messageRepository.getAllMessages(roomID, MessageStatus.UNSENT);
        }

        @Override
        protected void onPostExecute(List<Message> messages) {
            super.onPostExecute(messages);
            if (messages.size() > 0)
                sendBulkMessages(messages);
        }

    }

    private class DeleteMessageAsync extends AsyncTask<Void, Void, Void> {
        List<Message> messages;

        public DeleteMessageAsync(List<Message> messages) {
            this.messages = messages;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            MessageRepository repository = new MessageRepository(MyApplication.getInstance());
            for (Message msg : messages) {
                repository.deleteMessage(msg.getMsgId());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new GetAllMessagesAsync().execute(roomID);
            clearMessageSelection();
        }

    }

    private void sendBulkMessages(List<Message> messages) {
        new MessagingService().sendBulkMessages(context, messages, this);
    }


    private void startListening() {
        CollectionReference roomRef = fireStore.collection("rooms").document(roomID).collection("messages");
        messageListener = roomRef.orderBy("createdAt").addSnapshotListener((documentSnapshots, e) -> {
            if (e != null) {
                return;
            }

            Message message;
            for (DocumentChange change : documentSnapshots.getDocumentChanges()) {
                switch (change.getType()) {
                    case ADDED:
                        message = change.getDocument().toObject(Message.class);
                        if (!message.getUId().equalsIgnoreCase(myUid) && message.getIsSeen().equalsIgnoreCase(MessageStatus.SENT)) {
                            message.setIsSeen(MessageStatus.READ);
                            new SaveMessageAsync().execute(message);
                            change.getDocument().getReference().update("isSeen", MessageStatus.READ);
                            messageList.add(message);
                            chatRoomModel.setLastMsg(message.getMsg());
                            chatRoomModel.setUpdatedAt(System.currentTimeMillis());
                            new UpdateRoomAsync(chatRoomModel).execute();
                            notifyItemInserted();
                        }

                        new UpdateMessageAsync(message).execute();
                        break;
                    case MODIFIED:
                        message = change.getDocument().toObject(Message.class);
                        if (!message.getUId().equalsIgnoreCase(myUid) && message.getIsSeen().equalsIgnoreCase(MessageStatus.READ)) {
                            change.getDocument().getReference().update("isSeen", MessageStatus.READ);
                            notifyItemInserted();
                        }
                        if (message.getMediaMessageStatus() != null && message.getMediaMessageStatus().equalsIgnoreCase(MediaMessageStatus.UPLOADED)) {
                            notifyItemUpdated(message);
                        }
                        ChatRoomService.updateGroupOnline(ChatActivity.this, chatRoomModel, true, this);
                        new UpdateMessageAsync(message).execute();
                        break;
                    case REMOVED:
                        message = change.getDocument().toObject(Message.class);
                        if (messageList != null)
                            try {
                                messageList.remove(message);
                            } catch (Exception es) {
                                es.printStackTrace();
                            }
                        break;
                }
            }

        });
    }


    private void updateSelectedMessageList(Message message) {
        if (selectedMessageList.contains(message)) {
            selectedMessageList.remove(message);
        } else {
            selectedMessageList.add(message);
        }
        if (selectedMessageList.size() > 0) {
            isLongClickEnabled = true;
            rlHeader.setVisibility(View.GONE);
            rlRoomSelection.setVisibility(View.VISIBLE);
        } else {
            rlHeader.setVisibility(View.VISIBLE);
            rlRoomSelection.setVisibility(View.GONE);
            isLongClickEnabled = false;
        }
    }

    private void clearMessageSelection() {
        for (int i = 0; i < messageList.size(); i++) {
            messageList.get(i).setSelected(false);
        }
        rlHeader.setVisibility(View.VISIBLE);
        rlRoomSelection.setVisibility(View.GONE);
        isLongClickEnabled = false;
        mAdapter.notifyDataSetChanged();
    }

    private void forwardMessages() {
        Intent intent = new Intent(ChatActivity.this, HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("FORWARD_MESSAGES", (ArrayList<? extends Parcelable>) selectedMessageList);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (!isLongClickEnabled) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            clearMessageSelection();
        }
    }


    private void openPlacePicker() {
      /*  try {
            PingPlacePicker.IntentBuilder builder = new PingPlacePicker.IntentBuilder();
            builder.setAndroidApiKey("AIzaSyAI2n7kQ2n-hN-cWXF000TDtjaDoOaEpGo")
                    .setMapsApiKey("AIzaSyAI2n7kQ2n-hN-cWXF000TDtjaDoOaEpGo");
            Intent placeIntent = builder.build(ChatActivity.this);
            startActivityForResult(placeIntent, REQUEST_SEND_LOCATION);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }*/
    }

    private String getStaticMapLink(double lat, double lng) {

        String url = "http://maps.google.com/maps/api/staticmap?center=" + lat + "," + lng
                + "&zoom=15&size=200x200&sensor=false"
                + "&markers=color:red%7Clabel:C%7C" + lat + "," + lng
                + "&key=AIzaSyAI2n7kQ2n-hN-cWXF000TDtjaDoOaEpGo";

        return url;
    }

    void sendGCM(String msg) {
        Gson gson = new Gson();
        NotificationModel.Body body = new NotificationModel.Body();
        body.roomId = roomID;
        body.message = msg;
        body.activityName = "CHAT";
        body.roomPhoto = chatRoomModel.getPhoto();
        body.toUid = currentUser.getUserId();
        body.isGroup = isGroup;
        if (!isGroup)
            body.senderName = notificationTitle != null ? notificationTitle : currentUser.getUsernm();
        else
            body.senderName = roomTitle;
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.notification.title = notificationTitle != null ? notificationTitle : currentUser.getUsernm();
        notificationModel.notification.body = msg;

        notificationModel.data.body = body;
        String members = chatRoomModel.getMembers();
        if (members != null) {
            Map<String, GroupUser> userList = new Gson().fromJson(
                    members, new TypeToken<HashMap<String, Object>>() {
                    }.getType());

            for (Object value : userList.values()) {
                JsonElement jsonElement = gson.toJsonTree(value);
                GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
                if (myUid.equals(groupUser.getId())) continue;
                notificationModel.to = groupUser.getToken();
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf8"), gson.toJson(notificationModel));
                Request request = new Request.Builder()
                        .header("Content-Type", "application/json")
                        .addHeader("Authorization", "key=AAAAQesl-yQ:APA91bGgpHoq3XzM9RAakMzQXcAVIQRtrmN_108xAhF3-jlwUTtbL4whAgdjMi6tDoy3ffM1wdM4FP5Up5jYA2TeZ83ZRu6cgdzFcL7A1pqHZqbyF1yjPRwN1FJVn7oScG8Xs3_0iDPK")
                        .url("https://fcm.googleapis.com/fcm/send")
                        .post(requestBody)
                        .build();
                OkHttpClient okHttpClient = new OkHttpClient();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        Log.e(TAG, response.message());
                    }
                });
            }
        }
    }


    /**
     * Open the add-contact screen with pre-filled info
     *
     * @param person {@link Person} to add to contacts list
     */
    public void addAsContactConfirmed(Contacts person) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, person.getName());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, person.getMobile());
        startActivity(intent);

    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void loadThumb(String url, ImageView view) {
        Glide.with(ChatActivity.this)
                .load(url)
                .apply(new RequestOptions().override(1000, 1000))
                .into(view);
    }

    private void showDirections(double lat, double lng) {
        String s = "geo:" + lat + "," + lng;
        Uri gmmIntentUri = Uri.parse(s);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private void createCall(String callType) {
        startCallService(callType);
    }

    private void startCallService(String callType) {
        Map<String, String> headers = new HashMap<>();
        Call call = null;
        if (callType.equalsIgnoreCase("audio")) {
            call = getSinchServiceInterface().callUserAudio(toUid);
        } else if (callType.equalsIgnoreCase("video")) {
            call = getSinchServiceInterface().callUserVideo(toUid);
        }

        String callId = call.getCallId();
        Intent callScreen = new Intent(this, CallScreenAudioVideoActivity.class);
        callScreen.putExtra("CALL_TYPE", "Outgoing call");
        callScreen.putExtra("SCREEN_SHOW", callType);
        callScreen.putExtra(SinchService.CALL_ID, callId);
        startActivity(callScreen);
    }


    @Override
    public void onClientStarted() {

    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {

    }

    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
        getSinchServiceInterface().startClient(currentUser.getUserId());
    }

    @Override
    protected void onServiceDisconnected() {
        getSinchServiceInterface().stopClient();
    }

    @Override
    public void onDestroy() {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }
        super.onDestroy();
    }


    private void launchCamera() {

        // Create the capture image intent
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the temporary File where the photo should go
            File photoFile = null;
            try {
                photoFile = BitmapUtils.createTempImageFile(this);
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                // Get the path of the temporary file
                mTempPhotoPath = photoFile.getAbsolutePath();

                // Get the content URI for the image file
                Uri photoURI = FileProvider.getUriForFile(this, BitmapUtils.FILE_PROVIDER_AUTHORITY, photoFile);

                // Add the URI so the camera can store the image
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                // Launch the camera activity
                startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
            }
        }
    }

    private void startRecording() {
        recordingFileUri = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC).getAbsolutePath() + "/trustApp";
        File recordingDir = new File(recordingFileUri);
        boolean success = true;
        if (!recordingDir.exists()) {
            success = recordingDir.mkdirs();
        }
        if (success) {
            recordingFileUri += "/trustChat_Recording" + currentUser.getUserId() + "_" + System.currentTimeMillis() + ".wav";
            recordingFile = new File(recordingFileUri);
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile(recordingFile.getAbsolutePath());
            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Toast.makeText(context, "prepare() failed", Toast.LENGTH_SHORT).show();
            }
            mRecorder.start();
        } else {
            Toast.makeText(context, "Unable to create file", Toast.LENGTH_SHORT).show();
        }
    }

    private void stopRecording() {
        try {
            mRecorder.stop();
            if (!isRecordingCancelled)
                sendAudioMessage();
        } catch (Exception e) {
            recordingFile.delete();  //you must delete the outputfile when the recorder stop failed.

        } finally {
            mRecorder.release();
            mRecorder = null;
        }

    }

    private void sendAudioMessage() {
        String filename = Util9.getUniqueValue(userId);
        Uri fileUri = Uri.fromFile(new File(recordingFileUri));
        File file = null;
        ChatModel.FileInfo fileInfo = getFileDetailFromUri(this, fileUri);
        String mimeType = null;
        try {
            file = FileUtil.from(this, fileUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (fileUri != null)
            mimeType = FileUtil.getMimeType(fileUri);
        String getSubFolder = FileUtil.getSubFolder(mimeType);
        assert file != null;
        String fileExt = file.getPath().substring(file.getPath().lastIndexOf("."));

        assert fileInfo != null;
        fileInfo.fileUrl = FILE_BASE_URL + getSubFolder + filename + fileExt;
        fileInfo.fileName = filename;
        sendMessage("Audio", MessageType.MESSAGE_TYPE_AUDIO, fileInfo, fileExt, mimeType, "", "", file, fileUri.toString());
        sendGCM("Audio");
    }


    private void updateOpponent(ChatRoomModel chatRoomModel) {
        String members = chatRoomModel.getMembers();
        Map<String, GroupUser> users = new Gson().fromJson(
                members, new com.google.gson.reflect.TypeToken<HashMap<String, Object>>() {
                }.getType());
        Gson gson = new Gson();
        for (Object value : users.values()) {
            JsonElement jsonElement = gson.toJsonTree(value);
            GroupUser groupUser = gson.fromJson(jsonElement, GroupUser.class);
            for (UserModel userModel : appUsers) {
                if (!groupUser.getId().equalsIgnoreCase(myUid)) {
                    if (groupUser.getId().equalsIgnoreCase(userModel.getUserId())) {
                        groupUser.setName(userModel.getUsernm());
                        users.put(groupUser.getId(), groupUser);
                        break;
                    }
                }
            }
        }
        chatRoomModel.setMembers(new Gson().toJson(users));
        ChatRoomService.updateGroupOnline(context, chatRoomModel, false, null);

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        //  Toast.makeText(this, "Selected Item: " + item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.view_contact:
                // do your code
                Intent intent = new Intent(ChatActivity.this, SingleProfileDetailActivity.class);
                intent.putExtra("title", roomTitle);
                intent.putExtra("roomId", roomID);
                intent.putExtra("toUid", toUid);
                startActivity(intent);
                return true;
            case R.id.clear_chat:
            case R.id.clear_chat_group:
                // do your code
                mAdapter.clearChat();
                return true;
            case R.id.group_info:
                // do your code
                Intent intentGroup = new Intent(ChatActivity.this, GroupDetailsActivity.class);
                intentGroup.putExtra("roomId", roomID);
                intentGroup.putExtra("IS_MEMBER", isPartOfGroup);
                startActivity(intentGroup);
                return true;
            default:
                return false;
        }
    }
}
