package com.app.trustChat.ui.otpVerificationFragment;

import android.view.View;

public interface OtpVerificationNavigator {

    void callVerificationApi(String otp);
}
