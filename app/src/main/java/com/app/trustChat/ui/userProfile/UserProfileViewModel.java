package com.app.trustChat.ui.userProfile;

import com.app.trustChat.base.BaseViewModel;

public class UserProfileViewModel extends BaseViewModel<UserProfileNavigator> {

    public void onBackPress() {
        getNavigator().callBack();
    }

    public void onEditName() {
        getNavigator().callEditName();
    }

    public void onEditStatus() {
        getNavigator().callEditAbout();
    }

    public void pickImage() {
        getNavigator().onProfilePickChooser();
    }

}
