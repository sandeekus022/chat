package com.app.trustChat.ui.callLog;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.model.CallLog;
import com.app.trustChat.repository.CallLogRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallLogFragment extends Fragment {

    private List<CallLog> callList = new ArrayList<>();
    private CallLogAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new GetAllCallLogsAsync().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_call_log, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        adapter = new CallLogAdapter(callList);
        recyclerView.setAdapter(adapter);
    }


    private class GetAllCallLogsAsync extends AsyncTask<Void, Void, List<CallLog>> {

        @Override
        protected List<CallLog> doInBackground(Void... voids) {
            CallLogRepository repository = new CallLogRepository(MyApplication.getInstance());
            return repository.getAllCallLog();
        }

        @Override
        protected void onPostExecute(List<CallLog> callLogs) {
            super.onPostExecute(callLogs);
            if (callLogs != null) {
                callList.addAll(callLogs);
                adapter.notifyDataSetChanged();
            }
        }
    }
}
