package com.app.trustChat.ui.profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;

import com.app.trustChat.BR;
import com.app.trustChat.BuildConfig;
import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.base.BaseFragment;
import com.app.trustChat.constants.UserStatus;
import com.app.trustChat.databinding.ProfileFragmentBinding;
import com.app.trustChat.model.Contacts;
import com.app.trustChat.model.DistrictModel;
import com.app.trustChat.model.OtpVerificationModel;
import com.app.trustChat.model.StateModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.UserRepository;
import com.app.trustChat.ui.chat.HomeActivity;
import com.app.trustChat.ui.contact.UserListFragment;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.ui.userProfile.UserProfileActivity;
import com.app.trustChat.utils.BitmapUtils;
import com.app.trustChat.utils.FileUtil;
import com.app.trustChat.utils.MainUtils;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProfileFragment extends BaseFragment<ProfileFragmentBinding, ProfileViewModel>
        implements ProfileNavigator {
    public static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private String currentPhotoPath;
    public static final int GALLERY_PICK_CODE = 10;
    private String profileImageUrl = null;
    private ProfileViewModel mViewModel;
    private ImageDownloaderTask task;
    private String fcmToken;
    private View view;
    private ArrayAdapter<StateModel> stateAdapter;
    private ArrayAdapter<DistrictModel> districtAdapter;
    private List<StateModel> stateList = new ArrayList<>();
    private List<DistrictModel> districtList = new ArrayList<>();
    private String mTempPhotoPath;

    @Override
    public int getLayoutId() {
        return R.layout.profile_fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public ProfileViewModel getViewModel() {
        if (mViewModel == null)
            mViewModel = new ProfileViewModel();
        mViewModel.setNavigator(this);
        return mViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFCMToken();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        String mobileNumber = SharedPrefManager.getInstance().get(SharedPrefManager.Key.MOBILE, "");
        profileImageUrl = SharedPrefManager.getInstance(getContext()).
                get(SharedPrefManager.Key.PROFILE_PIC, "");
        getViewModel().setMobileNumber(mobileNumber);

        String name = SharedPrefManager.getInstance(getContext()).
                get(SharedPrefManager.Key.NAME, "");

        String email = SharedPrefManager.getInstance(getContext()).
                get(SharedPrefManager.Key.EMAIL, "");
        getViewDataBinding().constraintLayout8.setVisibility(View.GONE);
        getViewDataBinding().etMobileNumber.setText(mobileNumber);
        getViewDataBinding().tvCountryCode.setText("+91");
        getViewDataBinding().etName.setText(name);
        getViewDataBinding().etEmail.setText(email);
        task = new ImageDownloaderTask(profileImageUrl, getContext());
        task.execute();

        stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout
                .simple_spinner_item, stateList);
        getViewDataBinding().spinnerState.setAdapter(stateAdapter);
        getViewDataBinding().spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getViewDataBinding().tvState.setText(stateList.get(position).getState());
                getDistricts(getViewModel().state.getValue());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setDistrictAdapter();
        getStates();

    }


    @Override
    public void pickProfileImage() {
        if (checkAndRequestPermission()) {
            openDialogChooser();

        }
    }

    @Override
    public void showEmailValidation(String msg) {
        getViewDataBinding().etEmail.requestFocus();
        getViewDataBinding().etEmail.setError(msg);
    }

    @Override
    public void showNameValidation(String msg) {
        getViewDataBinding().etName.requestFocus();
        getViewDataBinding().etName.setError(msg);
    }

    @Override
    public void onBackPress() {
        getActivity().onBackPressed();
    }

    @Override
    public void setProfilePicureUrl(String image) {
        ProgressDialog.hide(getChildFragmentManager());
        profileImageUrl = image;
        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.PROFILE_PIC, image);
    }

    @Override
    public void updaterProfile(String name, String email, String mobile) {
        ProgressDialog.show(getChildFragmentManager());
        Single<Response<OtpVerificationModel>> caller = RetrofitBuilder.getService(getContext())
                .updateProfile(mobile, name, email, getViewModel().state.getValue(), getViewModel().district.getValue());
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<OtpVerificationModel>>() {
                    @Override
                    public void onSuccess(Response<OtpVerificationModel> response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        if (response.body().getStatus().equalsIgnoreCase("1")) {
                            SharedPrefManager.getInstance(getContext()).
                                    set(SharedPrefManager.Key.NAME, response.body().getName());
                            SharedPrefManager.getInstance(getContext()).
                                    set(SharedPrefManager.Key.EMAIL, response.body().getEmail());

                            MainUtils.showSimpleOkayDialog(getContext(), "Message",
                                    "Profile Updated Successful",
                                    () -> saveUserToFirebase(name, email, mobile));
                        }
                        if (response.body().getStatus().equalsIgnoreCase("0")) {
                            MainUtils.showSimpleOkayDialog(getContext(), "Message",
                                    "Profile Updated Successful",
                                    () -> saveUserToFirebase(name, email, mobile));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }


    @Override
    public void saveUserToFirebase(String name, String email, String mobile) {

        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.NAME, name);
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        SharedPrefManager.getInstance(getContext()).
                set(SharedPrefManager.Key.EMAIL, email);


        String uid = SharedPrefManager.getInstance(getContext()).
                get(SharedPrefManager.Key.USER_ID, "");
        UserModel userModel = new UserModel();
        userModel.setUserId(uid);
        userModel.setUsernm(name);
        userModel.setEmail(email);
        userModel.setMobile(mobile);
        userModel.setToken(fcmToken);
        userModel.setLastSeen(System.currentTimeMillis());
        userModel.setUserphoto(profileImageUrl == null ? "" : profileImageUrl);
        userModel.setUserStatus(UserStatus.ONLINE);
        userModel.setTypingTo(UserStatus.TYPING_TO_NO_ONE);
        userModel.setUsermsg("");
        userModel.setMood("Hi, I am using TrustApp");
        userModel.setState(getViewDataBinding().tvState.getText().toString());
        userModel.setDistrict(getViewDataBinding().tvDistrict.getText().toString());
        userModel.setLastSeen(System.currentTimeMillis());
        SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.USER, new Gson().toJson(userModel));
        ProgressDialog.show(getChildFragmentManager());
        db.collection("users").document(uid)
                .set(userModel)
                .addOnSuccessListener(aVoid -> createSessionAndLoadHomeScreen());
    }

    @Override
    public void selectDistrict() {
        if (getViewModel().state.getValue() == null || getViewModel().state.getValue().isEmpty()) {
            Toast.makeText(getContext(), "Please select state first", Toast.LENGTH_SHORT).show();
        } else {
            getViewDataBinding().spinnerDistrict.performClick();
        }
    }

    @Override
    public void selectState() {
        getViewDataBinding().spinnerState.performClick();
    }

    @Override
    public void showStateValidation(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDistValidation(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }


    private void createSessionAndLoadHomeScreen() {
        new ContactAsync().execute();
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            fcmToken = instanceIdResult.getToken();
            Log.e("fcmToken", fcmToken);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == GALLERY_PICK_CODE && resultCode == Activity.RESULT_OK && data != null) {
            try {
                Log.e("UCrop", "data---" + data.getData());
                Uri destinationUri = Uri.fromFile(getImageFile());
                openCropActivity(data.getData(), destinationUri);
            } catch (Exception ex) {
                Toast.makeText(getActivity(), ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_ACTION_PICK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap mResultsBitmap = BitmapUtils.resamplePic(getContext(), mTempPhotoPath);
            Uri resultUri = Uri.fromFile(new File(BitmapUtils.saveImage(getContext(), mResultsBitmap)));

            try {
                File file = FileUtil.from(getContext(), resultUri);

                Glide.with(getContext()).load(file)
                        .placeholder(R.drawable.ic_user)
                        .into(getViewDataBinding().ivUserImage);
                ProgressDialog.show(getChildFragmentManager());
                getViewModel().uploadProfileImage(getContext(), file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            Uri imageUri = UCrop.getOutput(data);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                drawable.setCircular(true);
                getViewDataBinding().ivUserImage.setImageDrawable(drawable);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File file = null;
            try {
                file = FileUtil.from(getContext(), imageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ProgressDialog.show(getChildFragmentManager());
            getViewModel().uploadProfileImage(getContext(), file);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Throwable cropError = UCrop.getError(data);
            Log.e("UCrop", "cropError---" + cropError);
        }

    }

    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(getActivity(), R.color.colorOrange));
        UCrop.of(sourceUri, destinationUri)
                //.withAspectRatio(5f, 5f)
                .start(getContext(), ProfileFragment.this);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (task != null)
            task.cancel(true);

    }


    private void getStates() {
        ProgressDialog.show(getChildFragmentManager());
        Single<List<StateModel>> caller = RetrofitBuilder.getService(getContext())
                .getState();
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<StateModel>>() {
                    @Override
                    public void onSuccess(List<StateModel> response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        stateList.clear();
                        stateList.addAll(response);
                        stateAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());

                    }
                }));
    }


    private void getDistricts(String state) {
        ProgressDialog.show(getChildFragmentManager());
        Single<List<DistrictModel>> caller = RetrofitBuilder.getService(getContext())
                .getDistrict(state);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(caller
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<DistrictModel>>() {
                    @Override
                    public void onSuccess(List<DistrictModel> response) {
                        ProgressDialog.hide(getChildFragmentManager());
                        districtList.clear();
                        districtList.addAll(response);
                        setDistrictAdapter();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ProgressDialog.hide(getChildFragmentManager());
                    }
                }));
    }

    private void setDistrictAdapter() {
        districtAdapter = new ArrayAdapter<>(getContext(), android.R.layout
                .simple_spinner_item, districtList);
        getViewDataBinding().spinnerDistrict.setAdapter(districtAdapter);
        getViewDataBinding().spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getViewModel().district.setValue(districtList.get(position).getDistrict());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private class ImageDownloaderTask extends AsyncTask<Void, Void, Bitmap> {
        private String profileImageUrl;
        @SuppressLint("StaticFieldLeak")
        private Context context;

        ImageDownloaderTask(String profileImageUrl, Context context) {
            this.profileImageUrl = profileImageUrl;
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            URL url;
            Bitmap bitmap = null;
            try {
                url = new URL(profileImageUrl);
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            RoundedBitmapDrawable drawable =
                    RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
            drawable.setCircular(true);
            ImageView ivUser = view.findViewById(R.id.ivUserImage);
            ivUser.setImageDrawable(drawable);
        }
    }

    private class ContactAsync extends AsyncTask<Void, Void, Map<String, Contacts>> {
        @Override
        protected void onPreExecute() {
            ProgressDialog.show(getChildFragmentManager());
            super.onPreExecute();
        }

        @Override
        protected Map<String, Contacts> doInBackground(Void... voids) {
            Map<String, Contacts> nameList = new HashMap<>();
            ContentResolver cr = getContext().getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);
            if ((cur != null ? cur.getCount() : 0) > 0) {
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor crPhones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = ?", new String[]{id}, null);


                        while (crPhones.moveToNext()) {
                            String phone = crPhones.getString(crPhones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String phoneNumber = phone.replaceAll("[^a-zA-Z0-9]", "");
                            if (phoneNumber.length() >= 10) {
                                String mobileNumber = phoneNumber.substring(phoneNumber.length() - 10);
                                nameList.put(mobileNumber, new Contacts(name, mobileNumber));
                            }
                        }
                        crPhones.close();
                    }
                }
            }
            if (cur != null) {
                cur.close();
            }
            return nameList;
        }

        @Override
        protected void onPostExecute(Map<String, Contacts> mobileArray) {
            super.onPostExecute(mobileArray);
            if (mobileArray.size() > 0) {
                FirebaseFirestore.getInstance().collection("users").orderBy("usernm").get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ProgressDialog.hide(getChildFragmentManager());
                        QuerySnapshot documentSnapshot = task.getResult();
                        List<UserModel> list = documentSnapshot.toObjects(UserModel.class);
                        Map<String, UserModel> userMap = new HashMap<>();
                        for (UserModel u : list) {
                            userMap.put(u.getMobile(), u);
                        }
                        List<UserModel> filteredList = new ArrayList<>();
                        for (Contacts user : mobileArray.values()) {
                            if (userMap.containsKey(user.getMobile())) {
                                UserModel userModel = userMap.get(user.getMobile());
                                userModel.setContact(true);
                                userModel.setUsernm(user.getName());
                                filteredList.add(userModel);
                            } else {
                                UserModel userModel = new UserModel();
                                userModel.setUserId(user.getMobile());
                                userModel.setContact(false);
                                userModel.setMobile(user.getMobile());
                                userModel.setUsernm(user.getName());
                                filteredList.add(userModel);
                            }
                        }
                        ProgressDialog.hide(getChildFragmentManager());
                        SharedPrefManager prefManager = SharedPrefManager.getInstance(getContext());
                        prefManager.set(SharedPrefManager.Key.USERS_LIST, new Gson().toJson(filteredList));
                        SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.SESSION, true);
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
            } else {
                SharedPrefManager.getInstance(getContext()).set(SharedPrefManager.Key.SESSION, true);
                Intent intent = new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();
            }

        }
    }


    private String[] appPermissions = {
            WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE,
            CAMERA
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    private boolean checkAndRequestPermission() {
        List<String> listPermissionNeeded = new ArrayList<>();
        for (String perm : appPermissions) {
            if (ContextCompat.checkSelfPermission(getActivity(), perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionNeeded.add(perm);
            }
        }

        //Ask for non-granted permissions
        if (!listPermissionNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(),
                    listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]),
                    PERMISSIONS_REQUEST_CODE);
            return false;
        }

        //App has all permissions. Proceed ahead
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            //Gather permission grant results
            for (int i = 0; i < grantResults.length; i++) {
                //Add only permission which are denied
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            //Check if all permissions are granted
            if (deniedCount == 0) {
                //Proceed ahead with the app
                openDialogChooser();
            }
            //Atleast one or all permissions are denied
            else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();

                    //permission is denied(this is the first time, when "never ask again" is not checked)
                    //so ask again explaining the usage of permission
                    //shouldShowRequestPermissionRationale will return true
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permName)) {
                        //Show dialog of explanation
                        showDialog("", "This app needs Camera and Storage permission to work without and problems.",
                                "Yes, Grant Permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        if (checkAndRequestPermission()) {
                                            openDialogChooser();

                                        }
                                    }
                                },
                                "No, Exit app", (dialogInterface, i) -> {
                                    dialogInterface.dismiss();
                                    //finish();
                                }, false);
                        break;
                    } else {
                        //permission is denied( and never ask again is checked)
                        //shouldShowRequestPermissionRationale will return false
                        //Ask user to go to settings and manually allow permissions
                        showDialog("", "You have denied some permissions. Allow all permissions at [Setting] > [Permissions]",
                                "Go to Settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        //Go to settings
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);

                                    }
                                }, "No, Exit app", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        //finish();
                                    }
                                }, false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showDialog(String title, String msg, String positiveLabel,
                                  DialogInterface.OnClickListener positiveOnClick,
                                  String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
                                  boolean isCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);

        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }


    public Intent getGalleryPickerIntent() {

        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        return pictureIntent;
    }

    public void takeCameraIntent() {
        Uri uriImage;
        Intent pictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        try {
            File file = getImageFile();
            if (Build.VERSION.SDK_INT >= 24) {
                uriImage = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID.concat(".provider"), file);
            } else {
                uriImage = Uri.fromFile(file);
            }
            pictureIntent.putExtra("output", uriImage);
            startActivityForResult(pictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Please select another image", Toast.LENGTH_SHORT).show();
        }
    }

    private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists()) {
            System.out.println("File exists");
        } else {
            System.out.println("File not exists");
        }
        File file = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }


    public void openDialogChooser() {
        final Dialog registerUser = new Dialog(getContext());
        registerUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerUser.setContentView(R.layout.chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(registerUser.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        registerUser.getWindow().setAttributes(lp);
        registerUser.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        registerUser.setCancelable(false);
        registerUser.show();

        LinearLayout galleryId = registerUser.findViewById(R.id.llGallery);
        LinearLayout cameraId = registerUser.findViewById(R.id.llCamera);
        RelativeLayout relativeLayout = registerUser.findViewById(R.id.rlDailog);

        cameraId.setOnClickListener(v -> {
            registerUser.dismiss();
            launchCamera();
        });

        galleryId.setOnClickListener(v -> {
            registerUser.dismiss();
            startActivityForResult(Intent.createChooser(getGalleryPickerIntent(), "Select Profile Picture"), GALLERY_PICK_CODE);

        });


        relativeLayout.setOnClickListener(v -> registerUser.dismiss());

    }

    private void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = BitmapUtils.createTempImageFile(getContext());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mTempPhotoPath = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(getContext(), BitmapUtils.FILE_PROVIDER_AUTHORITY, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
            }
        }
    }

}