package com.app.trustChat.ui.voiceCall;

public interface OnClientStartListener {
    void onClientStarted();
}
