package com.app.trustChat.ui.profile;

import android.content.Context;
import android.widget.Toast;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.base.BaseViewModel;
import com.app.trustChat.model.MediaUploadModel;
import com.app.trustChat.networking.APICallbackListener;
import com.app.trustChat.networking.ApiError;
import com.app.trustChat.networking.GenericApiCaller;
import com.app.trustChat.networking.RetrofitBuilder;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.ui.main.ProgressDialog;
import com.app.trustChat.ui.userProfile.UserProfileActivity;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.regex.Pattern;

import androidx.lifecycle.MutableLiveData;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ProfileViewModel extends BaseViewModel<ProfileNavigator> {

    public MutableLiveData<String> name = new MutableLiveData<>();
    public MutableLiveData<String> email = new MutableLiveData<>();
    public MutableLiveData<String> mobile = new MutableLiveData<>();
    public MutableLiveData<String> state = new MutableLiveData<>();
    public MutableLiveData<String> district = new MutableLiveData<>();
    public MutableLiveData<String> referralCode = new MutableLiveData<>();
    private String mobileNumber = "";

    public void onBackPress() {
        getNavigator().onBackPress();
    }

    public void pickImage() {
        getNavigator().pickProfileImage();
    }

    public void selectDistrict() {
        getNavigator().selectDistrict();
    }

    public void selectState() {
        getNavigator().selectState();
    }

    private boolean isValidRequest() {
        if (name.getValue() == null || name.getValue().isEmpty()) {
            getNavigator().showNameValidation("Please enter name");
            return false;
        }
        if (email.getValue() == null || email.getValue().isEmpty()) {
            getNavigator().showEmailValidation("Please enter email");
            return false;
        }

        if (!isValidEmailId(email.getValue())) {
            getNavigator().showEmailValidation("Please enter valid email");
            return false;
        }

        if (state.getValue() == null || state.getValue().isEmpty()) {
            getNavigator().showStateValidation("Please select state");
            return false;
        }
        if (district.getValue() == null || district.getValue().isEmpty()) {
            getNavigator().showDistValidation("Please select district");
            return false;
        }

        return true;
    }


    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public void uploadProfileImage(Context context, File file) {
        MultipartBody.Part requestImage = null;
        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
        RequestBody image = RequestBody.create(MediaType.parse("image/*"), file);
        requestImage = MultipartBody.Part.createFormData("image", file.getName(), image);

        new GenericApiCaller<>(context, RetrofitBuilder.getService(context)
                .uploadProfileImage(requestImage, mobile))
                .doApiCall(new APICallbackListener<MediaUploadModel>() {
                    @Override
                    public void onSuccess(Response<MediaUploadModel> response) {
                        // ProgressDialog.hide(getSupportFragmentManager());
                        if (response.body().getStatus() == 1) {
                            Toast.makeText(context, "Profile Picture Upload Successfully\nIt will reflect soon on your profile",
                                    Toast.LENGTH_SHORT).show();
                            SharedPrefManager.getInstance(context).
                                    set(SharedPrefManager.Key.PROFILE_PIC, response.body().getImage());
                            getNavigator().setProfilePicureUrl(response.body().getImage());

                        } else {
                            Toast.makeText(context, "Profile Picture Upload Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(ApiError object) {
                        // ProgressDialog.hide((ProfileFragment)context.getApplicationContext());
                        Toast.makeText(context, object.getResponseMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void submitData() {
        if (isValidRequest())
            getNavigator().updaterProfile(name.getValue(), email.getValue(), mobile.getValue());

    }


    public void setMobileNumber(String s) {
        this.mobileNumber = s;
    }
}
