package com.app.trustChat.base;

import java.lang.ref.WeakReference;

import androidx.lifecycle.ViewModel;

public class BaseViewModel<N> extends ViewModel {

    private WeakReference<N> mNavigator;

    public N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N mNavigator) {
        this.mNavigator = new WeakReference<>(mNavigator);
    }
}
