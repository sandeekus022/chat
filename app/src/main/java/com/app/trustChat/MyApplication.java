package com.app.trustChat;

import android.os.Handler;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.app.trustChat.constants.UserStatus;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

/**
 * Created by Rahulucky03 on 03-02-2019.
 */
public class MyApplication extends MultiDexApplication {

    private static MyApplication Instance;
    public static volatile Handler applicationHandler = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Instance = this;
        EmojiManager.install(new IosEmojiProvider());

        applicationHandler = new Handler(getInstance().getMainLooper());

        NativeLoader.initNativeLibs(MyApplication.getInstance());

    }

    public static MyApplication getInstance() {
        return Instance;
    }


    public static void saveCurrentUser(UserModel user) {
        SharedPrefManager.getInstance(getInstance()).
                set(SharedPrefManager.Key.USER, new Gson().toJson(user));
    }

    public UserModel getCurrentUser() {
        UserModel currentUser = currentUser = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        return currentUser;
    }


    public void setUserOnline(boolean inChat) {
        UserModel currentUser = getCurrentUser();
        SharedPrefManager sharedPrefManager = SharedPrefManager.getInstance(this);
        DocumentReference fireStore = FirebaseFirestore.getInstance()
                .collection("users").document(currentUser.getUserId());
        currentUser = new Gson().fromJson(sharedPrefManager
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);

        currentUser.setUserphoto(sharedPrefManager.get(SharedPrefManager.Key.PROFILE_PIC, ""));
        if (currentUser != null) {
            if (inChat)
                currentUser.setInChat("Y");
            currentUser.setUserStatus(UserStatus.ONLINE);
            fireStore.set(currentUser);
            MyApplication.saveCurrentUser(currentUser);
        }
    }

    public void setUserOffline() {
        UserModel currentUser = getCurrentUser();
        DocumentReference fireStore = FirebaseFirestore.getInstance()
                .collection("users").document(currentUser.getUserId());
        currentUser = new Gson().fromJson(SharedPrefManager.getInstance(this)
                .get(SharedPrefManager.Key.USER, ""), UserModel.class);
        if (currentUser != null) {
            currentUser.setInChat("N");
            currentUser.setLastSeen(System.currentTimeMillis());
            currentUser.setUserStatus(UserStatus.OFFLINE);
            fireStore.set(currentUser).addOnCompleteListener(task -> {
                Log.e("UPDATED", "DONE");
            });
            MyApplication.saveCurrentUser(currentUser);
        }
    }


}
