package com.app.trustChat.repository;

import android.app.Application;

import com.app.trustChat.dao.CallDao;
import com.app.trustChat.db.TrustChatDatabase;
import com.app.trustChat.model.CallLog;

import java.util.List;

public class CallLogRepository {
    private CallDao callDao;
    public CallLogRepository(Application application) {
        TrustChatDatabase database = TrustChatDatabase.getDatabase(application);
        callDao = database.getCallDao();
    }

    public Long insertLog(CallLog log){
        return callDao.insertCallLog(log);
    }

    public List<CallLog> getAllCallLog(){
        return callDao.getAllLogs();
    }
}
