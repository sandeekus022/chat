package com.app.trustChat.repository;

import android.app.Application;

import com.app.trustChat.dao.MessageDao;
import com.app.trustChat.db.TrustChatDatabase;
import com.app.trustChat.model.Message;

import java.util.List;

public class MessageRepository {

    private MessageDao messageDao;

    public MessageRepository(Application application) {
        TrustChatDatabase database = TrustChatDatabase.getDatabase(application);
        this.messageDao = database.messageDao();
    }

    public List<Message> getMessages(String roomId) {
        return messageDao.getAllMessages(roomId);
    }

    public List<Message> getAllMessages(String roomId, String isSeen) {
        return messageDao.getUnsentMsgs(roomId, isSeen);
    }

    public Message getMessage(String roomId, Long messageId) {
        return messageDao.getMessage(roomId, messageId);
    }

    public void deleteMessage(String msgId) {
        messageDao.deleteMessageById(msgId);
    }

    public Long insertMessage(Message msg) {
        return messageDao.insertMessage(msg);
    }

    public Integer updateMessage(Message msg) {
        return messageDao.updateMessage(msg);
    }

    public void deleteAllMessages(String roomId){
         messageDao.deleteMessage();
    }
}
