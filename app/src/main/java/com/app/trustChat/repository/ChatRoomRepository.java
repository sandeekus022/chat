package com.app.trustChat.repository;

import android.app.Application;

import com.app.trustChat.dao.ChatRoomDao;
import com.app.trustChat.db.TrustChatDatabase;
import com.app.trustChat.model.ChatRoomModel;

import java.util.List;

public class ChatRoomRepository {
    private ChatRoomDao chatRoomDao;
    private List<ChatRoomModel> chatRoomList;

    public ChatRoomRepository(Application application) {
        TrustChatDatabase database = TrustChatDatabase.getDatabase(application);
        chatRoomDao = database.getChatRoomDao();
    }

    public List<ChatRoomModel> getAllChatRooms() {
        chatRoomList = chatRoomDao.getAllRooms();
        return chatRoomList;
    }

    public ChatRoomModel getChatRoom(String roomId) {
        return chatRoomDao.getRoomDetails(roomId);
    }

    public Long createChatRoom(ChatRoomModel room) {
        return chatRoomDao.createChatRoom(room);
    }

    public Integer update(ChatRoomModel room) {
        return chatRoomDao.updateChatRoom(room);
    }

    public void updateRoomPhoto(String roomId, String photo) {
         chatRoomDao.updateRoomPhoto(roomId, photo);
    }


    public void deleteChatRoomById(String roomId) {
        chatRoomDao.deleteChatRoomById(roomId);
    }
}
