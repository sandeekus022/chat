package com.app.trustChat.repository;

import android.app.Application;

import com.app.trustChat.dao.UserDao;
import com.app.trustChat.db.TrustChatDatabase;
import com.app.trustChat.model.UserModel;

import java.util.List;

public class UserRepository {
    private UserDao userDao;
    private List<UserModel> users;

    public UserRepository(Application application){
        TrustChatDatabase database = TrustChatDatabase.getDatabase(application);
        userDao = database.userDao();
    }

    public List<UserModel> getUsers() {
        users = userDao.getAllUsers();
        return users;
    }
    public UserModel getUser(String userId) {
        UserModel user = userDao.getAllUser(userId);
        return user;
    }

    public void updateUsers(List<UserModel> users){
        TrustChatDatabase.databaseWriteExecutor.execute(() -> userDao.updateUserList(users));
    }
}
