package com.app.trustChat.services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.app.trustChat.MyApplication;
import com.app.trustChat.model.Contacts;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.preferences.SharedPrefManager;
import com.app.trustChat.repository.UserRepository;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class UpdateContactsService extends IntentService {
    public UpdateContactsService() {
        super("UpdateContactsService");
    }

    public static void getContactAsync(Context context) {
        Intent intent = new Intent(context, UpdateContactsService.class);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            getFilteredUsers();
        }
    }


    private void getFilteredUsers() {
        Map<String, Contacts> mobileArray = getMobileContacts();
        if (mobileArray.size() > 0) {
            FirebaseFirestore.getInstance().collection("users").orderBy("usernm").get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    QuerySnapshot documentSnapshot = task.getResult();
                    List<UserModel> list = documentSnapshot.toObjects(UserModel.class);
                    Map<String, UserModel> userMap = new HashMap<>();
                    for (UserModel u : list) {
                        userMap.put(u.getMobile(), u);
                    }
                    List<UserModel> filteredList = new ArrayList<>();
                    for (Contacts user : mobileArray.values()) {
                        if (userMap.containsKey(user.getMobile())) {
                            UserModel userModel = userMap.get(user.getMobile());
                            userModel.setContact(true);
                            userModel.setUsernm(user.getName());
                            filteredList.add(userModel);
                        } else {
                            UserModel userModel = new UserModel();
                            userModel.setUserId(user.getMobile());
                            userModel.setContact(false);
                            userModel.setMobile(user.getMobile());
                            userModel.setUsernm(user.getName());
                            filteredList.add(userModel);
                        }
                    }
                    new ContactDbAsync(filteredList).doInBackground();
                }
            });
        }
    }


    private Map<String, Contacts> getMobileContacts() {
        Map<String, Contacts> nameList = new HashMap<>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor crPhones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = ?", new String[]{id}, null);


                    while (crPhones.moveToNext()) {
                        String phone = crPhones.getString(crPhones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String phoneNumber = phone.replaceAll("[^a-zA-Z0-9]", "");
                        if (phoneNumber.length() >= 10) {
                            String finalNumber = phoneNumber.substring(phoneNumber.length() - 10);
                            nameList.put(finalNumber, new Contacts(name, finalNumber));
                        }
                    }
                    crPhones.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }
        return nameList;
    }

    private class ContactDbAsync extends AsyncTask<Void, Void, Void> {
        private List<UserModel> lists;

        public ContactDbAsync(List<UserModel> lists) {
            this.lists = lists;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            UserRepository repository = new UserRepository(MyApplication.getInstance());
            repository.updateUsers(lists);
            SharedPrefManager prefManager = SharedPrefManager.getInstance(UpdateContactsService.this);
            prefManager.set(SharedPrefManager.Key.USERS_LIST, new Gson().toJson(lists));
            return null;
        }
    }


}
