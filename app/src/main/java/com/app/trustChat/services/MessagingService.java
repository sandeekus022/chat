package com.app.trustChat.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.app.trustChat.MyApplication;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.constants.MessageStatus;
import com.app.trustChat.model.Message;
import com.app.trustChat.repository.MessageRepository;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class MessagingService extends IntentService {
    static MessageCallback callback;

    public MessagingService() {
        super("MessagingService");
    }

    public void sendMessageToFirebase(Context context, Message msg, MessageCallback callback) {
        Intent intent = new Intent(context, MessagingService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("MSG", msg);
        intent.putExtras(bundle);
        intent.putExtra("IS_UPDATE", false);
        context.startService(intent);
        this.callback = callback;
    }


    public static void updateMessageOnFirebase(Context context, Message msg) {

    }

    public void sendBulkMessages(Context context, List<Message> msgList, MessageCallback callback) {
        for (Message message : msgList) {
            sendMessageToFirebase(context, message, callback);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean isUpdate = intent.getBooleanExtra("IS_UPDATE", false);
        Message msg = intent.getParcelableExtra("MSG");
        if (!isUpdate) {
            final Map<String, Object> messages = new HashMap<>();
            messages.put("msgId", msg.getMsgId());
            messages.put("roomId", msg.getRoomId());
            messages.put("UId", msg.getUId());
            messages.put("name", msg.getName());
            messages.put("msg", msg.getMsg());
            messages.put("msgType", msg.getMsgType());
            messages.put("createdAt", System.currentTimeMillis());
            messages.put("isSeen", MessageStatus.SENT);
            messages.put("contacts", msg.getContacts());
            messages.put("locatiom", msg.getLocation());
            msg.setIsSeen(MessageStatus.SENT);
            List<String> readUsers = new ArrayList<>();
            readUsers.add(msg.getUId());
            messages.put("readUsers", new Gson().toJson(readUsers));
            if (msg.getFileName() != null) {
                messages.put("fileName", msg.getFileName());
                messages.put("fileSize", msg.getFileSize());
                messages.put("mediaUrl", msg.getMediaUrl());
                messages.put("mediaType", msg.getMediaType());
                messages.put("mimeType", msg.getMimeType());
                messages.put("mediaMessageStatus", msg.getMediaMessageStatus());
            }

            DocumentReference msgRef = FirebaseFirestore.getInstance()
                    .collection("rooms").document(msg.getRoomId())
                    .collection("messages").document(msg.getMsgId());

            msgRef.set(messages).addOnCompleteListener(task -> {
                new UpdateMessageAsync(msg).execute();
            });
        } else {
            //todo update Message on firebase
        }
    }


    static class UpdateMessageAsync extends AsyncTask<Void, Void, Integer> {
        Message message;

        public UpdateMessageAsync(Message message) {
            this.message = message;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            MessageRepository repository = new MessageRepository(MyApplication.getInstance());
            return repository.updateMessage(message);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            callback.onSuccess(message);
        }
    }

}

