package com.app.trustChat.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.app.trustChat.MyApplication;
import com.app.trustChat.callbacks.MessageCallback;
import com.app.trustChat.constants.ChatRoomStatus;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.GroupUser;
import com.app.trustChat.repository.ChatRoomRepository;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ChatRoomService extends IntentService {

    private static MessageCallback messageCallback;

    public ChatRoomService() {
        super("CreateChatRoomService");
    }

    public static void createGroupOnline(Context context, ChatRoomModel roomModel) {
        Intent intent = new Intent(context, ChatRoomService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("ROOM", roomModel);
        intent.putExtras(bundle);
        intent.putExtra("IS_CREATE", true);
        context.startService(intent);
    }

    public static void updateGroupOnline(Context context, ChatRoomModel roomModel, boolean isUpdate, MessageCallback callback) {
        Intent intent = new Intent(context, ChatRoomService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("ROOM", roomModel);
        intent.putExtras(bundle);
        intent.putExtra("IS_UPDATE", isUpdate);
        intent.putExtra("IS_CREATE", false);
        messageCallback = callback;
        context.startService(intent);

    }

    public static void updateMembers(Context context, ChatRoomModel roomModel, MessageCallback callback) {
        Intent intent = new Intent(context, ChatRoomService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("ROOM", roomModel);
        intent.putExtras(bundle);
        intent.putExtra("IS_UPDATE", false);
        intent.putExtra("IS_CREATE", false);
        intent.putExtra("IS_UPDATE_MEMBERS", true);
        messageCallback = callback;
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        ChatRoomModel chatRoomModel = intent.getExtras().getParcelable("ROOM");
        String members = chatRoomModel.getMembers();
        Map<String, GroupUser> users = new Gson().fromJson(
                members, new TypeToken<HashMap<String, Object>>() {
                }.getType());
        boolean isCreate = intent.getBooleanExtra("IS_CREATE", false);
        boolean isUpdate = intent.getBooleanExtra("IS_UPDATE", false);
        boolean isUpdateMemberOnly = intent.getBooleanExtra("IS_UPDATE_MEMBERS", false);
        if (isCreate) {
            Map<String, Object> data = new HashMap<>();
            chatRoomModel.setStatus(ChatRoomStatus.ONLINE);
            chatRoomModel.setUpdatedAt(System.currentTimeMillis());
            data.put("userId", chatRoomModel.getUserId());
            data.put("title", chatRoomModel.getTitle());
            data.put("isGroup", chatRoomModel.isGroup());
            data.put("members", users);
            data.put("createdAt", chatRoomModel.getCreatedAt());
            if (isUpdate)
                data.put("updatedAt", System.currentTimeMillis());
            else
                data.put("updatedAt", chatRoomModel.getUpdatedAt());
            data.put("photo", chatRoomModel.getPhoto());
            data.put("lastMsg", chatRoomModel.getLastMsg());
            data.put("unreadCount", chatRoomModel.getUnreadCount());
            data.put("subject", chatRoomModel.getSubject());
            data.put("opponentId", chatRoomModel.getOpponentId());
            data.put("status", ChatRoomStatus.ONLINE);
            DocumentReference newRoom = FirebaseFirestore.getInstance()
                    .collection("rooms").document(chatRoomModel.getRoomId());

            newRoom.set(data).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    new UpdateRoomAsync(chatRoomModel).execute();
                }
            });
        } else {
            DocumentReference room = FirebaseFirestore.getInstance()
                    .collection("rooms").document(chatRoomModel.getRoomId());
            Map<String, Object> data = new HashMap<>();
            if (isUpdate) {
                data.put("updatedAt", System.currentTimeMillis());
                data.put("lastMsg", chatRoomModel.getLastMsg());
            }
            data.put("status", ChatRoomStatus.ONLINE);
            data.put("members", users);
            room.update(data).addOnCompleteListener(task -> {
                new UpdateRoomAsync(chatRoomModel).execute();
                if (messageCallback != null)
                    messageCallback.onSuccess(null);
            });
        }
    }


    static class UpdateRoomAsync extends AsyncTask<Void, Void, Integer> {
        ChatRoomModel chatRoomModel;

        public UpdateRoomAsync(ChatRoomModel chatRoomModel) {
            this.chatRoomModel = chatRoomModel;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            return repository.update(chatRoomModel);
        }
    }


}



