package com.app.trustChat.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.trustChat.constants.MediaMessageStatus;
import com.app.trustChat.model.MediaUploadModel;
import com.app.trustChat.networking.LoggerUtils;
import com.app.trustChat.networking.RetrofitBuilder;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class FileUploaderService extends IntentService {
    private static final String TAG = "FileUploaderService";
    private static final String FILE_NAME = "fileName";
    private static final String FILE = "file";
    private static final String MOBILE_NUMBER = "mobileNumber";
    private static final String MESSAGE_ID = "messageId";
    private static final String ROOM_ID = "roomId";

    public FileUploaderService() {
        super("FileUploaderService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void uploadFile(Context context, File file, String msgId, String roomId, String fileName, String mobileNumber) {
        Intent intent = new Intent(context, FileUploaderService.class);
        intent.putExtra(FILE, file);
        intent.putExtra(FILE_NAME, mobileNumber);
        intent.putExtra(MOBILE_NUMBER, fileName);
        intent.putExtra(MESSAGE_ID, msgId);
        intent.putExtra(ROOM_ID, roomId);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            final String fileName = intent.getStringExtra(FILE_NAME);
            final String mobile = intent.getStringExtra(MOBILE_NUMBER);
            final String msgId = intent.getStringExtra(MESSAGE_ID);
            final String roomId = intent.getStringExtra(ROOM_ID);
            final File file = (File) intent.getSerializableExtra(FILE);
            uploadFileToServer(file, fileName, mobile, msgId, roomId);

        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void uploadFileToServer(File file, String fileNamee, String mobileNumber, String msgId, String roomId) {
        CompositeDisposable disposable = new CompositeDisposable();
        MultipartBody.Part doc = null;
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), fileNamee);
        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        doc = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        Single<Response<MediaUploadModel>> caller = RetrofitBuilder.getService(this)
                .uploadFile(doc, mobile, fileName);

        disposable.add(caller.subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .subscribeWith(new DisposableSingleObserver<Response<MediaUploadModel>>() {
                    @Override
                    public void onSuccess(Response<MediaUploadModel> mediaUploadModelResponse) {
                        LoggerUtils.debug(TAG, mediaUploadModelResponse.body().getFile());
                        updateMessageOnFirebase(roomId, msgId, MediaMessageStatus.UPLOADED);
                    }

                    @Override
                    public void onError(Throwable e) {
                        updateMessageOnFirebase(roomId, msgId, MediaMessageStatus.FAILED);
                        LoggerUtils.debug(TAG, e.getMessage());
                    }
                }));
    }


    private void updateMessageOnFirebase(String roomId, String msgId, String status) {
        DocumentReference msgRef = FirebaseFirestore.getInstance()
                .collection("rooms").document(roomId)
                .collection("messages").document(msgId);
        msgRef.update("mediaMessageStatus", status).addOnSuccessListener(aVoid -> Log.d(TAG, status));
    }
}
