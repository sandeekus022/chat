package com.app.trustChat.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.app.trustChat.MyApplication;
import com.app.trustChat.R;
import com.app.trustChat.constants.UserStatus;
import com.app.trustChat.model.ChatRoomModel;
import com.app.trustChat.model.NotificationModel;
import com.app.trustChat.model.UserModel;
import com.app.trustChat.repository.ChatRoomRepository;
import com.app.trustChat.ui.chat.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.sinch.android.rtc.SinchHelpers;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements ServiceConnection {
    private SinchService.SinchServiceInterface mSinchServiceInterface;
    private UserModel currentUser;
    private CharSequence message;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        currentUser = MyApplication.getInstance().getCurrentUser();
        if (SinchHelpers.isSinchPushPayload(remoteMessage.getData())) {
            getApplicationContext().bindService(new Intent(this, SinchService.class), this, BIND_AUTO_CREATE);
        } else {
            /* if (!currentUser.getLastSeen().equals(UserStatus.ONLINE)) {*/
            if (remoteMessage.getData().size() > 0) {
                String body = remoteMessage.getData().get("body");
                getRoomDetails(body);
            }
            /*  }*/
        }
    }

    private void getRoomDetails(String msgBody) {
        NotificationModel.Body data = new Gson()
                .fromJson(msgBody,
                        NotificationModel.Body.class);
        message = data.message;
        new GetRoomDetailsAsync(data.roomId).execute();
    }

    private void sendNotification(ChatRoomModel chatRoomModel) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "fcm_default_channel";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(chatRoomModel.getTitle())
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            getSinchServiceInterface().startClient(currentUser.getUserId());
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        getSinchServiceInterface().stopClient();
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    class GetRoomDetailsAsync extends AsyncTask<Void, Void, ChatRoomModel> {
        String roomId;

        public GetRoomDetailsAsync(String roomId) {
            this.roomId = roomId;
        }

        @Override
        protected ChatRoomModel doInBackground(Void... voids) {
            ChatRoomRepository repository = new ChatRoomRepository(MyApplication.getInstance());
            ChatRoomModel chatRoom = repository.getChatRoom(roomId);
            return chatRoom;
        }

        @Override
        protected void onPostExecute(ChatRoomModel chatRoom) {
            if (chatRoom != null) {
                sendNotification(chatRoom);
            }
        }
    }
}