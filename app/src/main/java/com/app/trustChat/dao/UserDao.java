package com.app.trustChat.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.app.trustChat.model.UserModel;

import java.util.List;

@Dao
public abstract class UserDao {

    @Transaction
    public void updateUserList(List<UserModel> users){
        deleteAllUsers();
        insertAllUsers(users);
    }

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insertAllUsers(List<UserModel> users);

    @Query("DELETE FROM user_table")
    public abstract void deleteAllUsers();

    @Query("SELECT* FROM user_table ORDER BY usernm")
    public abstract List<UserModel> getAllUsers();

    @Query("SELECT* FROM user_table WHERE userId = :userId")
    public abstract UserModel getAllUser(String userId);
}
