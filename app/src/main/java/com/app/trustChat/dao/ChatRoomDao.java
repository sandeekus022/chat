package com.app.trustChat.dao;

import com.app.trustChat.model.ChatRoomModel;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public abstract class ChatRoomDao {
    @Query("DELETE FROM chat_room_table WHERE roomId = :roomId")
    public abstract void deleteChatRoom(String roomId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract Long createChatRoom(ChatRoomModel chatRoomModel);

     @Query("UPDATE chat_room_table set photo = :photo WHERE roomId = :roomId")
    public abstract void updateRoomPhoto(String roomId,String photo);

    @Query("SELECT *FROM chat_room_table WHERE roomId = :roomId")
    public abstract ChatRoomModel getRoomDetails(String roomId);

    @Query("SELECT *FROM chat_room_table")
    public abstract List<ChatRoomModel> getAllRooms();

    @Query("DELETE FROM chat_room_table WHERE roomId =:id")
    public abstract void deleteChatRoomById(String id);

    @Update
    public abstract Integer updateChatRoom(ChatRoomModel model);

}
