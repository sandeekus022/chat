package com.app.trustChat.dao;

import com.app.trustChat.model.Message;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public abstract class MessageDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insertAllMessage(List<Message> messages);

    @Query("DELETE FROM msg_table")
    public abstract void deleteMessage();

    @Query("DELETE FROM msg_table WHERE roomId= :roomId")
    public abstract void deleteMessageByRoomId(String roomId);

    @Query("DELETE FROM msg_table WHERE msgId = :msgId")
    public abstract void deleteMessageById(String msgId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract Long insertMessage(Message msg);

    @Query("SELECT *FROM msg_table WHERE roomId = :roomId")
    public abstract List<Message> getAllMessages(String roomId);


    @Query("SELECT *FROM msg_table WHERE roomId = :roomId AND isSeen = :isSeen")
    public abstract List<Message> getUnsentMsgs(String roomId,String isSeen);


    @Update
    public abstract Integer updateMessage(Message message);

    @Query("SELECT *FROM msg_table WHERE roomId = :roomId AND msgId = :messageId")
    public abstract Message getMessage(String roomId, Long messageId);
}
