package com.app.trustChat.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.app.trustChat.model.CallLog;

import java.util.List;

@Dao
public abstract class CallDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract Long insertCallLog(CallLog callLog);

    @Query("SELECT* FROM call_table ORDER BY startTime DESC")
    public abstract List<CallLog> getAllLogs();

}
